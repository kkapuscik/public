#ifndef V_LIST_H
#define V_LIST_H

#include <stdlib.h>


template <class T>
class VList;

#include "listelem.h"

template <class T>
class VList
{
// ----- FIELDS -----
private:
	VListElem<T>	*m_Head,*m_Tail;

// ----- METHODS -----
public:
	VList();
	~VList();

	void Clear();

	bool Delete(VListElem<T>* elem);
	bool Remove(VListElem<T>* elem);
	
	VListElem<T>* Find(const T data) const;
	VListElem<T>* FindAfter(const T data,VListElem<T>* last,bool include) const;
	VListElem<T>* FindBefore(const T data,VListElem<T>* last,bool include) const;

	VListElem<T>* GetHead() const;
	VListElem<T>* GetTail() const;

	bool IsInList(VListElem<T>* elem) const;
	
	VListElem<T>* InsertHead(const T data);
	VListElem<T>* InsertTail(const T data);
	VListElem<T>* InsertAfter(const T data,VListElem<T>* elem);
	VListElem<T>* InsertBefore(const T data,VListElem<T>* elem);

	void InsertHead(VListElem<T>* elem);
	void InsertTail(VListElem<T>* elem);
	bool InsertAfter(VListElem<T>* newelem,VListElem<T>* elem);
	bool InsertBefore(VListElem<T>* newelem,VListElem<T>* elem);

private:
	VListElem<T>* CreateNewElem(const T data);
};

template <class T>
VList<T>::VList() :
	m_Head(NULL),m_Tail(NULL)
{
}
template <class T>
VList<T>::~VList()
{
	Clear();
}

template <class T>
void VList<T>::Clear()
{
	while(m_Head)
		Delete(m_Head);
}

template <class T>
bool VList<T>::Delete(VListElem<T>* elem)
{
	if(Remove(elem))
	{
		delete elem;
		return true;
	}
	else
		return false;
}

template <class T>
bool VList<T>::Remove(VListElem<T>* elem)
{
	// test if belong to list
	if(!elem || elem->m_ParentList != this)
		return false;
	// test if it is head/tail and relink if needed
	if(elem == m_Head)
		m_Head = elem->m_Next;
	if(elem == m_Tail)
		m_Tail = elem->m_Prev;
	// relinking
	if(elem->m_Next)
		elem->m_Next->m_Prev = elem->m_Prev;
	if(elem->m_Prev)
		elem->m_Prev->m_Next = elem->m_Next;
	// remove
	elem->m_Next = elem->m_Prev = NULL;
	elem->m_ParentList = NULL;
	// all OK
	return true;
}


template <class T>
VListElem<T>* VList<T>::Find(const T data) const
{
	VListElem<T>* temp = m_Head;
	while(temp)
	{
		if(temp->m_Data == data)
			return temp;
		temp = temp->Next;
	}
	return NULL;
}

template <class T>
VListElem<T>* VList<T>::FindAfter(const T data,VListElem<T>* last,bool include) const
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->Next;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->Next;
	}
	return last;
}

template <class T>
VListElem<T>* VList<T>::FindBefore(const T data,VListElem<T>* last,bool include) const
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->Prev;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->Prev;
	}
	return last;
}


template <class T>
VListElem<T>* VList<T>::GetHead() const
{
	return m_Head;
}

template <class T>
VListElem<T>* VList<T>::GetTail() const
{
	return m_Tail;
}


template <class T>
bool VList<T>::IsInList(VListElem<T>* elem) const
{
	if(!elem)
		return false;
	return elem->m_ParentList == this;
}


template <class T>
VListElem<T>* VList<T>::InsertHead(const T data)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		InsertHead(newelem);
	return newelem;
}

template <class T>
VListElem<T>* VList<T>::InsertTail(const T data)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		InsertTail(newelem);
	return newelem;
}

template <class T>
VListElem<T>* VList<T>::InsertAfter(const T data,VListElem<T>* elem)
{
	VListElem<T> *newelem;

	if(!elem || elem->m_ParentList != this)
		return NULL;

	newelem = CreateNewElem(data);
	if(newelem)
	{
		if (!InsertAfter(newelem,elem))
		{
			delete newelem;
			newelem = NULL;
		}
	}
	return newelem;
}

template <class T>
VListElem<T>* VList<T>::InsertBefore(const T data,VListElem<T>* elem)
{
	VListElem<T> *newelem;

	if(!elem || elem->m_ParentList != this)
		return NULL;

	newelem = CreateNewElem(data);
	if(newelem)
	{
		if (!InsertBefore(newelem,elem))
		{
			delete newelem;
			newelem = NULL;
		}
	}
	return newelem;
}

	
template <class T>
void VList<T>::InsertHead(VListElem<T>* elem)
{
	// test if there is an element
	if(!elem || elem==m_Head)
		return;
	// remove it from previous list
	elem->Remove();
	// linking
	elem->m_ParentList = this;
	elem->m_Next = m_Head;
	if(m_Head)
		m_Head->m_Prev = elem;
	// set new head & eventually tail
	m_Head = elem;
	if(m_Tail == NULL)
		m_Tail = elem;
}

template <class T>
void VList<T>::InsertTail(VListElem<T>* elem)
{
	// test if there is an element
	if(!elem || elem==m_Tail)
		return;
	// remove it from previous list
	elem->Remove();
	// linking
	elem->m_ParentList = this;
	elem->m_Prev = m_Tail;
	if(m_Tail)
		m_Tail->m_Next = elem;
	// set new head & eventually tail
	m_Tail = elem;
	if(m_Head == NULL)
		m_Head = elem;
}

template <class T>
bool VList<T>::InsertAfter(VListElem<T>* newelem,VListElem<T>* elem)
{
	// check if element belong to list
	if(!elem || !newelem || elem->m_ParentList!=this || newelem==elem)
		return false;
	// remove newelem from old list
	newelem->Remove();
	// relink
	newelem->m_ParentList = this;
	newelem->m_Prev = elem;
	newelem->m_Next = elem->m_Next;
	
	if(elem->m_Next)
		elem->m_Next->m_Prev = newelem;
	elem->m_Next = newelem;
	if(elem == m_Tail)
		m_Tail = newelem;
	// all OK
	return true;
}

template <class T>
bool VList<T>::InsertBefore(VListElem<T>* newelem,VListElem<T>* elem)
{
	// check if element belong to list
	if(!elem || !newelem || elem->m_ParentList!=this || newelem==elem)
		return false;
	// remove newelem from old list
	newelem->Remove();
	// relink
	newelem->m_ParentList = this;
	newelem->m_Next = elem;
	newelem->m_Prev = elem->m_Prev;
	
	if(elem->m_Prev)
		elem->m_Prev->m_Next = newelem;
	elem->m_Prev = newelem;
	if(elem == m_Head)
		m_Head = newelem;
	// all OK
	return true;
}


template <class T>
VListElem<T>* VList<T>::CreateNewElem(const T data)
{
	VListElem<T> *newelem;
	try
	{
		newelem = new VListElem<T>(data);
	}
	catch(...)
	{
		newelem = NULL;
	}
	return newelem;
}

#endif
