#ifndef V_LIST_ELEM_H
#define V_LIST_ELEM_H

#include <stdlib.h>


template <class T>
class VListElem;

#include "list.h"

template <class T>
class VListElem
{
	friend class VList<T>;

// ----- FIELDS -----
public:
	T				m_Data;
private:
	VList<T>		*m_ParentList;
	VListElem<T>	*m_Next,*m_Prev;

// ----- METHODS -----
public:
	VListElem(const T data);
	~VListElem();

	VListElem<T>* InsertAfter(const T data);
	VListElem<T>* InsertBefore(const T data);

	bool InsertAfter(VListElem<T>* elem);
	bool InsertBefore(VListElem<T>* elem);

	void Remove();
	void Delete();

	VListElem<T>* GetNext() const;
	VListElem<T>* GetPrev() const;
};

template <class T>
VListElem<T>::VListElem(const T data) :
	m_Data(data),
	m_ParentList(NULL),
	m_Next(NULL),m_Prev(NULL)
{
}
template <class T>
VListElem<T>::~VListElem()
{
	if(m_ParentList)
		Remove();
}

template <class T>
VListElem<T>* VListElem<T>::InsertAfter(const T data)
{
	if(!m_ParentList)
		return NULL;
	return m_ParentList->InsertAfter(data,this);
}
template <class T>
VListElem<T>* VListElem<T>::InsertBefore(const T data)
{
	if(!m_ParentList)
		return NULL;
	return m_ParentList->InsertBefore(data,this);
}

template <class T>
bool VListElem<T>::InsertAfter(VListElem<T>* elem)
{
	if(!m_ParentList)
		return false;
	return m_ParentList->InsertAfter(elem,this);
}
template <class T>
bool VListElem<T>::InsertBefore(VListElem<T>* elem)
{
	if(!m_ParentList)
		return false;
	return m_ParentList->InsertBefore(elem,this);
}

template <class T>
void VListElem<T>::Remove()
{
	if(m_ParentList)
		m_ParentList->Remove(this);
}
template <class T>
void VListElem<T>::Delete()
{
	Remove();
	delete this;
}

template <class T>
VListElem<T>* VListElem<T>::GetNext() const
{
	return Next;
}
template <class T>
VListElem<T>* VListElem<T>::GetPrev() const
{
	return Prev;
}

#endif
