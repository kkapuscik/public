#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *buffer;
int bsize;
FILE *fin,*fout;
size_t load;
int total;

int main(int argc, char *argv[])
{
	if(argc != 4)
	{
		printf("USE: filecopy <FROM> <TO> <BUFFERSIZE>\n");
		printf("USE: filecopy <SKAD> <DOKAD> <ROZMIAR BUFORA>\n");
		return 0;
	}

	printf("FROM:   %s\n",argv[1]);
	printf("TO:     %s\n",argv[2]);
	printf("BUFFER: %s\n",argv[3]);

 	if (argc<3 || argc>4)
   {
   	printf("ERROR: Wrong number of parameters.\n");
		return 0;
   }
   bsize = -1;
   if (argc==4)
   	bsize = strtol(argv[3],NULL,10);
   if(bsize<1)
   	bsize=1;
   bsize *= 1024;

	buffer = (char *)malloc(bsize);
   if (buffer!=NULL)
   {
   	fin = fopen(argv[1],"rb");
      if (fin!=NULL)
      {
      	fout = fopen(argv[2],"wb");
         if (fout!=NULL)
         {
            for(total=0;;total+=load)
            {
         		printf("Copied %d bytes\r",total);
	         	load = fread(buffer,1,bsize,fin);
   	         if (load<=0)
               	break;
					if (fwrite(buffer,1,load,fout)!=load)
               {
               	printf("ERROR: Cannot write data.\n");
               	break;
               }
      		}
        		printf("Copied %d bytes\r",total);
            if (!feof(fin))
            	printf("ERROR: Cannot read data.\n");
            fclose(fout);
         }
         else
         	printf("ERROR: Cannot open destination file.\n");
         fclose(fin);
      }
      else
      	printf("ERROR: Cannot open source file.\n");
      free(buffer);
   }
   else
   	printf("ERROR: Cannot allocate memory.\n");

	return 0;
}
