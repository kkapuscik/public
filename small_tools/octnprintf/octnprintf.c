/*
 * Copyright (c) 2006 OCTaedr.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the OCTaedr.org nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */ 

#include "octnprintf.h"

#include <stdarg.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

/*---------------------------------------------------------------------------*/

#define DBGMSG(x)       x

/*---------------------------------------------------------------------------*/

// % [ param-no $] flags width [ . precision ] type conversion

/*---------------------------------------------------------------------------*/

#define WRITE_ERROR     -1

typedef int (*write_f)(void* context, char c);

/*---------------------------------------------------------------------------*/

typedef enum flags_t {
    NO_FLAGS    =   0,
    ZERO_FLAG   =   (1 << 0),
    MINUS_FLAG  =   (1 << 1),
    PLUS_FLAG   =   (1 << 2),
    SPACE_FLAG  =   (1 << 3),
    HASH_FLAG   =   (1 << 4),
    APOS_FLAG   =   (1 << 5)    
} flags_t;

/*---------------------------------------------------------------------------*/

typedef enum modifier_t {
    NO_MODIFIER,
    CHAR_MODIFIER,
    SHORT_MODIFIER,
    INTMAX_MODIFER,
    LONG_MODIFIER,
    LONGLONG_MODIFIER,
    QUAD_MODIFIER = LONGLONG_MODIFIER,
    PTRDIFF_MODIFIER,
    SIZET_MODIFIER
} modifier_t;

/*---------------------------------------------------------------------------*/

typedef enum conversion_t {
    INVALID_CONVERSION,
    INT_CONVERSION,
    CHAR_CONVERSION,
    FLOAT_CONVERSION,
    STRING_CONVERSION,
    OCTAL_CONVERSION,
    UNSIGNED_CONVERSION,
    LOW_HEX_CONVERSION,
    HIGH_HEX_CONVERSION,
    LOW_EXPONENT_CONVERSION,
    HIGH_EXPONENT_CONVERSION,
    LOW_BEST_FLOAT_CONVERSION,
    HIGH_BEST_FLOAT_CONVERSION,
    LOW_HEXFLOAT_CONVERSION,
    HIGH_HEXFLOAT_CONVERSION,
    POINTER_CONVERSION,
    CHARS_SO_FAR_CONVERSION,
    ERRNO_STRING_CONVERSION,
    LITERAL_PERCENT_CONVERSION
} conversion_t;

/*---------------------------------------------------------------------------*/

typedef struct entry_t {
    int error;
    int paramNo;
    int flags;
    int width;
    int precision;
    modifier_t modifier;
    conversion_t conversion;
} entry_t;

/*---------------------------------------------------------------------------*/

static int char2digit(
        char c)
{
    switch(c) {
        case '0':   return 0;
        case '1':   return 1;
        case '2':   return 2;
        case '3':   return 3;
        case '4':   return 4;
        case '5':   return 5;
        case '6':   return 6;
        case '7':   return 7;
        case '8':   return 8;
        case '9':   return 9;
        default:    assert(0);  return -1;
    }
}

/*---------------------------------------------------------------------------*/

static int parse_flags(
        const char* format,
        int i,
        entry_t* entry)
{
    entry->flags = NO_FLAGS;

    for(;; ++i) {
        switch(format[i]) {
            case '-':   entry->flags |= MINUS_FLAG;     break;
            case '+':   entry->flags |= PLUS_FLAG;      break;
            case ' ':   entry->flags |= SPACE_FLAG;     break;
            case '#':   entry->flags |= HASH_FLAG;      break;
            case '\'':  entry->flags |= APOS_FLAG;      break;
            case '0':   entry->flags |= ZERO_FLAG;      break;
            default:
                return i;
        }
    }
}

/*---------------------------------------------------------------------------*/

static int parse_number(
        const char* format,
        int i,
        int* result)
{
    int numValue = 0;
    int valid = 0;

    for(;; ++i) {
        switch(format[i]) {
            case '0':
                if(numValue > 0) {
                    numValue = numValue * 10 + char2digit(format[i]);
                } else {
                    *result = -1;
                    return i;
                }
                break;
            case '1':   case '2':   case '3':   case '4':
            case '5':   case '6':   case '7':   case '8':   case '9':
                numValue = numValue * 10 + char2digit(format[i]);
                valid = 1;
                break;
            case '*':
                if(numValue == 0) {
                    ++i;
                    *result = -2;
                    return i;
                } else {
                    *result = -1;
                    return i;
                }
            default:
                *result = (valid ? numValue : -1);
                return i;
        }
    }
}

/*---------------------------------------------------------------------------*/

static int parse_width(
        const char* format,
        int i,
        entry_t* entry)
{
    return parse_number(format, i, &entry->width);
}

/*---------------------------------------------------------------------------*/

static int parse_precision(
        const char* format,
        int i,
        entry_t* entry)
{
    return parse_number(format, i, &entry->precision);
}

/*---------------------------------------------------------------------------*/

static int parse_modifier(
        const char* format,
        int i,
        entry_t* entry)
{
    entry->modifier = NO_MODIFIER;

    switch(format[i]) {
        case 'h':
            ++i;
            if(format[i] == 'h') {
                ++i;
                entry->modifier = CHAR_MODIFIER;
            } else {
                entry->modifier = SHORT_MODIFIER;
            }
            break;
        case 'j':
            ++i;
            entry->modifier = INTMAX_MODIFER;
            break;
        case 'l': /* also "ll" */
            ++i;
            if(format[i] == 'l') {
                ++i;
                entry->modifier = LONGLONG_MODIFIER;
            } else {
                entry->modifier = LONG_MODIFIER;
            }
            break;
        case 'L':
        case 'q':
            ++i;
            entry->modifier = LONGLONG_MODIFIER;
            break;
        case 't':
            ++i;
            entry->modifier = PTRDIFF_MODIFIER;
            break;
        case 'z':
        case 'Z':
            ++i;
            entry->modifier = SIZET_MODIFIER;
            break;
    }
    
    return i;
}

/*---------------------------------------------------------------------------*/

static int parse_conversion(
        const char* format,
        int i,
        entry_t* entry)
{
    switch(format[i]) {
        /* conversion */
        case 'd':
        case 'i':   ++i;    entry->conversion = INT_CONVERSION;             break;
        case 'c':
        case 'C':   ++i;    entry->conversion = CHAR_CONVERSION;            break;
        case 'f':   ++i;    entry->conversion = FLOAT_CONVERSION;           break;
        case 's':
        case 'S':   ++i;    entry->conversion = STRING_CONVERSION;          break;
        case 'o':   ++i;    entry->conversion = OCTAL_CONVERSION;           break;
        case 'u':   ++i;    entry->conversion = UNSIGNED_CONVERSION;        break;
        case 'x':   ++i;    entry->conversion = LOW_HEX_CONVERSION;         break;
        case 'X':   ++i;    entry->conversion = HIGH_HEX_CONVERSION;        break;
        case 'e':   ++i;    entry->conversion = LOW_EXPONENT_CONVERSION;    break;
        case 'E':   ++i;    entry->conversion = HIGH_EXPONENT_CONVERSION;   break;
        case 'g':   ++i;    entry->conversion = LOW_BEST_FLOAT_CONVERSION;  break;
        case 'G':   ++i;    entry->conversion = HIGH_BEST_FLOAT_CONVERSION; break;
        case 'a':   ++i;    entry->conversion = LOW_HEXFLOAT_CONVERSION;    break;
        case 'A':   ++i;    entry->conversion = HIGH_HEXFLOAT_CONVERSION;   break;
        case 'p':   ++i;    entry->conversion = POINTER_CONVERSION;         break;
        case 'n':   ++i;    entry->conversion = CHARS_SO_FAR_CONVERSION;    break;
        case 'm':   ++i;    entry->conversion = ERRNO_STRING_CONVERSION;    break;
        case '%':   ++i;    entry->conversion = LITERAL_PERCENT_CONVERSION; break;
        default:
            entry->conversion = INVALID_CONVERSION;
            entry->error = 1;
            break;
    }
    return i;
}

/*---------------------------------------------------------------------------*/

static int parse_entry(
        const char* format,
        int i,
        entry_t* entry)
{
    /* parse flags */
    i = parse_flags(format, i, entry);
    /* parse width */
    i = parse_width(format, i, entry);
    if(format[i] == '$') {
        ++i;
        if((entry->flags == NO_FLAGS) && (entry->width > 0)) {
            entry->paramNo = entry->width;
        } else {
            entry->error = 1;
            return i;
        }
        /* parse flags */
        i = parse_flags(format, i, entry);
        /* parse width */
        i = parse_width(format, i, entry);
    } else {
        entry->paramNo = -1;
    }
    if(format[i] == '.') {
        ++i;
        /* parse precision */
        i = parse_precision(format, i, entry);
    } else {
        entry->precision = -1;
    }
    /* parse modifier */
    i = parse_modifier(format, i, entry);
    /* parse conversion */
    i = parse_conversion(format, i, entry);
    
    return i;
}

/*---------------------------------------------------------------------------*/

static int print_nstring(
        write_f writer,
        void* context,
        entry_t* entry,
        const char* s,
        int slen)
{
    // TODO: Flags, precision, 

    int i;
    int count = 0;
    int res;

    for(i = 0; i < slen; ++i) {
        res = writer(context, s[i]);
        if(res < 0) {
            return res;
        } else {
            count += res;
        }
    }

    return count;
}

/*---------------------------------------------------------------------------*/

static int print_char(
        write_f writer,
        void* context,
        entry_t* entry,
        int c)
{
    char cs[2];

    /* prepare 'fake' string */    
    cs[0] = (char)c;
    cs[1] = 0;

    return print_nstring(writer, context, entry, cs, 1);
}

/*---------------------------------------------------------------------------*/

static int print_string(
        write_f writer,
        void* context,
        entry_t* entry,
        const char* s)
{
    int len;

    if(s == NULL) {
        s = "(null)";
    }

    len = strlen(s);

    return print_nstring(writer, context, entry, s, len);
}

/*---------------------------------------------------------------------------*/

static int print_float(
        write_f writer,
        void* context,
        entry_t* entry,
        double f)
{
    char buffer[200];
    
    sprintf(buffer, "%f", f);
    
    return print_string(writer, context, entry, buffer);
}

/*---------------------------------------------------------------------------*/

static int print_exponent(
        write_f writer,
        void* context,
        entry_t* entry,
        double value,
        int upcase)
{
    if(upcase) {
        return printf("%E", value);
    } else {
        return printf("%e", value);
    }
}

/*---------------------------------------------------------------------------*/

static int print_best_float(
        write_f writer,
        void* context,
        entry_t* entry,
        double value,
        int upcase)
{
    if(upcase) {
        return printf("%G", value);
    } else {
        return printf("%g", value);
    }
}

/*---------------------------------------------------------------------------*/

static int print_hex_float(
        write_f writer,
        void* context,
        entry_t* entry,
        double value,
        int upcase)
{
    if(upcase) {
        return printf("%A", value);
    } else {
        return printf("%a", value);
    }
}

/*---------------------------------------------------------------------------*/

static int print_pointer(
        write_f writer,
        void* context,
        entry_t* entry,
        const void* value)
{
    return printf("%p", value);
}

/*---------------------------------------------------------------------------*/

static int print_errstr(
        write_f writer,
        void* context,
        entry_t* entry)
{
    assert(0);
    // printf("%s", strerror(errno));
    return printf("E");
}

/*---------------------------------------------------------------------------*/

static int print_set_charssofar(
        write_f writer,
        void* context,
        entry_t* entry,
        int* resvalue)
{
    assert(0);
    *resvalue = -1;
    return 0;
}

/*---------------------------------------------------------------------------*/

static char digit2char(
        int digit, int highcase)
{
    if(digit < 0) {
        assert(0);
        return '?';
    } else if((digit >= 0) && (digit <= 9)) {
        return '0' + digit;
    } else {
        if(highcase) {
            digit -= 10;
            digit += 'A';
            if(digit <= 'Z') {
                return (char)digit;
            } else {
                assert(0);
                return '?';
            }
        } else {
            digit -= 10;
            digit += 'a';
            if(digit <= 'z') {
                return (char)digit;
            } else {
                assert(0);
                return '?';
            }
        }
    }
}

/*---------------------------------------------------------------------------*/
typedef long long integer_t;
typedef unsigned long long uinteger_t;

static int print_integer(
        write_f writer,
        void* context,
        entry_t* entry,
        uinteger_t value,
        int negative,
        int base,
        int highcase)
{
    char buffer[256];
    char* current = &buffer[sizeof(buffer)];
    int used;
    int i;
    int written = 0;

    writer(context, '#');

    used = 0;
    if(value != 0) {
        while(value > 0) {
            int tmp = value % base;
            ++used;
            *(--current) = digit2char(tmp, highcase);
            value /= base;
        }
    } else {
        ++used;
        *(--current) = '0';
    }

    if(negative) {
        ++used;
        *(--current) = '-';
    } else {
        if(entry->flags & PLUS_FLAG) {
            ++used;
            *(--current) = '+';            
        } else if(entry->flags & SPACE_FLAG) {
            ++used;
            *(--current) = ' ';
        }
    }
    
    // TODO: Field justification

    for(i = 0; i < used; ++i) {
        if(writer(context, current[i]) >= 0) {
            ++written;
        } else {
            return -1;
        }
    }
    
    return written;
}

/*---------------------------------------------------------------------------*/

static int get_argument(
        int sigtype,
        entry_t* entry,
        va_list* args,
        uinteger_t* value,
        int* negative)
{
    if(sigtype) {
        integer_t tmp;

        switch(entry->modifier) {
            case NO_MODIFIER:
                tmp = va_arg(*args, int);
                break;
            case LONG_MODIFIER:
                tmp = va_arg(*args, long);
                break;
            case LONGLONG_MODIFIER:
                tmp = va_arg(*args, long long);
                break;
            case SHORT_MODIFIER:
                tmp = (short)va_arg(*args, int);
                break;
            case CHAR_MODIFIER:
                tmp = (char)va_arg(*args, int);
                break;
            default:
                entry->error = 1;
                return 0;
        }
        if(tmp < 0) {
            *negative = 1;
            *value = -tmp;
        } else {
            *negative = 0;
            *value = tmp;
        }
    } else {
        *negative = 0;

        switch(entry->modifier) {
            case NO_MODIFIER:
                *value = va_arg(*args, unsigned int);
                break;
            case LONG_MODIFIER:
                *value = va_arg(*args, unsigned long);
                break;
            case LONGLONG_MODIFIER:
                *value = va_arg(*args, unsigned long long);
                break;
            case SHORT_MODIFIER:
                *value = (unsigned short)va_arg(*args, unsigned int);
                break;
            case CHAR_MODIFIER:
                *value = (unsigned char)va_arg(*args, unsigned int);
                break;
            default:
                entry->error = 1;
                return 0;
        }
    }
    
    return 1;
}

static int print_entry(
        write_f writer,
        void* context,
        entry_t* entry,
        va_list* args)
{
    uinteger_t intvalue;
    int negative;

    if(entry->paramNo > 0) {
        assert(0);
        // NOT SUPPORTED YET
        return 1;
    }
    
    if(entry->width == -2) {
        entry->width = va_arg(*args, int);
    }
    if(entry->precision == -2) {
        entry->precision = va_arg(*args, int);
    }

    switch(entry->conversion) {
        case INT_CONVERSION:
            if(get_argument(1, entry, args, &intvalue, &negative)) {
                return print_integer(writer, context, entry, intvalue, negative, 10, 0);
            } else {
                return -1;
            }
        case OCTAL_CONVERSION:
            if(get_argument(0, entry, args, &intvalue, &negative)) {
                return print_integer(writer, context, entry, intvalue, negative, 8, 0);
            } else {
                return -1;
            }
        case UNSIGNED_CONVERSION:
            if(get_argument(0, entry, args, &intvalue, &negative)) {
                return print_integer(writer, context, entry, intvalue, negative, 10, 0);
            } else {
                return -1;
            }
        case LOW_HEX_CONVERSION:
            if(get_argument(0, entry, args, &intvalue, &negative)) {
                return print_integer(writer, context, entry, intvalue, negative, 16, 0);
            } else {
                return -1;
            }
        case HIGH_HEX_CONVERSION:
        {
            if(get_argument(0, entry, args, &intvalue, &negative)) {
                return print_integer(writer, context, entry, intvalue, negative, 16, 1);
            } else {
                return -1;
            }
        }

        case CHAR_CONVERSION:
            return print_char(writer, context, entry, va_arg(*args, int));
        case STRING_CONVERSION:
            return print_string(writer, context, entry, va_arg(*args, const char*));

        case FLOAT_CONVERSION:
            return print_float(writer, context, entry, va_arg(*args, double));
        case LOW_EXPONENT_CONVERSION:
            return print_exponent(writer, context, entry, va_arg(*args, double), 0);
        case HIGH_EXPONENT_CONVERSION:
            return print_exponent(writer, context, entry, va_arg(*args, double), 1);
        case LOW_BEST_FLOAT_CONVERSION:
            return print_best_float(writer, context, entry, va_arg(*args, double), 0);
        case HIGH_BEST_FLOAT_CONVERSION:
            return print_best_float(writer, context, entry, va_arg(*args, double), 1);
        case LOW_HEXFLOAT_CONVERSION:
            return print_hex_float(writer, context, entry, va_arg(*args, double), 0);
        case HIGH_HEXFLOAT_CONVERSION:
            return print_hex_float(writer, context, entry, va_arg(*args, double), 1);

        case POINTER_CONVERSION:
            return print_pointer(writer, context, entry, va_arg(*args, const void*));

        case CHARS_SO_FAR_CONVERSION:
            return print_set_charssofar(writer, context, entry, va_arg(*args, int*));
        case ERRNO_STRING_CONVERSION:
            return print_errstr(writer, context, entry);

        case LITERAL_PERCENT_CONVERSION:
            return print_char(writer, context, entry, '%');
        case INVALID_CONVERSION:
        default:
            assert(0);
            return -1;
    }
}

/*---------------------------------------------------------------------------*/

static int iprintf(
        write_f writer,
        void* context,
        const char* format,
        va_list args)
{
    int i;
    int count = 0;
    int result;

    for(i = 0; format[i] != '\0'; ) {
        if(format[i] == '%') {
            entry_t entry;
            int start;
            int end;

            entry.error = 0;

            start = i;
            ++i;

            i = parse_entry(format, i, &entry);
            end = i;

            if(!entry.error) {
                result = print_entry(writer, context, &entry, &args);
                if(result >= 0) {
                    count += result;
                } else {
                    return result;
                }
            } else {
                int j;
                for(j = start; j < end; ++j) {
                    result = writer(context, format[i]);
                    if(result >= 0) {
                        count += result;
                    } else {
                       return result;
                    }
                }
            }
        } else {
            result = writer(context, format[i]);
            if(result >= 0) {
                count += result;
            } else {
               return result;
            }
            ++i;
        }
    }

    return 0;
}

/*---------------------------------------------------------------------------*/

typedef struct stream_context_t {
    FILE* file;
} stream_context_t;

static int stream_writer(
        void* ctx,
        char c)
{
    stream_context_t* context = (stream_context_t*)ctx;

    /* FIXME ? Use printf so written chars will be good? */

    if(fputc(c, context->file) != EOF) {
        return 1;
    } else {
        return WRITE_ERROR;
    }
}

/*---------------------------------------------------------------------------*/

typedef struct string_context_t {
    char* buffer;
    int length;
    int position;
} string_context_t;

static int string_writer(
        void* ctx,
        char c)
{
    string_context_t* context = (string_context_t*)ctx;

    if(context->position < context->length) {
        context->buffer[context->position++] = c;
        return 1;
    } else {
        return 0;
    }
}

/*---------------------------------------------------------------------------*/

int oct_fprintf(
        FILE* file,
        const char* format,
        ...)
{
    va_list list;
    stream_context_t context;
    int result;

    context.file = file;

    va_start(list, format);
    result = iprintf(stream_writer, &context, format, list);
    va_end(list);

    return result;
}

/*---------------------------------------------------------------------------*/

int oct_printf(
        const char* format,
        ...)
{
    va_list list;
    stream_context_t context;
    int result;

    context.file = stdout;

    va_start(list, format);
    result = iprintf(stream_writer, &context, format, list);
    va_end(list);

    return result;
}

/*---------------------------------------------------------------------------*/

int oct_snprintf(
        char* buffer,
        size_t size,
        const char* format,
        ...)
{
    va_list list;
    string_context_t context;
    int result;

    assert(size > 0);

    if(size <= 0) {
        return -1;
    }
    if(size == 1) {
        buffer[0] = 0;
        return 0;
    }
    
    context.buffer = buffer;
    context.length = size - 1;
    context.position = 0;
    
    va_start(list, format);
    result = iprintf(string_writer, &context, format, list);
    va_end(list);

    context.buffer[context.position] = 0;

    return result;
}

/*---------------------------------------------------------------------------*/
