/*
 * Copyright (c) 2006 OCTaedr.org
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the OCTaedr.org nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */ 

#ifndef _OCT_PRINTF_H_
#define _OCT_PRINTF_H_

/*---------------------------------------------------------------------------*/

#include <stdio.h>

/*---------------------------------------------------------------------------*/

/**
 * \brief Print to standard output
 * 
 * The printf function writes output to standard output (stdout).
 * The output format is controlled by format string.
 * 
 * \param format - format string controlling the output.
 * 
 * \return Function returns the number of characters printed, or a negative
 * value if there was an output error.
 * 
 * \see See printf() function documentation for details.
 */
int oct_printf(
        const char* format,
        ...);


/**
 * \brief Print to stream
 * 
 * The printf function writes output to given stream.
 * The output format is controlled by format string.
 * 
 * \param stream - stream used to write output.
 * \param format - format string controlling the output.
 * 
 * \return Function returns the number of characters printed, or a negative
 * value if there was an output error.
 * 
 * \see See fpprintf() function documentation for details.
 */
int oct_fprintf(
        FILE* stream,
        const char* format,
        ...);


/**
 * \brief Print to string
 * 
 * The printf function writes output to given string.
 * The output format is controlled by format string.
 * 
 * \param buffer - string (buffer) to store output.
 * \param format - format string controlling the output.
 * 
 * \return Function returns the number of characters printed, or a negative
 * value if there was an output error.
 * 
 * \see See sprintf() function documentation for details.
 */
int oct_sprintf(
        char* buffer,
        const char* format,
        ...);


/**
 * \brief Print to string with limited size
 * 
 * The printf function writes output to given string.
 * The output format is controlled by format string.
 * 
 * \param buffer - string (buffer) to store output.
 * \param size - maximum number of characters to write (including null
 *               character).
 * \param format - format string controlling the output.
 * 
 * \return Function returns the number of characters to be printed, or a
 * negative value if there was an output error. Thus the whole output has been
 * stored in buffer if and only if returned value is lower than \c size.
 * 
 * \see See snprintf() function documentation for details.
 */
int oct_snprintf(
        char* buffer,
        size_t size,
        const char* format,
        ...);


/**
 * \brief Print to allocated string
 * 
 * The printf function writes output to allocated string. The string is
 * allocated using malloc().
 * The output format is controlled by format string.
 * 
 * \param resptr - place where allocated string will be placed.
 * \param buffer - string (buffer) to store output.
 * \param format - format string controlling the output.
 * 
 * \return Function returns the number of characters allocated for the buffer,
 * or a negative value if there was an error (usually on low-memory
 * situtations). If the function succeeded a pointer to allocated string is
 * stored in \c resptr otherwise the NULL is written under the address. The
 * user is responsible of freeing allocated buffer (if any) using free()
 * function.
 * 
 * \see See snprintf() function documentation for details.
 */
int oct_asprintf(
        char** resptr,
        const char* format,
        ...);

/*---------------------------------------------------------------------------*/

#endif /*_OCT_PRINTF_H_*/
