#include "octnprintf.h"

#include <stdio.h>

void test1()
{
    char buffer[1024];
/*
    printf("|  0x0.0000p+0|       0.0000|   0.0000e+00|            0|\n");
    printf("|  0x1.0000p-1|       0.5000|   5.0000e-01|          0.5|\n");
    printf("|  0x1.0000p+0|       1.0000|   1.0000e+00|            1|\n");
    printf("| -0x1.0000p+0|      -1.0000|  -1.0000e+00|           -1|\n");
    printf("|  0x1.9000p+6|     100.0000|   1.0000e+02|          100|\n");
    printf("|  0x1.f400p+9|    1000.0000|   1.0000e+03|         1000|\n");
    printf("| 0x1.3880p+13|   10000.0000|   1.0000e+04|        1e+04|\n");
    printf("| 0x1.81c8p+13|   12345.0000|   1.2345e+04|    1.234e+04|\n");
    printf("| 0x1.86a0p+16|  100000.0000|   1.0000e+05|        1e+05|\n");
    printf("| 0x1.e240p+16|  123456.0000|   1.2346e+05|    1.235e+05|\n");
*/
    printf("|a|       0.0000|   0.0000e+00|            0|\n");
    printf("|a|       0.5000|   5.0000e-01|          0.5|\n");
    printf("|a|       1.0000|   1.0000e+00|            1|\n");
    printf("|a|      -1.0000|  -1.0000e+00|           -1|\n");
    printf("|a|     100.0000|   1.0000e+02|          100|\n");
    printf("|a|    1000.0000|   1.0000e+03|         1000|\n");
    printf("|a|   10000.0000|   1.0000e+04|        1e+04|\n");
    printf("|a|   12345.0000|   1.2345e+04|    1.234e+04|\n");
    printf("|a|  100000.0000|   1.0000e+05|        1e+05|\n");
    printf("|a|  123456.0000|   1.2346e+05|    1.235e+05|\n");

    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 0.0, 0.0, 0.0, 0.0);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 0.5, 0.5, 0.5, 0.5);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 1.0, 1.0, 1.0, 1.0);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", -1.0, -1.0, -1.0, -1.0);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 100.0, 100.0, 100.0, 100.0);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 1000.0, 1000.0, 1000.0, 1000.0);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 10000.0000, 10000.0000, 10000.0000, 10000.0000);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 12345.0000, 12345.0000, 12345.0000, 12345.0000);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 100000.0000, 100000.0000, 100000.0000, 100000.0000);
    printf("|%13.4a|%13.4f|%13.4e|%13.4g|\n", 123456.0000, 123456.0000, 123456.0000, 123456.0000);

    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 0.0, 0.0, 0.0, 0.0);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 0.5, 0.5, 0.5, 0.5);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 1.0, 1.0, 1.0, 1.0);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", -1.0, -1.0, -1.0, -1.0);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 100.0, 100.0, 100.0, 100.0);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 1000.0, 1000.0, 1000.0, 1000.0);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 10000.0000, 10000.0000, 10000.0000, 10000.0000);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 12345.0000, 12345.0000, 12345.0000, 12345.0000);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 100000.0000, 100000.0000, 100000.0000, 100000.0000);
    printf(buffer);
    oct_snprintf(buffer, sizeof(buffer), "|%13.4a|%13.4f|%13.4e|%13.4g|\n", 123456.0000, 123456.0000, 123456.0000, 123456.0000);
    printf(buffer);
}

int main(int argc, char* argv[])
{
    char buffer[10];
    int len;
    
    len = fprintf(stdout, "\n");
    printf("%d %d\n", len, strlen(buffer));
    
    oct_printf("-10 => %-10d\n", -100);

/*
    printf("%X\n", -5);

    printf("%d\n", snprintf(buffer, sizeof(buffer), "%d", 5));

    printf("% d\n", 0);
    printf("%+ d\n", 0);
    printf("%+d\n", 0);
    printf("%+d\n", -1);
    printf("%+d\n", +1);

    len = oct_snprintf(buffer, sizeof(buffer),
            "%s ma %s, %s ma %s. Ale za to %d+%d%c%c%f\n",
            "Ala", "kota", "kot", "Ale", 2, 2, '<', '>', 5.5);
    printf("LEN = %d\n", len);
    printf("STR = %s\n", buffer);
*/
    len = oct_fprintf(stderr,
            "%s ma %s, %s ma %s. Ale za to %d+%d%c%c%f\n",
            "Ala", "kota", "kot", "Ale", 2, 2, '<', '>', 5.5);
    printf("LEN = %d\n", len);

    len = oct_snprintf(buffer, sizeof(buffer),
            "%s ma %s, %s ma %s. Ale za to %d+%d%c%c%f\n",
            "Ala", "kota", "kot", "Ale", 2, 2, '<', '>', 5.5);
    printf("STR = %s\n", buffer);
    printf("LEN = %d\n", len);

    printf("\n");
    printf("-10 => %-10d\n", -100);
    printf("+10 => %+10d\n", -100);
    printf(" 10 => % 10d\n", -100);
    printf("010 => %010d\n", -100);
    printf("10  => %10d\n", -100);
    printf("\n");
    printf("-10 => %-10d\n", 100);
    printf("+10 => %+10d\n", 100);
    printf(" 10 => % 10d\n", 100);
    printf("010 => %010d\n", 100);
    printf("10  => %10d\n", 100);
    printf("\n");
    printf("-10 => %-10d\n", 0);
    printf("+10 => %+10d\n", 0);
    printf(" 10 => % 10d\n", 0);
    printf("010 => %010d\n", 0);
    printf("10  => %10d\n", 0);
    printf("\n");
    printf("- => %-d\n", -100);
    printf("+ => %+d\n", -100);
    printf("  => % d\n", -100);
    printf("\n");
    printf("- => %-d\n", 100);
    printf("+ => %+d\n", 100);
    printf("  => % d\n", 100);
    printf("\n");
    printf("- => %-d\n", 0);
    printf("+ => %+d\n", 0);
    printf("  => % d\n", 0);

    oct_printf("\n");
    oct_printf("-10 => %-10d\n", -100);
    oct_printf("+10 => %+10d\n", -100);
    oct_printf(" 10 => % 10d\n", -100);
    oct_printf("010 => %010d\n", -100);
    oct_printf("10  => %10d\n", -100);
    oct_printf("\n");
    oct_printf("-10 => %-10d\n", 100);
    oct_printf("+10 => %+10d\n", 100);
    oct_printf(" 10 => % 10d\n", 100);
    oct_printf("010 => %010d\n", 100);
    oct_printf("10  => %10d\n", 100);
    oct_printf("\n");
    oct_printf("-10 => %-10d\n", 0);
    oct_printf("+10 => %+10d\n", 0);
    oct_printf(" 10 => % 10d\n", 0);
    oct_printf("010 => %010d\n", 0);
    oct_printf("10  => %10d\n", 0);
    oct_printf("\n");
    oct_printf("- => %-d\n", -100);
    oct_printf("+ => %+d\n", -100);
    oct_printf("  => % d\n", -100);
    oct_printf("\n");
    oct_printf("- => %-d\n", 100);
    oct_printf("+ => %+d\n", 100);
    oct_printf("  => % d\n", 100);
    oct_printf("\n");
    oct_printf("- => %-d\n", 0);
    oct_printf("+ => %+d\n", 0);
    oct_printf("  => % d\n", 0);

//    test1();

    return 0;
}
