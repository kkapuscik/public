#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <io.h>
#include <sys/stat.h>


char *buffer;
int bsize;
int fin,fout;
int load;
int total;

int main(int argc, char *argv[])
{
	if(argc != 4)
	{
		printf("USE: filecopy2 <FROM> <TO> <BUFFERSIZE>\n");
		printf("USE: filecopy2 <SKAD> <DOKAD> <ROZMIAR BUFORA>\n");
		return 0;
	}

	printf("FROM:   %s\n",argv[1]);
	printf("TO:     %s\n",argv[2]);
	printf("BUFFER: %s\n",argv[3]);

 	if (argc<3 || argc>4)
   {
   	printf("ERROR: Wrong number of parameters.\n");
		return 0;
   }
   bsize = -1;
   if (argc==4)
   	bsize = strtol(argv[3],NULL,10);
   if(bsize<1)
   	bsize=1;
   bsize *= 1024;

	buffer = (char *)malloc(bsize);
   if (buffer!=NULL)
   {
   	fin = open(argv[1],O_RDONLY | O_BINARY);
      if (fin>=0)
      {
      	fout = open(argv[2],O_WRONLY | O_CREAT | O_TRUNC | O_BINARY, S_IREAD | S_IWRITE);
         if (fout>=0)
         {
            for(total=0;;total+=load)
            {
         		printf("Copied %d bytes\r",total);
	         	load = read(fin,buffer,bsize);
   	         if (load<=0)
               	break;
					if (write(fout,buffer,load)!=load)
               {
               	printf("ERROR: Cannot write data.\n");
               	break;
               }
      		}
        		printf("Copied %d bytes\r",total);
            if (load < 0)
            	printf("ERROR: Cannot read data.\n");
            close(fout);
         }
         else
         	printf("ERROR: Cannot open destination file.\n");
         close(fin);
      }
      else
      	printf("ERROR: Cannot open source file.\n");
      free(buffer);
   }
   else
   	printf("ERROR: Cannot allocate memory.\n");

	return 0;
}
