#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <io.h>

int c;
char *buffer;
long bsize,reps;



struct Params
{
	char	SrcFile[512];
	char	DestFile[512];
	int		BufferSize;
	int		Retry;
};


bool ParseParams(int argc, char* argv[], Params& par)
{
	int len;
	char* p;
	char* q;

	// NUMBER OF PARAMS
	if(argc<3 || argc>5)
		return false;

	// SRC FILE
	strcpy(par.SrcFile,argv[1]);
	
	len = strlen(par.SrcFile);
	if(len == 0 || par.SrcFile[len-1]=='\\')
		return false;

	// DEST FILE / DEST DIR
	strcpy(par.DestFile,argv[2]);

	len = strlen(par.DestFile);
	if(len == 0)
		return false;
	if(par.DestFile[len-1] == '\\')
	{
		p = par.SrcFile;
		while(q = strchr(p, '\\'))
			p = q+1;

		strcat(par.DestFile,p);
	}

	// BUFFER SIZE
	if (sscanf(argv[3],"%d", &par.BufferSize) != 1)
		return false;
	if(par.BufferSize < 1)
		return false;

	// RETRY COUNT
	if (sscanf(argv[4],"%d", &par.Retry) != 1)
		return false;
	if(par.Retry < 0)
		return false;

	return true;
}

int main(int argc, char *argv[])
{
	Params par;
	char* buffer;
	int fin,fout;
	long load;
	long total,now,rr;

	if (!ParseParams(argc, argv, par))
	{
		printf("SpecCopy (C) Viewpoint 2002\n");
		printf("---------------------------\n");
		printf("speccopy <source_filename> <destination_filename> <buffersize> <retries>  or\n");
		printf("speccopy <source_filename> <destination_directory> <buffersize> <retries>  or\n");
		printf("(destination_directory must have '\\' at end\n");
		return 0;
	}

	printf("FROM:   %s\n",par.SrcFile);
	printf("TO:     %s\n",par.DestFile);
	printf("BUFFER: %s KB\n",par.BufferSize);
	printf("RETRY:  %s times\n",par.Retry);

	buffer = new char[par.BufferSize*1024];
	if(buffer != NULL)
	{
		fin = open(argv[1],O_RDONLY|O_BINARY);
		if (fin>=0)
		{
			fout = open(argv[2],O_CREAT|O_TRUNC|O_WRONLY|O_BINARY);
			if (fout>=0)
			{
				// COPYING
				now = 0;
				rr = 0;
				total = filelength(fin);
				while(now < total)
				{
					if(rr == 0)
						printf("POS: %ld / %ld\r", now, total);
					else
						printf("POS: %ld / %ld\r.  REP: %d\n", now, total, rr);
						
					now = lseek(fin, now, SEEK_SET);
					
					load = read(fin, buffer, bsize);
					if(load < 0)
					{
						rr++;
						if(rr >= reps)
						{
							int size = bsize;

						}
					}
				}


				close(fout);
			}
			else
				printf("Error opening destination file:\n  %s\n",par.DestFile);
		
			close(fin);
		}
		else
			printf("Error opening source file:\n  %s\n",par.SrcFile);

		delete[] buffer;
	}
	else
		printf("Memory allocation error (%d kB)\n", par.BufferSize);


	return 0;
}
