#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dir.h>
#include <io.h>

/* ----- COMPILATION FLAGS ----- */
const bool BigEndian = false;		// ON AMIGA CHANGE THIS TO 'true'

/* ----- STRUCTURES DEFINITIONS, CONSTANS etc. ----- */
typedef struct ModuleHeader
{
	char Name[20];
} MODHEAD;
typedef struct SampleHeader
{
	char				Name[22];
	unsigned short Length;
   char				FineTune;
   char				Volume;
   unsigned short	RepeatStart;
   unsigned short RepeatLength;
} SAMHEAD;
typedef struct SongHeader
{
	unsigned char	Length;		// 1-128
   char				OBSOLETE;	// should be 127
   char           Positions[128];
   char				ID[4];		// "M.K." or other letters
} SONGHEAD;
typedef struct PatternData
{	unsigned char	Data[1024];	} PATDATA;

/* ----- FUNCTIONS ----- */
unsigned short ReverseEndian(unsigned short value)
{	return (unsigned short)((value/0x100)+((value%0x100)<<8));	}




bool ReadInfos(FILE *DataFile, MODHEAD *Head,
	SAMHEAD *Sam, SONGHEAD *Song)
{
	short Cnt;

	if (fread(Head,sizeof(MODHEAD),1,DataFile)!=1)
   	return false;
   for (Cnt=0; Cnt<31; Cnt++)
   {
   	if (fread(&Sam[Cnt],sizeof(SAMHEAD),1,DataFile)!=1)
      	return false;
   	Sam[Cnt].Length=ReverseEndian(Sam[Cnt].Length);
   }
   if (fread(Song,sizeof(SONGHEAD),1,DataFile)!=1)
   	return false;
   return true;
}
void DisplayModuleInfo(MODHEAD *Module, SONGHEAD *Song)
{
	printf ("MODULE: %s\n", Module->Name);
   printf ("LENGTH: %hd patterns\n", (short)Song->Length);
}
short FindTotalPatterns(SONGHEAD *Song)
{
	short Cnt,Max;

   printf ("Searching for patterns number...");
   Max=0;
   for (Cnt=0; Cnt<(short)(Song->Length); Cnt++)
   	if ((short)(Song->Positions[Cnt]) > Max)
      	Max=Song->Positions[Cnt];
   printf ("%hd patterns found\n",Max+1);
   return short(Max+1);	/* because counted from 0 */
}
bool SkipPatterns(FILE *DataFile, short SkipNum)
{
	short Cnt;

	printf ("Skipping...\n");
   for (Cnt=0; Cnt<SkipNum; Cnt++)
   	if (fseek(DataFile,sizeof(PATDATA),SEEK_CUR)!=0)
      	return false;
   return true;
}

void ToUchar(char *Data, long Length)
{
	long Cnt;
   for (Cnt=0; Cnt<Length; Cnt++)
   	Data[Cnt]=(unsigned char)(Data[Cnt]+128);
}

typedef struct WaveHeader
{
	char RiffStr[4];
   unsigned long RiffLen;
   char WaveStr[4];

   char FrmtStr[4];
   unsigned long FrmtLen;
   unsigned short FrmtType;
   unsigned short FrmtChan;
   unsigned long  FrmtSmplPerSec;
   unsigned long  FrmtBytePerSec;
   unsigned short BlockAlign;
   unsigned short BitsPerSample;

   char DataStr[4];
   unsigned long DataLen;
} WAVEHEAD;
WAVEHEAD WavHead =
{	{'R','I','F','F'},
	0,
   {'W','A','V','E'},
   {'f','m','t',' '},
   16,
   1,		// PCM
   1,
   8287,
   8287,
   1,		// allign
   8,		// bitrate
   {'d','a','t','a'},
   0,
};

bool SaveSamples(FILE *DataFile,SAMHEAD *SamHeads,
	char *SamData,char *SamDir)
{
	char FileName[256];
	FILE *SamFile;
	long SamLen;
   short Cnt;
   bool Success;

   printf ("Saving...\n");
   for (Cnt=0,Success=true; Cnt<31 && Success==true; Cnt++)
   {
   	SamLen=SamHeads[Cnt].Length*2;
		if (SamLen<=0)
      	continue;
   	printf ("Sample%02hd %ld bytes\n", Cnt+1, SamLen);
      if (fread(SamData,SamLen,1,DataFile)==1)
      {
      	sprintf(FileName,"%s\\Sample%02hd.wav",SamDir,Cnt+1);
      	if ((SamFile=fopen(FileName,"wb"))!=NULL)
	      {
         	WavHead.RiffLen=SamLen+36;
         	WavHead.DataLen=SamLen;
         	if (fwrite(&WavHead,sizeof(WAVEHEAD),1,SamFile)!=1)
            	Success=false;
	      	ToUchar(SamData, SamLen);
				if (fwrite(SamData,SamLen,1,SamFile)!=1)
					Success=false;
				fclose(SamFile);
         }
         else
         	Success=false;
      }
      else
      	Success=false;
   }
   return Success;
}


FILE *ModFile,*SamFile;
char *SamData;
MODHEAD ModHead;
SAMHEAD SamHeads[32];
SONGHEAD SongHead;
bool Process(char *ModName,char *DestDir)
{
	bool Success=true;

   if (access(DestDir,0))
   	if (mkdir(DestDir)!=0)
      	return false;

	if ((SamData=(char *)malloc(65535L*2))!=NULL)
   {
      if ((ModFile = fopen(ModName, "rb"))!=NULL)
      {
			if (ReadInfos(ModFile,&ModHead,SamHeads,&SongHead)==true)
			{
         	DisplayModuleInfo(&ModHead,&SongHead);
         	if (SkipPatterns(ModFile,FindTotalPatterns(&SongHead))!=false)
            {
            	if (SaveSamples(ModFile,SamHeads,SamData,DestDir)==false)
               	Success=false;
					printf("END POSITION: %d\n", ftell(ModFile));
            }
            else
            	Success=false;
         }
         else
         	Success=false;
         fclose (ModFile);
      }
      else
      	Success=false;
      free(SamData);
   }
	else
   	Success=false;
   return Success;
}


/* ----- MAIN FUNCTION ----- MAIN FUNCTION ----- MAIN FUNCTION ----- MAIN FUNCTION ----- */
int main(int argc, char *argv[])
{
	if (argc!=3)
   {
   	printf ("RIGHT TEMPLATE IS: <MODULE_NAME> <DESTINATION_NAME>\n");
      return 0;
   }
   printf ("PROCESSING MODULE\n  %s\nCUTTING SAMPLES TO DIRECTORY:\n  %s\n", argv[1], argv[2]);
   if (Process (argv[1], argv[2]) == true)
   	printf ("OPERATION SUCCESSFUL\n");
	else
   	printf ("ERROR OCCURED\n");
}

