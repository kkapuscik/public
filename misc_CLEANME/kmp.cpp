#include <cstdlib>
#include <cstdio>
#include <cstring>
using namespace std;

int* preKmp(
        const char* needle)
{
    int* kmpNext = new int[strlen(needle) + 1];

    int i = 0;
    int j = kmpNext[0] = -1;
    while (needle[i]) {
        while (j > -1 && needle[i] != needle[j]) {
            j = kmpNext[j];
        }
        ++i;
        ++j;
        if (needle[i] == needle[j]) {
            kmpNext[i] = kmpNext[j];
        } else {
            kmpNext[i] = j;
        }
    }

    for(i = 0; ; ++i) {
        printf("%4d: %4d - %s %s\n", i, kmpNext[i], needle, needle + i);
        if(! needle[i]) {
            break;
        }
    }

    return kmpNext;
}


static void kmp(
        const char* haystack,
        const char* needle)
{
    /* preprocessing */
    int* kmpNext = preKmp(needle);

    /* searching */
    int i = 0;
    int j = 0;
    while (haystack[j]) {
        while (i > -1 && haystack[j] != needle[i]) {
printf("%d %d\n", i, j);
            i = kmpNext[i];
        }
        ++i;
        ++j;
printf("%d %d\n", i, j);
        if (needle[i] == 0) {
            printf("%d\n", j - i);
            i = kmpNext[i];
printf("%d %d\n", i, j);
        }
    }

    delete kmpNext;
}

int main(int argc, char* argv[])
{
    if(argc == 3 && argv[1] && argv[2]) {
        kmp(argv[1], argv[2]);
    }
    return 0;
}
