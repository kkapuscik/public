// THE JOSEPHUS PROBLEM SOLUTION
// - prints full josephus permutation (n,m)
//   in time O(nlogn)
//
// AUTHOR: Saveman / VSOFTWARE
// DATE:   08.02.2002

#include <iostream.h>
#include <stdlib.h>


const int MAXN = 50000;

struct Node
{
	int		Rank;
	int		Value;
	Node	*Parent,*Left,*Right;
} nodes[MAXN];

Node *MakeTree(int start,int end,Node *parent)
{
	if (start > end)
		return NULL;
	int m = (start+end)/2;
	nodes[m-1].Value = m;
	nodes[m-1].Left = MakeTree(start,m-1,&nodes[m-1]);
	nodes[m-1].Right = MakeTree(m+1,end,&nodes[m-1]);

	nodes[m-1].Parent = parent;
	nodes[m-1].Rank = 1;
	if(nodes[m-1].Left)
		nodes[m-1].Rank += nodes[m-1].Left->Rank;
	if(nodes[m-1].Right)
		nodes[m-1].Rank += nodes[m-1].Right->Rank;
   
	return &nodes[m-1];
}
void OutTree(Node *node)
{
	if(!node)
		return;
	OutTree(node->Left);
	cout << node->Value << "(" << node->Rank << ") ";
	OutTree(node->Right);
}
Node *TreeSuccessor(Node *node)
{
	if (node->Right!=NULL)
	{
		node = node->Right;
		while(node->Left)
			node = node->Left;
		return node;
	}
	Node *y = node->Parent;
	while(y && node==y->Right)
	{
		node = y;
		y = y->Parent;
	}
	return y;
}
void TreeDecrease(Node *z)
{
	while(z!=NULL)
	{
		z->Rank--;
		z = z->Parent;
	}
}
void TreeDelete(Node *z,Node *&root)
{
	Node *x,*y;
	if (!z->Left || !z->Right)
		y = z;
	else
		y = TreeSuccessor(z);
	TreeDecrease(y);
	if (y->Left)
		x = y->Left;
	else
		x = y->Right;
	if (x)
		x->Parent = y->Parent;
	if (!y->Parent)
		root = x;
	else
	{
		if(y == y->Parent->Left)
			y->Parent->Left = x;
		else
			y->Parent->Right = x;
	}
	if(y!=z)
		z->Value = y->Value;
}
inline int GetSize(Node *node)
{
	if (node)
		return node->Rank;
	else
		return 0;
}
Node *GetRankNode(Node *root,int rank)
{
	if (!root)
		return NULL;
	int r = GetSize(root->Left)+1;
	if (rank==r)
		return root;
	else if (rank<r)
		return GetRankNode(root->Left,rank);
	else
		return GetRankNode(root->Right,rank-r);
}

int Solve(int n,int m)
{
	int t,p,last;
	Node *root,*act;

	root = MakeTree(1,n,NULL);

	for(p=0,t=n; t>0; t--)
	{
		p += m;
		while(p>t)
			p -= t;
		act = GetRankNode(root,p);
		p--;
		last = act->Value;
		TreeDelete(act,root);
	}
	return last;
}
