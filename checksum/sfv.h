#ifndef _CHECKSUM__SFV_H_
#define _CHECKSUM__SFV_H_

typedef struct checksum_sfv_t checksum_sfv_t;

checksum_sfv_t* ChecksumSfv_Load(
        const char* fileName);

int ChecksumSfv_GetCount(
        checksum_sfv_t* sfv);

const char* ChecksumSfv_GetFileName(
        checksum_sfv_t* sfv,
        int index);

unsigned long ChecksumSfv_GetCrc32(
        checksum_sfv_t* sfv,
        int index);

void ChecksumSfv_Destroy(
        checksum_sfv_t* sfv);

#endif /*_CHECKSUM__SFV_H_*/
