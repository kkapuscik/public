#ifndef _CHECKSUM__CRC32_H_
#define _CHECKSUM__CRC32_H_

typedef struct {
    unsigned long value;
} checksum_crc32_t;

void ChecksumCrc32_Init(
        checksum_crc32_t* crc);

void ChecksumCrc32_Update(
        checksum_crc32_t* crc,
        void* data,
        int length);

unsigned long ChecksumCrc32_Final(
        checksum_crc32_t* crc);

#endif /*_CHECKSUM__CRC32_H_*/
