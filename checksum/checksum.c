#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "crc32.h"
#include "sfv.h"

typedef enum {
    CHECK_RESULT__SUCCESS,
    CHECK_RESULT__NOT_FOUND,
    CHECK_RESULT__READ_ERROR,
    CHECK_RESULT__BAD_CRC
} check_result_t;

enum {
    RESULT__SUCCESS,
    RESULT__ARGS_ERROR,
    RESULT__SFV_ERROR,
    RESULT__CRC_ERROR
};

static check_result_t CheckFileCrc32(
        const char* fileName,
        unsigned long crc32)
{
    char* buffer = malloc(16 * 1024 * 1024);
    check_result_t rv;

    FILE* f = fopen(fileName, "rb");
    if (f != NULL) {
        checksum_crc32_t crc32ctx;
        unsigned long fileCrc32;

        ChecksumCrc32_Init(&crc32ctx);

        for(;;) {
            int br = fread(buffer, 1, sizeof(buffer), f);
            if (br < 0) {
                fclose(f);
                return CHECK_RESULT__READ_ERROR;
            } else if (br == 0) {
                fclose(f);
                break;
            }

            ChecksumCrc32_Update(&crc32ctx, buffer, br);
        }

        fileCrc32 = ChecksumCrc32_Final(&crc32ctx);

        rv = (fileCrc32 == crc32) ? CHECK_RESULT__SUCCESS : CHECK_RESULT__BAD_CRC;

    } else {
        rv = CHECK_RESULT__NOT_FOUND;
    }

    free(buffer);

    return rv;
}

static const char* GetCheckResultString(
        check_result_t checkResult)
{
    switch (checkResult) {
        case CHECK_RESULT__SUCCESS:
            return "OK";
        case CHECK_RESULT__NOT_FOUND:
            return "Not Found";
        case CHECK_RESULT__READ_ERROR:
            return "Read Error";
        case CHECK_RESULT__BAD_CRC:
            return "Bad CRC";
        default:
            return "Unknown error";
    }
}

int main(int argc, char* argv[])
{
    int rv = RESULT__SUCCESS;

    if (argc == 2) {
        checksum_sfv_t* sfv;

        sfv = ChecksumSfv_Load(argv[1]);
        if (sfv != NULL) {
            int count = ChecksumSfv_GetCount(sfv);
            int i;

            printf("Number of entries in sfv file: %d\n", count);
            for (i = 0; i < count; ++i) {
                const char* fileName = ChecksumSfv_GetFileName(sfv, i);
                unsigned long crc32 = ChecksumSfv_GetCrc32(sfv, i);
                check_result_t checkResult;
                const char* checkResultStr;

                printf("   %s: ", fileName);
                fflush(stdout);

                checkResult = CheckFileCrc32(fileName, crc32);
                checkResultStr = GetCheckResultString(checkResult);

                printf("%s\n", checkResultStr);

                if (checkResult != CHECK_RESULT__SUCCESS) {
                    rv = RESULT__CRC_ERROR;
                }
            }

            if (rv != RESULT__SUCCESS) {
                fprintf(stderr, "Check failed for sfv file: %s\n", argv[1]);
            }

            ChecksumSfv_Destroy(sfv);
        } else {
            fprintf(stderr, "Could not load sfv file: %s\n", argv[1]);
            rv = RESULT__SFV_ERROR;
        }

    } else {
        fprintf(stderr, "Invalid arguments. Usage sfvcheck <sfv-file>\n");
        rv = RESULT__ARGS_ERROR;
    }

    return rv;
}
