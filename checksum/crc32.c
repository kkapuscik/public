#include "crc32.h"

/* Table of CRCs of all 8-bit messages. */
static unsigned long lookupTable[256];

/* Flag: has the table been computed? Initially false. */
static int lookupTableInitialized = 0;

/* Make the table for a fast CRC. */
void PrepareLookupTable(void)
{
    unsigned long c;
    int n, k;

    for (n = 0; n < 256; n++) {
        c = (unsigned long) n;

        for (k = 0; k < 8; k++) {
            if (c & 1) {
                c = 0xedb88320L ^ (c >> 1);
            } else {
                c = c >> 1;
            }
        }

        lookupTable[n] = c;
    }

    lookupTableInitialized = 1;
}

/*
 * Update a running CRC with the bytes buf[0..len-1] -- the CRC
 * should be initialized to all 1's, and the transmitted value
 * is the 1's complement of the final running CRC.
 */
unsigned long UpdateInternal(
        unsigned long crc,
        unsigned char *buf,
        int len)
{
    unsigned long c = crc;
    int n;

    for (n = 0; n < len; n++) {
        c = lookupTable[(c ^ buf[n]) & 0xff] ^ (c >> 8);
    }

    return c;
}


void ChecksumCrc32_Init(
        checksum_crc32_t* crc)
{
    if (!lookupTableInitialized) {
        PrepareLookupTable();
    }

    crc->value = 0xFFFFFFFFL;
}

void ChecksumCrc32_Update(
        checksum_crc32_t* crc,
        void* data,
        int length)
{
    crc->value = UpdateInternal(crc->value, (unsigned char*)data, length);
}

unsigned long ChecksumCrc32_Final(
        checksum_crc32_t* crc)
{
    crc->value ^= 0xFFFFFFFFL;
    return crc->value;
}
