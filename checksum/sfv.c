#include "sfv.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#define SFV_ARRAY_STEP  100

typedef struct sfv_entry_t {
    unsigned long crc32;
    char fileName[1];
} sfv_entry_t;

struct checksum_sfv_t {
    sfv_entry_t** entries;
    int used;
    int size;
};

static void AddEntry(
        checksum_sfv_t* sfv,
        const char* fileName,
        unsigned long crc32)
{
    sfv_entry_t* entry = malloc(sizeof(sfv_entry_t) + strlen(fileName));
    entry->crc32 = crc32;
    strcpy(entry->fileName, fileName);

    if (sfv->used == sfv->size) {
        sfv->size += SFV_ARRAY_STEP;
        sfv->entries = realloc(sfv->entries, sfv->size * sizeof(sfv_entry_t*));
    }

    sfv->entries[sfv->used++] = entry;
}

static int LoadFile(
        checksum_sfv_t* sfv,
        const char* fileName)
{
    int rv = 0; /* error */
    FILE* f = fopen(fileName, "rt");
    if (f != NULL) {
        char line[1024];
        for (;;) {
            char* lineRead = fgets(line, (int)sizeof(line), f);

            if (lineRead == NULL) {
                if (! ferror(f)) {
                    rv = 1; /* success */
                }
                break;
            } else {
                int i;
                int len = strlen(lineRead);
                char* sep;
                unsigned long crc32;

                if (lineRead[0] == ';') {
                    continue;
                }

                // TODO: check for max size

                for (i = len - 1; i >= 0; --i) {
                    if (isspace(lineRead[i])) {
                        lineRead[i] = 0;
                    } else {
                        break;
                    }
                }

                sep = strrchr(lineRead, ' ');
                if (sep == NULL || sep == lineRead) {
                    break;
                } else {
                    *sep = 0;
                }

                // TODO: fix getting crc
                sscanf(sep + 1, "%lx", &crc32);

                AddEntry(sfv, lineRead, crc32);
            }
        }

        fclose(f);
    }

    return rv;
}

checksum_sfv_t* ChecksumSfv_Load(
        const char* fileName)
{
    checksum_sfv_t* sfv;

    sfv = malloc(sizeof(checksum_sfv_t));
    sfv->entries = NULL;
    sfv->used = 0;
    sfv->size = 0;

    if (LoadFile(sfv, fileName)) {
        return sfv;
    } else {
        ChecksumSfv_Destroy(sfv);
        return NULL;
    }
}

int ChecksumSfv_GetCount(
        checksum_sfv_t* sfv)
{
    return sfv->used;
}

const char* ChecksumSfv_GetFileName(
        checksum_sfv_t* sfv,
        int index)
{
    return sfv->entries[index]->fileName;
}

unsigned long ChecksumSfv_GetCrc32(
        checksum_sfv_t* sfv,
        int index)
{
    return sfv->entries[index]->crc32;
}

void ChecksumSfv_Destroy(
        checksum_sfv_t* sfv)
{
    int i;

    for(i = 0; i < sfv->used; ++i) {
        free(sfv->entries[i]);
    }
    free(sfv->entries);
    free(sfv);
}
