/* Munkres (hungarian) assignement algorithm implementation */
/* CODE BY: Saveman 05-apr-2001 */

/* specification */
/* input */
/* n     - the size of C table */
/* C[][] - this is a cost table that you should fill - for example */
/*			  in [0][0..n-1] you should insert cost of doing specific */
/*			  work (0..n-1) by worker (0) */
/* the following data MUST be set to 0 at start (the algorithm don't do that */
/* M[][],Row[],Col[],Parh[][2],SaveRow,SaveCol; */
/* where [] means [0..n-1] */
/* -	-	-	-	TO RUN ALGORITHM CALL -	-	-	>>>>>>     Munkres();	*/
/* output */
/* at end of word you'll get the table M filled with 0's and 1's */
/* where 1 means that worker ROW does the word COLUMN */
/* example M[1][5] - worker 1 does the work 5 (and nobody other does it) - so */
/* there is no more 1's in row 1 and column 5 */
/* Clear?? */
/* THERE are three big things that you should now */
/* the cost matrix is of type Element that you can/should change (def. short) */
/* the maximal size of cost matrix is defined as MAXN (def. 150) */
/* and you must set the ELEMENT_MAX value to maximal value that can be stored */
/*    in type element (def. 30000) */

typedef short Element;
/* maximum value of Element */
#define ELEMENT_MAX 30000
#define MAXN 150

Element C[MAXN][MAXN];
short n;
char M[MAXN][MAXN];
char Row[MAXN],Col[MAXN];
short Path[MAXN][2];
short SaveRow,SaveCol;

short StepOne(void);
short StepTwo(void);
short StepThree(void);
short StepFour(void);
short StepFive(void);
short StepSix(void);

void Munkres (void)
{
	short stepnum;
	bool done;

   done=false;
   stepnum=1;
   while (!done)
   {
      switch (stepnum)
      {
         case 1:  stepnum=StepOne();   break;
         case 2:  stepnum=StepTwo();   break;
         case 3:  stepnum=StepThree(); break;
         case 4:  stepnum=StepFour();  break;
         case 5:  stepnum=StepFive();  break;
         case 6:  stepnum=StepSix();   break;
         default: done=true;           break;
      }
   }
}
/*------------------------------------------------------------*/
/* step one with subroutines */
short StepOne (void)
{
   Element minval;
   short i,j;

   for (i=0; i<n; i++)
   {
      minval=C[i][0];
      for (j=1; j<n; j++)
         if (minval>C[i][j])
            minval=C[i][j];
      for (j=0; j<n; j++)
         C[i][j]-=minval;
   }
   return 2;
}
/*------------------------------------------------------------*/
/* step two with subroutines */
short StepTwo (void)
{
   short i,j;
   for (i=0; i<n; i++)
      Row[i]=Col[i]=0;
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
         if (C[i][j]==0 && Col[j]==0 && Row[i]==0)
            Row[i]=Col[j]=M[i][j]=1;
   for (i=0; i<n; i++)
      Row[i]=Col[i]=0;
   return 3;
}
/*------------------------------------------------------------*/
/* step three with subroutines */
short StepThree (void)
{
   short i,j,count;
   for (i=0; i<n; i++)
      for (j=0; j<n; j++)
         if (M[i][j]==1)
            Col[j]=1;
   count=0;
   for (j=0; j<n; j++)
      count+=Col[j];
   if (count >=n)
      return 7;
   else
      return 4;
}
/*------------------------------------------------------------*/
/* step four with subroutines */


void FindAZero (short &row, short &col)
{
	short i,j;
   row=col=-1;
   for (i=0;i<n;i++)
   	for (j=0;j<n;j++)
         if (C[i][j]==0 && Row[i]==0 && Col[j]==0)
         {	row=i; col=j; return;   }
}
bool StarInRow (short row)
{
	short i;
	for (i=0;i<n;i++)
   	if (M[row][i]==1)
      	return true;
   return false;
}
void FindStarInRow (short &row, short &col)
{
	short i;
   col=-1;
	for (i=0; i<n; i++)
      if (M[row][i]==1)
   	{	col=i;	return;	}
}
short StepFour (void)
{
	short row,col;

   for(;;)
   {
   	FindAZero (row,col);
      if (row==-1)
      	return 6;
      else
      {
      	M[row][col]=2;
         if (StarInRow(row))
         {
         	FindStarInRow(row,col);
         	Row[row]=1;
         	Col[col]=0;
         }
         else
         {
            SaveRow=row;
            SaveCol=col;
            return 5;
         }
      }
   }
}

/*------------------------------------------------------------*/
/* step five with subroutines */
void FindStarInCol (short c, short &r)
{
	short i;
	r=-1;
   for (i=0;i<n;i++)
   	if (M[i][c]==1)
      {	r=i;	return;	}
}
void FindPrimeInRow (short r, short &c)
{
	short i;
   for (i=0;i<n;i++)
   	if (M[r][i]==2)
      {	c=i;	return;	}
}
void ConvertPath (short Count)
{
	short i;
   for (i=0; i<=Count; i++)
   	if (M[Path[i][0]][Path[i][1]]==1)
      	M[Path[i][0]][Path[i][1]]=0;
      else
         M[Path[i][0]][Path[i][1]]=1;
}
void ClearCovers (void)
{
	short i;
   for (i=0; i<n; i++)
      Row[i]=Col[i]=0;
}
void ErasePrimes (void)
{
	short i,j;
   for (i=0; i<n; i++)
   	for (j=0; j<n; j++)
      	if (M[i][j]==2)
         	M[i][j]=0;
}
short StepFive (void)
{
	short Count;
   short r,c;
   bool done;
   Count=0;
   Path[Count][0]=SaveRow;
   Path[Count][1]=SaveCol;
   done=false;
   while (!done)
   {
   	FindStarInCol (Path[Count][1], r);
      if (r!=-1)
      {
      	Count++;
         Path[Count][0]=r;
         Path[Count][1]=Path[Count-1][1];
      }
      else
			done=true;
      if (!done)
      {
      	FindPrimeInRow (Path[Count][0],c);
         Count++;
         Path[Count][0]=Path[Count-1][0];
         Path[Count][1]=c;
      }
   }
   ConvertPath(Count);
   ClearCovers();
   ErasePrimes();
	return 3;
}
/*------------------------------------------------------------*/
/* step six with subroutines */
Element FindSmallest (void)
{
	Element minval;
   short i,j;
   minval=ELEMENT_MAX;
   for (i=0;i<n;i++)
   {
   	if (Row[i]!=0)
      	continue;
   	for (j=0;j<n;j++)
      {
      	if (Col[j]!=0)
         	continue;
         if (minval > C[i][j])
         	minval=C[i][j];
      }
   }
   return minval;
}
short StepSix (void)
{
	Element minval;
   short i,j;
   minval = FindSmallest();
   for (i=0; i<n; i++)
   	for (j=0; j<n; j++)
      {
      	if (Row[i]==1)
         	C[i][j]+=Element(minval);
         if (Col[j]==0)
         	C[i][j]-=Element(minval);
      }
   return 4;
}

