#include "List.h"
#include <iostream>
using namespace std;

VList<int> mylist;

template <class T>
void OutList(const VList<T>* list)
{
	const VListElem<T>* elem = list->GetHead();
	cout << "List elements:" << endl;
	while(elem)
	{
		cout << elem->m_Data << ' ';
		elem = elem->GetNext();
	}
	cout << endl;
}

int Compare(const int& Data1,const int& Data2)
{
	if(Data1 < Data2)
		return 1;
	if(Data1 > Data2)
		return -1;
	return 0;
}

int main()
{
	int opt,data;
	bool end = false;
	VListElem<int> *node;

	while(!end)
	{
		cout << endl;
		cout << "Select" << endl;
		cout << "1. Insert Head" << endl;
		cout << "2. Clear list" << endl;
		cout << "3. Print" << endl;
		cout << "4. Find" << endl;
		cout << "5. Delete" << endl;
		cout << "6. Sort" << endl;
		cout << "7. Remove & relink" << endl;
		cout << "8. Custom test" << endl;
		cout << "9. Reverse sort (function)" << endl;
		cout << "0. Quit" << endl;
		cout << endl << "> ";
		cin >> opt;
		switch(opt)
		{
			case 1:
				cout << "Enter value:" << endl; 
				cout << "> ";
				cin >> data;
				if (mylist.InsertHead(data) != NULL)
					cout << "-- VALUE INSERTED --" << endl;
				else
					cout << "--- INSERT ERROR ---" << endl;
				break;
			case 2:
				mylist.Clear();
				break;
			case 3:
				OutList(&mylist);
				break;
			case 4:
				cout << "Enter value:" << endl; 
				cout << "> ";
				cin >> data;
				if (mylist.Find(data) != NULL)
					cout << "---- VALUE FOUND ----" << endl;
				else
					cout << "- VALUE NOT FOUND ---" << endl;
				break;
			case 5:
				cout << "Enter value:" << endl; 
				cout << "> ";
				cin >> data;
				if ((node = mylist.Find(data)) != NULL)
				{
					mylist.Delete(node);
					cout << "--- VALUE " << data << " DELETED ---" << endl;
				}
				else
					cout << "-- VALUE NOT FOUND --" << endl;
				break;
			case 6:
				cout << "List before sort" << endl;
				OutList(&mylist);
				mylist.Sort();
				cout << "List after sort" << endl;
				OutList(&mylist);
				break;
			case 7:
				cout << "Enter value:" << endl; 
				cout << "> ";
				cin >> data;
				if ((node = mylist.Find(data)) != NULL)
				{
					node->Remove();

					if(mylist.InsertTail(node))
						cout << "Relink successful" << endl;
					else
					{
						cout << "Relink failed" << endl;
						delete node;
					}
				}
				else
					cout << "-- VALUE NOT FOUND --" << endl;
				break;
			case 8:
				cout << "List before test:" << endl;
				OutList(&mylist);
				cout << "Inserting -1 at start & +1 at end" << endl;
				mylist.InsertHead(-1);
				mylist.InsertTail(+1);
				cout << "List after test:" << endl;
				OutList(&mylist);
				break;
			case 9:
				cout << "List before sort" << endl;
				OutList(&mylist);
				mylist.Sort(Compare);
				cout << "List after sort" << endl;
				OutList(&mylist);
				break;
			case 0:
				end = true;
				break;
		}
	}
	
	return 0;
}
