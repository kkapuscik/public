//----------------------------------------------------------
// TreeAVL.H
//----------------------------------------------------------
// Implementation of AVL (balanced binary search tree)
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------


#ifndef V_TREE_AVL_H
#define V_TREE_AVL_H

//----------------------------------------------------------
template <class T>
class VTreeAvl;
//----------------------------------------------------------

//----------------------------------------------------------
#include "NodeAVL.h"
#include <stdlib.h>
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VTreeAvl
{
private:
	// wskaznik na korzen drzewa
	VNodeAvl<T>	*Root;


public:
	// konstruktor - tworzy puste drzewo
	VTreeAvl();
	// destruktor - zwalnia wszystkie obiekty
	~VTreeAvl();

public:
	// funkcja kasujaca wszystkie obiekty
	// znajdujace sie w drzewie
	void DeleteAll();
private:
	// funkcja pomocnicza do kasowania wszystkich
	// obiektow znajdujacych sie w drzewie
	void DeleteAllProc(VNodeAvl<T>* node);


public:
	// funkcja kasuj�ca obiekt z drzewa
	// moze zwrocic false kiedy parametr
	// 'node' jest rowny NULL lub
	// kiedy 'node' nie nalezy do drzewa
	bool Delete(VNodeAvl<T>* node);


	// funkcja dolaczajaca element do drzewa
	VNodeAvl<T>* Insert(VNodeAvl<T>* node);

	// funkcja tworz�ca nowy element
	// i dolaczajaca go do drzewa
	VNodeAvl<T>* Insert(const T data);


	// funkcja "wycinajaca" element z drzewa
	// element zostaje usuniety z drzewa, ale
	// jego pamiec nie zostaje zwolniona
	// (mozna go pozniej np. dolaczyc
	// funkcja Insert
	bool Remove(VNodeAvl<T>* node);


	// funkcja odnajdujaca element w drzewie
	// UWAGA: Funkcja zwraca pierwszy napotkany element
	// spelniajacy zadane warunki (najblizszy korzeniowi)
	VNodeAvl<T>* Find(const T data);
	// funkcja odnajdujaca element w drzewie
	// UWAGA: Funkcja zwraca pierwszy napotkany element
	// spelniajacy zadane warunki (najblizszy korzeniowi)
	const VNodeAvl<T>* Find(const T data) const;


	// funkcja zwracajaca korzen drzewa
	VNodeAvl<T>* GetRoot();
	// funkcja zwracajaca korzen drzewa
	const VNodeAvl<T>* GetRoot() const;


	// funkcja sprawdzajaca czy
	// element nalezy do drzewa
	bool IsTree(const VNodeAvl<T>* node) const;


	// zwraca minimum w drzewie
	VNodeAvl<T>* GetMin();
	// zwraca minimum w drzewie
	const VNodeAvl<T>* GetMin() const;

	// zwraca maksimum w drzewie
	VNodeAvl<T>* GetMax();
	// zwraca maksimum w drzewie
	const VNodeAvl<T>* GetMax() const;

private:
	// funkcja wykonujaca pojedyncza rotacje
	// parametr left = true oznacza ze jest to lewa
	// rotacja, natomiast = false ze prawa
	void RotateSingle(VNodeAvl<T>* parent,
		VNodeAvl<T>* son,bool left);
	// funkcja wykonujaca podwojna rotacje 
	void RotateDouble(VNodeAvl<T>* parent,VNodeAvl<T>* son,
		VNodeAvl<T>* subson,bool left);
};
//----------------------------------------------------------


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VTreeAvl<T>::VTreeAvl() :
	Root(NULL)
{
}

//----------------------------------------------------------
template <class T>
VTreeAvl<T>::~VTreeAvl()
{
	DeleteAll();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VTreeAvl<T>::DeleteAll()
{
	DeleteAllProc(Root);
	Root = NULL;
}

//----------------------------------------------------------
template <class T>
void VTreeAvl<T>::DeleteAllProc(VNodeAvl<T>* node)
{
	if(node)
	{
		if(node->Left)
			DeleteAllProc(node->Left);
		if(node->Right)
			DeleteAllProc(node->Right);
		delete node;
	}
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeAvl<T>::Delete(VNodeAvl<T>* node)
{
	if(node->Remove())
	{
		delete node;
		return true;
	}
	return false;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::Insert(VNodeAvl<T>* node)
{
	if(!node || node->OwnerTree)
		return NULL;
	
	node->OwnerTree = this;

	if(Root)
	{
		VNodeAvl<T>* parent = Root;
		for(;;)
		{
			if(node->Data <= parent->Data)
			{
				if(parent->Left)
					parent = parent->Left;
				else
				{
					parent->Left = node;
					node->Parent = parent;
					break;
				}
			}
			else
			{
				if(parent->Right)
					parent = parent->Right;
				else
				{
					parent->Right = node;
					node->Parent = parent;
					break;
				}
			}
		}

		// ODBUDOWA STRUKTURY DRZEWA AVL
		// osobno rozpatrujemy poprzednika drzewa
		if(parent->Differ != 0)
			parent->Differ = 0;
		else
		{
			if(node == parent->Left)
				parent->Differ = 1;
			else
				parent->Differ = -1;

			RestoreAVL(parent->Parent,parent,son,true);
		}
	}
	else
		Root = node;
	return node;
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::Insert(const T data)
{
	VNodeAvl<T>* newnode;
	// podwojny test na NULL i na throw exception
	// poniewaz rozne kompilatory roznie robia
	try
	{
		newnode = new VNodeAvl<T>(data);
	}
	catch(...)
	{
		newnode = NULL;
	}
	if(newnode)
	{
		if(Insert(newnode))
			return newnode;
		delete newnode;
	}
	return NULL;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeAvl<T>::Remove(VNodeAvl<T>* node)
{
	if(!node || node->OwnerTree != this)
		return false;

	VNodeBst<T> *todel,*restson;

	if(node->Left==NULL || node->Right==NULL)
		todel = node;
	else
		todel = node->GetNext();

	if(todel->Left != NULL)
		restson = todel->Left;
	else
		restson = todel->Right;

	if(restson!=NULL)
		restson->Parent = todel->Parent;
	if(todel->Parent == NULL)
		Root = restson;
	else
	{
		if(todel->Parent->Left == todel)
			todel->Parent->Left = restson;
		else
			todel->Parent->Right = restson;
	}
	if(todel != node)
	{
		// relink from todel to node
		todel->Parent = node->Parent;
		if(todel->Parent)
		{
			if(todel->Parent->Left == node)
				todel->Parent->Left = todel;
			else
				todel->Parent->Right = todel;
		}
		todel->Left = node->Left;
		if(todel->Left)
			todel->Left->Parent = todel;
		todel->Right = node->Right;
		if(todel->Right)
			todel->Right->Parent = todel;
	}



	node->OwnerTree = NULL;
	node->Left = node->Right = node->Parent = NULL;

	return true;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::GetMin()
{
	return Root->GetSubMin();
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VTreeAvl<T>::GetMin() const
{
	return Root->GetSubMin();
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::GetMax()
{
	return Root->GetSubMax();
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VTreeAvl<T>::GetMax() const
{
	return Root->GetSubMax();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::Find(const T data)
{
	VNodeAvl<T>* node = Root;
	while(node)
	{
		if(node->Data == data)
			return node;
		else if (node->Data < data)
			node = node->Right;
		else
			node = node->Left;
	}
	return NULL;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VTreeAvl<T>::Find(const T data) const
{
	return const_cast<VTreeAvl*>(this)->Find(data);
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VTreeAvl<T>::GetRoot()
{
	return Root;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VTreeAvl<T>::GetRoot() const
{
	return Root;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeAvl<T>::IsTree(const VNodeAvl<T>* node) const
{
	if(node && node->OwnerTree == this)
		return true;
	return false;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VTreeAvl<T>::RotateSingle(VNodeAvl<T>* parent,
	VNodeAvl<T>* son,bool left)
{
	// przewiazujemy rodzicow
	if(parent->Parent)
	{
		if(parent->Parent->Left == parent)
			parent->Parent->Left = son;
		else
			parent->Parent->Right = son;
	}
	else
		Root = son;
	son->Parent = parent->Parent;
	parent->Parent = son;

	// przewiazujemy dzieci
	if(left)
	{
		// B
		parent->Left = son->Right;
		parent->Left->Parent = parent;
		
		// przewiazujemy parent i son
		son->Right = parent;
	}
	else
	{
		// B
		parent->Right = son->Left;
		parent->Right->Parent = parent;

		// przewiazujemy parent i son
		son->Left = parent;
	} 
}

//----------------------------------------------------------
template <class T>
void VTreeAvl<T>::RotateDouble(VNodeAvl<T>* parent,
	VNodeAvl<T>* son,VNodeAvl<T>* subson,bool left)
{
	// przewiazujemy rodzicow
	if(parent->Parent)
	{
		if(parent->Parent->Left = parent)
			parent->Parent->Left = subson;
		else
			parent->Parent->Right = subson;
	}
	else
		Root = subson;
	subson->Parent = parent->Parent;
	parent->Parent = son->Parent = subson;

	if (left)
	{
		// B
		son->Right = subson->Left;
		son->Right->Parent = son;

		// C
		parent->Left = subson->Right;
		parent->Left->Parent = parent;

		// przewiazujemy elementy
		subson->Left = son;
		subson->Right = parent;
	}
	else
	{
		// B
		son->Left = subson->Right;
		son->Left->Parent = son;

		// C
		parent->Right = subson->Left;
		parent->Right->Parent = parent;

		// przewiazujemy elementy
		subson->Right = son;
		subson->Left = parent;
	}
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VTreeAvl<T>::RestoreAVL(VNodeAvl<T>* parent,
	VNodeAvl<T>* son,VNodeAvl<T>* subson,bool insert)
{
	if(insert)
	{
		while(parent)
		{
			switch(parent->Differ)
			{
				case 0:
					parent->Differ = parent->Left == son ? 1 : -1;
					break;
				case 1:
					if(parent->Right == son)
						parent->Differ = 0;
					else
					{
						if(son->Differ == 1)
							RotateSingle(parent,son,false);
						else
							RotateDouble(parent,son,subson,false);
					}
					return;
				case -1:
					if(parent->Left == son)
						parent->Differ = 0;
					else
					{
						if(son->Differ == -1)
							RotateSingle(parent,son,true);
						else
							RotateDouble(parent,son,subson,false);
					}
					return;
			}
			parent = parent->Parent;
			son = parent;
			subson = son;
		}
	}
	else // delete
	{

	}
}

//----------------------------------------------------------
//----------------------------------------------------------

#endif
