#ifndef v_tree_bst_h
#define v_tree_bst_h

#include <stdlib.h>

template <class T>
class VTreeBST;


template <class T>
class VNodeBST
{
	friend class VTreeBST<T>;


private:
	const T		*Data;
	VNodeBST<T>	*Left,*Right,*Parent;

private:
	VNodeBST(const T *data) :
		Left(NULL),Right(NULL),Parent(NULL),Data(data)
	{};

	~VNodeBST()
	{
		if(Data)
			delete const_cast<T*>(Data);
	};

public:
	const T* GetData() const 
	{	return Data;	};
};

template <class T>
class VTreeBST
{
	VNodeBST<T>		*Root;

private:
	void PrintInOrder(VNodeBST<T> *node);
	VNodeBST<T> * GetNext(VNodeBST<T> *node);
	VNodeBST<T> * GetPrev(VNodeBST<T> *node);
	VNodeBST<T> * GetMinimum(VNodeBST<T> *node);
	VNodeBST<T> * GetMaximum(VNodeBST<T> *node);
public:
	VTreeBST();
	~VTreeBST();

	bool Insert(const T *data);

	void Delete(const T *data);
	void Delete(VNodeBST<T> *node);
	
	T* Remove(const T *data);
	T* Remove(VNodeBST<T> *node);
	
	VNodeBST<T>* Find(const T *data);

	void PrintInOrder();
};

template <class T>
VTreeBST<T>::VTreeBST() :
	Root(NULL)
{
}
template <class T>
VTreeBST<T>::~VTreeBST()
{
	while(Root != NULL)
		Delete(Root);
}

template <class T>
bool VTreeBST<T>::Insert(const T *data)
{
	if(Root!=NULL)
	{
		VNodeBST<T> *parent,*newnode;

		newnode = new VNodeBST<T>(data);
		if(newnode==NULL)
			return false;
		parent = Root;
		while(parent)
		{
			if(*data < *parent->Data)
			{
				if(parent->Left!=NULL)
					parent = parent->Left;
				else
				{
					newnode->Parent = parent;
					parent->Left = newnode;
					parent = NULL;
				}
			}
			else
			{
				if(parent->Right!=NULL)
					parent = parent->Right;
				else
				{
					newnode->Parent = parent;
					parent->Right = newnode;
					parent = NULL;
				}
			}
		}
		return true;
	}
	else
	{
		Root = new VNodeBST<T>(data);
		return Root!=NULL;
	}
}

template <class T>
void VTreeBST<T>::Delete(const T *data)
{
	Delete(Find(data));
}
template <class T>
void VTreeBST<T>::Delete(VNodeBST<T> *node)
{
	if(node==NULL)
		return;
	
	VNodeBST<T> *todel,*restson;

	if(node->Left==NULL || node->Right==NULL)
		todel = node;
	else
		todel = GetNext(node);

	if(todel->Left != NULL)
		restson = todel->Left;
	else
		restson = todel->Right;

	if(restson!=NULL)
		restson->Parent = todel->Parent;
	if(todel->Parent == NULL)
		Root = restson;
	else
	{
		if(todel->Parent->Left == todel)
			todel->Parent->Left = restson;
		else
			todel->Parent->Right = restson;
	}
	if(todel!=node)
	{
		const T* temp;
		temp = node->Data;
		node->Data = todel->Data;
		todel->Data = temp;
	}

	delete todel;
}
template <class T>
T* VTreeBST<T>::Remove(const T *data)
{
	return Remove(Find(data));
}
template <class T>
T* VTreeBST<T>::Remove(VNodeBST<T> *node)
{
	T* data = const_cast<T*>(node->Data);
	node->Data = NULL;
	Delete(node);
	return data;
}
template <class T>
VNodeBST<T>* VTreeBST<T>::Find(const T *data)
{
	VNodeBST<T> *temp = Root;
	while(temp)
	{
		if(*temp->Data == *data)
			return temp;
		if(*temp->Data < *data)
			temp = temp->Right;
		else
			temp = temp->Left;
	}
	return NULL;
}

template <class T>
void VTreeBST<T>::PrintInOrder()
{
	if(Root)
		PrintInOrder(Root);
	cout << endl;
}

template <class T>
void VTreeBST<T>::PrintInOrder(VNodeBST<T> *node)
{
	if(node->Left)
		PrintInOrder(node->Left);
	cout << *node->Data << ' ';
	if(node->Right)
		PrintInOrder(node->Right);
}
template <class T>
VNodeBST<T> * VTreeBST<T>::GetNext(VNodeBST<T> *node)
{
	if(node==NULL)
		return NULL;
	if(node->Right != NULL)
		return GetMinimum(node->Right);
	else
	{
		VNodeBST<T> *par = node->Parent;
		while(par)
		{
			if(par->Right != node)
				break;
			else
			{
				par = par->Parent;
				node = node->Parent;
			}
		}
		return par;
	}
}
template <class T>
VNodeBST<T> * VTreeBST<T>::GetPrev(VNodeBST<T> *node)
{
	if(node==NULL)
		return NULL;
	if(node->Left != NULL)
		return Maximum(node->Left);
	else
	{
		VNodeBST<T> *par = node->Parent;
		while(par)
		{
			if(par->Left != node)
				break;
			else
			{
				par = par->Parent;
				node = node->par;
			}
		}
		return par;
	}
}
template <class T>
VNodeBST<T> * VTreeBST<T>::GetMinimum(VNodeBST<T> *node)
{
	if(node==NULL)
		return NULL;
	while(node->Left)
		node = node->Left;
	return node;
}
template <class T>
VNodeBST<T> * VTreeBST<T>::GetMaximum(VNodeBST<T> *node)
{
	if(node==NULL)
		return NULL;
	while(node->Right)
		node = node->Right;
	return node;
}

#endif
