template <class T> class Queue
{
	private:
   	T *data;
      int size,head,tail;
   public:
   	Queue(int elements);
      ~Queue(void);
      int  Inside(void);
      bool IsFull(void);
      bool IsEmpty(void);
      bool Enqueue(T element);
      bool Dequeue(T &element);
};
template <class T> Queue<T>::Queue(int elements)
{	data = new T[elements];
   size = elements;
   head = tail = 0;
}
template <class T> Queue<T>::~Queue(void)
{	delete[] data;		}
template <class T> Queue<T>::Inside(void)
{
	if (tail >= head)
   	return tail-head;
   else
   	return size-head+tail;
}
template <class T> bool Queue<T>::IsFull(void)
{	return head%size==(tail+1)%size ? true : false;		}
template <class T> bool Queue<T>::IsEmpty(void)
{	return head%size==tail%size ? true : false;		}
template <class T> bool Queue<T>::Enqueue(T element)
{
	if (IsFull())
   	return false;
   data[tail] = element;
   tail = (tail+1) % size;
	return true;
}
template <class T> bool Queue<T>::Dequeue(T &element)
{
	if (IsEmpty())
   	return false;
	element = data[head];
   head = (head+1) % size;
	return true;
}



int main()
{
	return 0;
}