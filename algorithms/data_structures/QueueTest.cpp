template <class T> class Queue
{
	private:
   	T *data;
      int size,head,tail;
   public:
   	Queue(int elements);
      ~Queue(void);
      int  Inside(void);
      bool IsFull(void);
      bool IsEmpty(void);
      bool Enqueue(T element);
      bool Dequeue(T &element);
};
template <class T> Queue<T>::Queue(int elements)
{	data = new T[elements];
   size = elements;
   head = tail = 0;
}
template <class T> Queue<T>::~Queue(void)
{	delete[] data;		}
template <class T> Queue<T>::Inside(void)
{
	if (tail >= head)
   	return tail-head;
   else
   	return size-head+tail;
}
template <class T> bool Queue<T>::IsFull(void)
{	return head%size==(tail+1)%size ? true : false;		}
template <class T> bool Queue<T>::IsEmpty(void)
{	return head%size==tail%size ? true : false;		}
template <class T> bool Queue<T>::Enqueue(T element)
{
	if (IsFull())
   	return false;
   data[tail] = element;
   tail = (tail+1) % size;
	return true;
}
template <class T> bool Queue<T>::Dequeue(T &element)
{
	if (IsEmpty())
   	return false;
	element = data[head];
   head = (head+1) % size;
	return true;
}


#include <iostream.h>
#include <ctype.h>

void StrToLow(char *s)
{
	while (*s)
	{
   	*s = (char)tolower(*s);
   	s++;
   }
}

Queue<int> tq(10);
int tmp;
char com[256];

int main()
{
	cout << "Welcome to QUEUE TEST program" << endl << endl;
	for (;;)
   {
   	cout << "> ";
   	cin >> com;
      StrToLow(com);
      if (!strcmp(com,"quit"))
      	break;
      else if (!strcmp(com,"help"))
      {
      	cout << "----- HELP !!! HELP !!! HELP -----" << endl;
      	cout << "List of possible commands:" << endl;
         cout << "   Help      - this text" << endl;
         cout << "   Put       - puts element in queue (enqueue)" << endl;
         cout << "   Get       - gets element from queue (dequeue)" << endl;
         cout << "   IsFull    - tests if queue is full" << endl;
         cout << "   IsEmpty   - tests if queue is empty" << endl;
         cout << "   HowInside - returns number of elements actually queued" << endl;
      	cout << "   QUIT      - quits the program" << endl << endl;
      }
      else if (!strcmp(com,"put"))
      {
      	cout << "Enter element to be enqueued" << endl << "> ";
         cin >> tmp;
      	cout << "Put REPORT" << endl;
      	if (tq.Enqueue(tmp))
         	cout << "The element added to queue is: " << tmp << endl;
         else
         	cout << "Can't put element to queue - queue is probably full" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"get"))
      {
      	cout << "Get REPORT" << endl;
      	if (tq.Dequeue(tmp))
         	cout << "The element got from queue is: " << tmp << endl;
         else
         	cout << "Can't get element from queue - queue is probably empty" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"isfull"))
      {
			cout << "IsFull REPORT:" << endl;
         cout << "The queue " << (tq.IsFull() ? "is" : "isn't") << " full" << endl << endl;
      }
      else if (!strcmp(com,"isempty"))
      {
			cout << "IsEmpty REPORT:" << endl;
         cout << "The queue " << (tq.IsEmpty() ? "is" : "isn't") << " empty" << endl << endl;
      }
      else if (!strcmp(com,"howinside"))
      {
      	cout << "HowInside REPORT:" << endl;
         cout << "There are " << tq.Inside() << " elements queued" << endl << endl;
      }
      else
      	cout << "ERROR: Unknown command: " << endl << "   " << com << endl << endl;
   }
   cout << "Thanks for using QUEUE TEST program" << endl;

	return 0;
}