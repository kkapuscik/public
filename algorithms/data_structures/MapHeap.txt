MapHeap - C++ template class

This class represents a special Heap (the biggest element is on the top of the heap tree as in normal heap - but every element has also a key number assigned to it).

We could declare a map heap of ints that way:
MapHeap<int>
or a heap of strings:
MapHeap<string>

The requirements are:
- the <T> class must have defined the comparision operators (<,>,<=,>=)
- the key must be different for every element and must lie in range 0..SizeOfHeap-1

The implementation specification:

Constructor - build heap stack of maximal size 'elements' elements.
MapHeap(int elements)

Destructor - deletes all memory used by heap.
~MapHeap()

Function returns true if the heap is empty, false otherwise.
bool IsEmpty()

Function returns true if the heap is full, false otherwise.
bool IsFull()

Function returns number of elements actually in heap.
int Inside()

Function inserts new element in heap
bool Insert(T element, int key)

Function gets the maximum (top) element of the stack
bool GetMax(T &element, int &key)

Function gets and deletes from stack the maximum (top) element of the stack
bool ExtractMax(T &element, int &key)

Function increases value of element with key 'key' to new value 'element'
bool IncreaseKey(int key, T element)

