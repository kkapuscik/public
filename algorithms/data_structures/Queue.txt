Queue - C++ template class

This class represents a standard FIFO queue (first in - first out).

We could declare a queue of ints that way:
Queue<int>
or queue of strings:
Queue<string>

The implementation specification:

Constructor - build queue of maximal size 'elements' elements.
Queue(int elements)

Destructor - deletes all memory used by queue.
~Queue(void)

Function returns number of elements actually in queue.
int  Inside(void)

Function returns true if the queue is full, false otherwise.
bool IsFull(void)

Function returns true if the queue is empty, false otherwise.
bool IsEmpty(void)

Inserts new element in queue (false on error - full queue).
bool Enqueue(T element)

Returns (and deletes) element from queue (false on error - empty queue).
bool Dequeue(T &element)
