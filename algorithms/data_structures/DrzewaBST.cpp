#include <iostream.h>

#include "tree_bst.h"

VTreeBST<int> intree;

int main()
{
	int *i,j;
	int opt;
	bool end;

	for(end=false; !end;)
	{
		cout << "Select option:" << endl;
		cout << "1. Insert" << endl;
		cout << "2. Find" << endl;
		cout << "3. Delete" << endl;
		cout << "4. Remove" << endl;
		cout << "5. Print in order" << endl;
		cout << "0. Quit" << endl;
		cout << endl;
		cout << "> ";
		cin >> opt;
		switch(opt)
		{
			case 0:
				end = true;
				break;
			case 1:
				cout << "INSERT" << endl;
				cout << "Enter value to insert:" << endl;
				cout << "> ";
				cin >> j;
				i = new int(j);
				intree.Insert(i);
				cout << "Value " << *i << " inserted" << endl << endl << endl;
				break;
			case 2:
				cout << "FIND" << endl;
				cout << "Enter value to find:" << endl;
				cout << "> ";
				cin >> j;
				if (intree.Find(&j) != NULL)
					cout << "Value " << j << " found" << endl << endl << endl;
				else
					cout << "Value " << j << " not found" << endl << endl << endl;
					break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				intree.PrintInOrder();
				break;
			default:
				cout << "Bad option scecified" << endl << endl << endl;
				break;
		}
	}
	

	return 0;
}
