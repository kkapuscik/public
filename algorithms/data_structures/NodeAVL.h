//----------------------------------------------------------
// NodeAVL.H
//----------------------------------------------------------
// Implementation of AVL (balanced binary search tree) node
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------


#ifndef V_NODE_AVL_H
#define V_NODE_AVL_H

//----------------------------------------------------------
template <class T>
class VNodeAvl;
//----------------------------------------------------------

//----------------------------------------------------------
#include "TreeAVL.h"
#include <stdlib.h>
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VNodeAvl
{
	// klasa drzewa
	friend class VTreeAvl<T>;

public:
	// dane przechowywane przez obiekt
	// nie moga byc modyfikowane poniewaz
	// mogloby to wymagac zmiany struktury drzewa
	const T	Data;

private:
	// wskaznik na drzewo do ktorego nalezy obiekt
	VTreeAvl<T>*	OwnerTree;
	// wskaznik na prawego potomka obiektu
	VNodeAvl<T>*	Right;
	// wskaznik na lewego potomka obiektu
	VNodeAvl<T>*	Left;
	// wkaznik na rodzica obiektu
	VNodeAvl<T>*	Parent;
	// roznica w wysokosci drzew lewego i prawego
	char			Differ;

public:
	// konstuktor - tworzy obiekt i kopiuje do niego
	// podane dane
	VNodeAvl(const T data);

	// destruktor - nie moze byc wywolany zewnetrznie
	// do kasowania sluzy funkcja Delete
	~VNodeAvl();

public:
	// zwraca drzewo do ktorego nalezy obiekt
	VTreeAvl<T>* GetTree();
	// zwraca drzewo do ktorego nalezy obiekt
	const VTreeAvl<T>* GetTree() const;


	// zwraca lewego potomka
	VNodeAvl<T>* GetLeft();
	// zwraca lewego potomka
	const VNodeAvl<T>* GetLeft() const;
	// zwraca lewego potomka
	VNodeAvl<T>* GetRight();
	// zwraca lewego potomka
	const VNodeAvl<T>* GetRight() const;
	// zwraca lewego potomka
	VNodeAvl<T>* GetParent();
	// zwraca lewego potomka
	const VNodeAvl<T>* GetParent() const;


	// zwraca minimum w poddrzewie ktorego
	// korzeniem jest obiekt
	VNodeAvl<T>* GetSubMin();
	// zwraca minimum w poddrzewie ktorego
	// korzeniem jest obiekt
	const VNodeAvl<T>* GetSubMin() const;
	// zwraca maksimum w poddrzewie ktorego
	// korzeniem jest obiekt
	VNodeAvl<T>* GetSubMax();
	// zwraca maksimum w poddrzewie ktorego
	// korzeniem jest obiekt
	const VNodeAvl<T>* GetSubMax() const;


	// zwraca nastepnika obiektu
	VNodeAvl<T>* GetNext();
	// zwraca nastepnika obiektu
	const VNodeAvl<T>* GetNext() const;
	// zwraca poprzednika obiektu
	VNodeAvl<T>* GetPrev();
	// zwraca poprzednika obiektu
	const VNodeAvl<T>* GetPrev() const;

	
	// "wypina" obiekt z drzewa
	// tzn. wycina go ze struktury danych
	// ale nie zwalnia przydzielonej mu pamieci
	// (obiekt moze zostac pozniej wlaczony
	//  do tego samego lub innego drzewa)
	// zwraca false gdy obiekt nie nalezy
	// do zadnego drzewa
	bool Remove();

	// "wpina" obiekt do drzewa
	// tzn. wstawia go do struktury danych
	// zwraca false gdy obiekt jest juz przypisany
	// do jakiegos drzewa lub podane drzewo
	// nie istnieje (parametr rowny NULL)
	bool Link(VTreeAvl<T>* tree);

	// kasuje obiekt - obiekt nie moze by wiecej
	// uzywany poniewaz zwalniana jest jego pamiec!
	void Delete();
};
//----------------------------------------------------------


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>::VNodeAvl(const T data) :
	Data(data),
	OwnerTree(NULL),
	Left(NULL),Right(NULL),Parent(NULL),
	Differ(0)
{
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>::~VNodeAvl()
{
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VTreeAvl<T>* VNodeAvl<T>::GetTree()
{
	return OwnerTree;
}

//----------------------------------------------------------
template <class T>
const VTreeAvl<T>* VNodeAvl<T>::GetTree() const
{
	return OwnerTree;
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetLeft()
{
	return Left;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetLeft() const
{
	return Left;
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetRight()
{
	return Right;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetRight() const
{
	return Right;
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetParent()
{
	return Parent;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetParent() const
{
	return Parent;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetSubMin()
{
	VNodeAvl<T>* temp = const_cast<VNodeAvl<T>*>(this);
	while(temp->Left)
		temp = temp->Left;
	return temp;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetSubMin() const
{
	return const_cast<VNodeAvl<T>*>(this)->GetSubMin();
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetSubMax()
{
	VNodeAvl<T>* temp = const_cast<VNodeAvl<T>*>(this);
	while(temp->Right)
		temp = temp->Right;
	return temp;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetSubMax() const
{
	return const_cast<VNodeAvl<T>*>(this)->GetSubMax();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetPrev()
{
	if(Left != NULL)
		return Left->GetSubMax();

	VNodeAvl<T>* node = const_cast<VNodeAvl<T>*>(this);
	VNodeAvl<T>* par = Parent;
	while(par != NULL)
	{
		if(par->Left != node)
			break;
		else
		{
			par = par->Parent;
			node = node->Parent;
		}
	}
	return par;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetPrev() const
{
	return const_cast<VNodeAvl<T>*>(this)->GetPrev();
}

//----------------------------------------------------------
template <class T>
VNodeAvl<T>* VNodeAvl<T>::GetNext()
{
	if(Right != NULL)
		return Right->GetSubMin();

	VNodeAvl<T>* node = const_cast<VNodeAvl<T>*>(this);
	VNodeAvl<T>* par = Parent;
	while(par != NULL)
	{
		if(par->Right != node)
			break;
		else
		{
			par = par->Parent;
			node = node->Parent;
		}
	}
	return par;
}

//----------------------------------------------------------
template <class T>
const VNodeAvl<T>* VNodeAvl<T>::GetNext() const
{
	return const_cast<VNodeAvl<T>*>(this)->GetNext();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VNodeAvl<T>::Remove()
{
	if(OwnerTree)
	{
		OwnerTree->Remove(this);
		return true;
	}
	else
		return false;
}

//----------------------------------------------------------
template <class T>
bool VNodeAvl<T>::Link(VTreeAvl<T> *tree)
{
	if(!tree)
		return false;
	return OwnerTree->Insert(this)==this;
}

//----------------------------------------------------------
template <class T>
void VNodeAvl<T>::Delete()
{
	if(OwnerTree)
		OwnerTree->Delete(this);
}
//----------------------------------------------------------

#endif
