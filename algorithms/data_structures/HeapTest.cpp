template <class T> class Heap
{
	private:
   	T *data;
      int size, actual;

   private:
   	void HeapDown(int start);
      void HeapUp(int start);
   public:
   	Heap(int elements);
      ~Heap(void);
      bool IsEmpty(void);
      bool IsFull(void);
      int Inside(void);
      bool Insert(T element);
      bool GetMax(T &element);
      bool ExtractMax(T &element);
};
template <class T> Heap<T>::Heap(int elements)
{
	data = new T[elements];
   actual = 0;
   size = elements;
};
template <class T> Heap<T>::~Heap(void)
{	delete[] data;		}
template <class T> bool Heap<T>::Insert(T element)
{
	if (IsFull())
   	return false;
   data[actual++] = element;
   HeapUp(actual-1);
   return true;
}
template <class T> bool Heap<T>::GetMax(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[0];
   return true;
}
template <class T> bool Heap<T>::ExtractMax(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[0];
   data[0] = data[--actual];
   HeapDown(0);
   return true;
}
template <class T> void Heap<T>::HeapDown(int start)
{
	int left,right,bigest;
   T temp;
   for (;;)
   {
	   left = start*2+1;
      if (left >= actual)
      	break;
      bigest = start;
      if (data[left] > data[start])
			bigest = left;
      right = start*2+2;
      if (right < actual && data[right] > data[bigest])
      	bigest = right;
      if (bigest == start)
      	break;
     	temp = data[bigest];
      data[bigest] = data[start];
      data[start] = temp;
      start = bigest;
   }
}
template <class T> void Heap<T>::HeapUp(int start)
{
	int root;
   T temp;
   while (start > 0)
   {
   	root = (start-1)/2;
      if (data[root] < data[start])
      {
      	temp = data[root];
         data[root] = data[start];
         data[start] = temp;
         start = root;
      }
      else
      	break;
   }
}
template <class T> bool Heap<T>::IsEmpty(void)
{	return actual<=0 ? true : false;		}
template <class T> bool Heap<T>::IsFull(void)
{	return actual>=size ? true : false;	}
template <class T> int Heap<T>::Inside(void)
{	return actual;	}

#include <iostream.h>
#include <ctype.h>

void StrToLow(char *s)
{
	while (*s)
	{
   	*s = (char)tolower(*s);
   	s++;
   }
}

Heap<int> tq(10);
int tmp;
char com[256];

int main()
{
	cout << "Welcome to HEAP TEST program" << endl << endl;
	for (;;)
   {
   	cout << "> ";
   	cin >> com;
      StrToLow(com);
      if (!strcmp(com,"quit"))
      	break;
      else if (!strcmp(com,"help"))
      {
      	cout << "----- HELP !!! HELP !!! HELP -----" << endl;
      	cout << "List of possible commands:" << endl;
         cout << "   Help      - this text" << endl;
         cout << "   Insert    - puts element into stack" << endl;
         cout << "   GetMax    - returns the 'biggest' element without removing" << endl;
         cout << "   Extract   - returns the 'biggest' element REMOVING it" << endl;
         cout << "   IsFull    - tests if heap is full" << endl;
         cout << "   IsEmpty   - tests if heap is empty" << endl;
         cout << "   HowInside - returns number of elements actually in heap" << endl;
      	cout << "   QUIT      - quits the program" << endl << endl;
      }
      else if (!strcmp(com,"insert"))
      {
      	cout << "Enter element to insert to heap" << endl << "> ";
         cin >> tmp;
      	cout << "Insert REPORT" << endl;
      	if (tq.Insert(tmp))
         	cout << "The element added to stack is: " << tmp << endl;
         else
         	cout << "Can't put element to stack - stack is probably full" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"getmax"))
      {
      	cout << "GetMax REPORT" << endl;
      	if (tq.GetMax(tmp))
         	cout << "The element got from heap is: " << tmp << endl;
         else
         	cout << "Can't get element from heap - heap is probably empty" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"extract"))
      {
      	cout << "Extract REPORT" << endl;
      	if (tq.ExtractMax(tmp))
         	cout << "The element extracted from heap is: " << tmp << endl;
         else
         	cout << "Can't get element from heap - heap is probably empty" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"isfull"))
      {
			cout << "IsFull REPORT:" << endl;
         cout << "The heap " << (tq.IsFull() ? "is" : "isn't") << " full" << endl << endl;
      }
      else if (!strcmp(com,"isempty"))
      {
			cout << "IsEmpty REPORT:" << endl;
         cout << "The heap " << (tq.IsEmpty() ? "is" : "isn't") << " empty" << endl << endl;
      }
      else if (!strcmp(com,"howinside"))
      {
      	cout << "HowInside REPORT:" << endl;
         cout << "There are " << tq.Inside() << " elements in heap now" << endl << endl;
      }
      else
      	cout << "ERROR: Unknown command: " << endl << "   " << com << endl << endl;
   }
   cout << "Thanks for using HEAP TEST program" << endl;

	return 0;
}

