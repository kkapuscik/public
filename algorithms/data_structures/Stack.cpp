template <class T> class Stack
{
	private:
   	T *data;
      int size,actual;
   public:
   	Stack(int elements);
      ~Stack(void);
      int  Inside(void);
      bool IsFull(void);
      bool IsEmpty(void);
      bool Pop(T &element);
      bool Push(T element);
};
template <class T> Stack<T>::Stack(int elements)
{
	data = new T[elements];
   size = elements;
   actual = 0;
}
template <class T> Stack<T>::~Stack(void)
{	delete[] data;		}
template <class T> bool Stack<T>::IsFull(void)
{	return actual>=size ? true : false;		}
template <class T> bool Stack<T>::IsEmpty(void)
{	return actual<=0 ? true : false;		}
template <class T> int Stack<T>::Inside(void)
{	return actual;	}
template <class T> bool Stack<T>::Pop(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[--actual];
   return true;
}
template <class T> bool Stack<T>::Push(T element)
{
	if (IsFull())
   	return false;
   data[actual++] = element;
   return true;
}



int main()
{
	return 0;
}