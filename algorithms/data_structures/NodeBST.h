//----------------------------------------------------------
// NodeBST.H
//----------------------------------------------------------
// Implementation of BST (binary search tree) node
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------


#ifndef V_NODE_BST_H
#define V_NODE_BST_H

//----------------------------------------------------------
template <class T>
class VNodeBst;
//----------------------------------------------------------

//----------------------------------------------------------
#include "TreeBST.h"
#include <stdlib.h>
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VNodeBst
{
	// klasa drzewa
	friend class VTreeBst<T>;

public:
	// dane przechowywane przez obiekt
	// nie moga byc modyfikowane poniewaz
	// mogloby to wymagac zmiany struktury drzewa
	const T	Data;

private:
	// wskaznik na drzewo do ktorego nalezy obiekt
	VTreeBst<T>*	OwnerTree;
	// wskaznik na prawego potomka obiektu
	VNodeBst<T>*	Right;
	// wskaznik na lewego potomka obiektu
	VNodeBst<T>*	Left;
	// wkaznik na rodzica obiektu
	VNodeBst<T>*	Parent;

public:
	// konstuktor - tworzy obiekt i kopiuje do niego
	// podane dane
	VNodeBst(const T data);

	// destruktor - nie moze byc wywolany zewnetrznie
	// do kasowania sluzy funkcja Delete
	~VNodeBst();

public:
	// zwraca drzewo do ktorego nalezy obiekt
	VTreeBst<T>* GetTree();
	// zwraca drzewo do ktorego nalezy obiekt
	const VTreeBst<T>* GetTree() const;


	// zwraca lewego potomka
	VNodeBst<T>* GetLeft();
	// zwraca lewego potomka
	const VNodeBst<T>* GetLeft() const;
	// zwraca lewego potomka
	VNodeBst<T>* GetRight();
	// zwraca lewego potomka
	const VNodeBst<T>* GetRight() const;
	// zwraca lewego potomka
	VNodeBst<T>* GetParent();
	// zwraca lewego potomka
	const VNodeBst<T>* GetParent() const;


	// zwraca minimum w poddrzewie ktorego
	// korzeniem jest obiekt
	VNodeBst<T>* GetSubMin();
	// zwraca minimum w poddrzewie ktorego
	// korzeniem jest obiekt
	const VNodeBst<T>* GetSubMin() const;
	// zwraca maksimum w poddrzewie ktorego
	// korzeniem jest obiekt
	VNodeBst<T>* GetSubMax();
	// zwraca maksimum w poddrzewie ktorego
	// korzeniem jest obiekt
	const VNodeBst<T>* GetSubMax() const;


	// zwraca nastepnika obiektu
	VNodeBst<T>* GetNext();
	// zwraca nastepnika obiektu
	const VNodeBst<T>* GetNext() const;
	// zwraca poprzednika obiektu
	VNodeBst<T>* GetPrev();
	// zwraca poprzednika obiektu
	const VNodeBst<T>* GetPrev() const;

	
	// "wypina" obiekt z drzewa
	// tzn. wycina go ze struktury danych
	// ale nie zwalnia przydzielonej mu pamieci
	// (obiekt moze zostac pozniej wlaczony
	//  do tego samego lub innego drzewa)
	// zwraca false gdy obiekt nie nalezy
	// do zadnego drzewa
	bool Remove();

	// "wpina" obiekt do drzewa
	// tzn. wstawia go do struktury danych
	// zwraca false gdy obiekt jest juz przypisany
	// do jakiegos drzewa lub podane drzewo
	// nie istnieje (parametr rowny NULL)
	bool Link(VTreeBst<T>* tree);

	// kasuje obiekt - obiekt nie moze by wiecej
	// uzywany poniewaz zwalniana jest jego pamiec!
	void Delete();
};
//----------------------------------------------------------


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>::VNodeBst(const T data) :
	Data(data),
	OwnerTree(NULL),
	Left(NULL),Right(NULL),Parent(NULL)
{
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>::~VNodeBst()
{
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VTreeBst<T>* VNodeBst<T>::GetTree()
{
	return OwnerTree;
}

//----------------------------------------------------------
template <class T>
const VTreeBst<T>* VNodeBst<T>::GetTree() const
{
	return OwnerTree;
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetLeft()
{
	return Left;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetLeft() const
{
	return Left;
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetRight()
{
	return Right;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetRight() const
{
	return Right;
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetParent()
{
	return Parent;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetParent() const
{
	return Parent;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetSubMin()
{
	VNodeBst<T>* temp = const_cast<VNodeBst<T>*>(this);
	while(temp->Left)
		temp = temp->Left;
	return temp;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetSubMin() const
{
	return const_cast<VNodeBst<T>*>(this)->GetSubMin();
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetSubMax()
{
	VNodeBst<T>* temp = const_cast<VNodeBst<T>*>(this);
	while(temp->Right)
		temp = temp->Right;
	return temp;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetSubMax() const
{
	return const_cast<VNodeBst<T>*>(this)->GetSubMax();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetPrev()
{
	if(Left != NULL)
		return Left->GetSubMax();

	VNodeBst<T>* node = const_cast<VNodeBst<T>*>(this);
	VNodeBst<T>* par = Parent;
	while(par != NULL)
	{
		if(par->Left != node)
			break;
		else
		{
			par = par->Parent;
			node = node->Parent;
		}
	}
	return par;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetPrev() const
{
	return const_cast<VNodeBst<T>*>(this)->GetPrev();
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VNodeBst<T>::GetNext()
{
	if(Right != NULL)
		return Right->GetSubMin();

	VNodeBst<T>* node = const_cast<VNodeBst<T>*>(this);
	VNodeBst<T>* par = Parent;
	while(par != NULL)
	{
		if(par->Right != node)
			break;
		else
		{
			par = par->Parent;
			node = node->Parent;
		}
	}
	return par;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VNodeBst<T>::GetNext() const
{
	return const_cast<VNodeBst<T>*>(this)->GetNext();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VNodeBst<T>::Remove()
{
	if(OwnerTree)
	{
		OwnerTree->Remove(this);
		return true;
	}
	else
		return false;
}

//----------------------------------------------------------
template <class T>
bool VNodeBst<T>::Link(VTreeBst<T> *tree)
{
	if(!tree)
		return false;
	return OwnerTree->Insert(this)==this;
}

//----------------------------------------------------------
template <class T>
void VNodeBst<T>::Delete()
{
	if(OwnerTree)
		OwnerTree->Delete(this);
}
//----------------------------------------------------------

#endif
