#define KEYFREE -1

template <class T> class MapHeap
{
  private:
     T *data;
      int *where,*here;
      int size, actual;

   private:
     void HeapDown(int start);
      void HeapUp(int start);
   public:
     MapHeap(int elements);
      ~MapHeap(void);
      bool IncreaseKey(int key, T element);
      bool IsEmpty(void);
      bool IsFull(void);
      int Inside(void);
      bool Insert(T element, int key);
      bool GetMax(T &element,int &key);
      bool ExtractMax(T &element, int &key);
};
template <class T> MapHeap<T>::MapHeap(int elements)
{
  data = new T[elements];
  where = new int[elements];
   here = new int[elements];
   for (int cnt=0; cnt<elements; cnt++)
     where[cnt] = KEYFREE;
   actual = 0;
   size = elements;
};
template <class T> MapHeap<T>::~MapHeap(void)
{
  delete[] data;
   delete[] where;
   delete[] here;
}
template <class T> bool MapHeap<T>::Insert(T element, int key)
{
  if (IsFull() || key<0 || key>=size || where[key]!=KEYFREE)
     return false;
   data[actual++] = element;
   where[key] = actual-1;
   here[actual-1] = key;
   HeapUp(actual-1);
   return true;
}
template <class T> bool MapHeap<T>::GetMax(T &element,int &key)
{
  if (IsEmpty())
     return false;
   element = data[0];
   key = here[0];
   return true;
}
template <class T> bool MapHeap<T>::ExtractMax(T &element,int &key)
{
  if (IsEmpty())
     return false;
   element = data[0];
   key = here[0];

  where[here[0]] = KEYFREE;

   data[0] = data[--actual];
   here[0] = here[actual];
   where[here[0]] = 0;

   HeapDown(0);
   return true;
}
template <class T> void MapHeap<T>::HeapDown(int start)
{
  int left,right,bigest;
   int keytemp;
   T temp;
   for (;;)
   {
     left = start*2+1;
      if (left >= actual)
        break;
      bigest = start;
      if (data[left] > data[start])
      bigest = left;
      right = left+1;
      if (right < actual && data[right] > data[bigest])
        bigest = right;
      if (bigest == start)
        break;

       temp = data[bigest];
      data[bigest] = data[start];
      data[start] = temp;

      where[here[start]] = bigest;
      where[here[bigest]] = start;

      keytemp = here[start];
      here[start] = here[bigest];
      here[bigest] = keytemp;

      start = bigest;
   }
}
template <class T> void MapHeap<T>::HeapUp(int start)
{
  int root;
   T temp;
   int keytemp;
   while (start > 0)
   {
     root = (start-1)/2;
      if (data[root] < data[start])
      {
        temp = data[root];
         data[root] = data[start];
         data[start] = temp;

         where[here[start]] = root;
         where[here[root]] = start;

         keytemp = here[start];
         here[start] = here[root];
         here[root] = keytemp;

         start = root;
      }
      else
        break;
   }
}
template <class T> bool MapHeap<T>::IsEmpty(void)
{
  if (actual <= 0)
     return true;
   else
     return false;
}
template <class T> bool MapHeap<T>::IsFull(void)
{
  if (actual >= size)
     return true;
   else
     return false;
}
template <class T> bool MapHeap<T>::IncreaseKey(int key, T element)
{
  if (key<0 || key>=size || where[key]==KEYFREE || data[where[key]]>=element)
     return false;
   data[where[key]] = element;
   HeapUp(where[key]);
   return true;
}
template <class T> int MapHeap<T>::Inside(void)
{  return actual;  }



int main()
{
  return 0;
}
