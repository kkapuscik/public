//----------------------------------------------------------
// TreeBST.H
//----------------------------------------------------------
// Implementation of BST (binary search tree)
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------


#ifndef V_TREE_BST_H
#define V_TREE_BST_H

//----------------------------------------------------------
template <class T>
class VTreeBst;
//----------------------------------------------------------

//----------------------------------------------------------
#include "NodeBST.h"
#include <stdlib.h>
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VTreeBst
{
private:
	// wskaznik na korzen drzewa
	VNodeBst<T>	*Root;


public:
	// konstruktor - tworzy puste drzewo
	VTreeBst();
	// destruktor - zwalnia wszystkie obiekty
	~VTreeBst();

public:
	// funkcja kasujaca wszystkie obiekty
	// znajdujace sie w drzewie
	void DeleteAll();
private:
	// funkcja pomocnicza do kasowania wszystkich
	// obiektow znajdujacych sie w drzewie
	void DeleteAllProc(VNodeBst<T>* node);


public:
	// funkcja kasuj�ca obiekt z drzewa
	// moze zwrocic false kiedy parametr
	// 'node' jest rowny NULL lub
	// kiedy 'node' nie nalezy do drzewa
	bool Delete(VNodeBst<T>* node);


	// funkcja dolaczajaca element do drzewa
	VNodeBst<T>* Insert(VNodeBst<T>* node);

	// funkcja tworz�ca nowy element
	// i dolaczajaca go do drzewa
	VNodeBst<T>* Insert(const T data);


	// funkcja "wycinajaca" element z drzewa
	// element zostaje usuniety z drzewa, ale
	// jego pamiec nie zostaje zwolniona
	// (mozna go pozniej np. dolaczyc
	// funkcja Insert
	bool Remove(VNodeBst<T>* node);


	// funkcja odnajdujaca element w drzewie
	// UWAGA: Funkcja zwraca pierwszy napotkany element
	// spelniajacy zadane warunki (najblizszy korzeniowi)
	VNodeBst<T>* Find(const T data);
	// funkcja odnajdujaca element w drzewie
	// UWAGA: Funkcja zwraca pierwszy napotkany element
	// spelniajacy zadane warunki (najblizszy korzeniowi)
	const VNodeBst<T>* Find(const T data) const;


	// funkcja zwracajaca korzen drzewa
	VNodeBst<T>* GetRoot();
	// funkcja zwracajaca korzen drzewa
	const VNodeBst<T>* GetRoot() const;


	// funkcja sprawdzajaca czy
	// element nalezy do drzewa
	bool IsTree(const VNodeBst<T>* node) const;


	// zwraca minimum w drzewie
	VNodeBst<T>* GetMin();
	// zwraca minimum w drzewie
	const VNodeBst<T>* GetMin() const;

	// zwraca maksimum w drzewie
	VNodeBst<T>* GetMax();
	// zwraca maksimum w drzewie
	const VNodeBst<T>* GetMax() const;
};
//----------------------------------------------------------


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VTreeBst<T>::VTreeBst() :
	Root(NULL)
{
}

//----------------------------------------------------------
template <class T>
VTreeBst<T>::~VTreeBst()
{
	DeleteAll();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VTreeBst<T>::DeleteAll()
{
	DeleteAllProc(Root);
	Root = NULL;
}

//----------------------------------------------------------
template <class T>
void VTreeBst<T>::DeleteAllProc(VNodeBst<T>* node)
{
	if(node)
	{
		if(node->Left)
			DeleteAllProc(node->Left);
		if(node->Right)
			DeleteAllProc(node->Right);
		delete node;
	}
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeBst<T>::Delete(VNodeBst<T>* node)
{
	if(node->Remove())
	{
		delete node;
		return true;
	}
	return false;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::Insert(VNodeBst<T>* node)
{
	if(!node || node->OwnerTree)
		return NULL;
	
	node->OwnerTree = this;

	if(Root)
	{
		VNodeBst<T>* parent = Root;
		for(;;)
		{
			if(node->Data <= parent->Data)
			{
				if(parent->Left)
					parent = parent->Left;
				else
				{
					parent->Left = node;
					node->Parent = parent;
					break;
				}
			}
			else
			{
				if(parent->Right)
					parent = parent->Right;
				else
				{
					parent->Right = node;
					node->Parent = parent;
					break;
				}
			}
		}
	}
	else
		Root = node;
	return node;
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::Insert(const T data)
{
	VNodeBst<T>* newnode;
	// podwojny test na NULL i na throw exception
	// poniewaz rozne kompilatory roznie robia
	try
	{
		newnode = new VNodeBst<T>(data);
	}
	catch(...)
	{
		newnode = NULL;
	}
	if(newnode)
	{
		if(Insert(newnode))
			return newnode;
		delete newnode;
	}
	return NULL;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeBst<T>::Remove(VNodeBst<T>* node)
{
	if(!node || node->OwnerTree != this)
		return false;

	VNodeBst<T> *todel,*restson;

	if(node->Left==NULL || node->Right==NULL)
		todel = node;
	else
		todel = node->GetNext();

	if(todel->Left != NULL)
		restson = todel->Left;
	else
		restson = todel->Right;

	if(restson!=NULL)
		restson->Parent = todel->Parent;
	if(todel->Parent == NULL)
		Root = restson;
	else
	{
		if(todel->Parent->Left == todel)
			todel->Parent->Left = restson;
		else
			todel->Parent->Right = restson;
	}
	if(todel != node)
	{
		// relink from todel to node
		todel->Parent = node->Parent;
		if(todel->Parent)
		{
			if(todel->Parent->Left == node)
				todel->Parent->Left = todel;
			else
				todel->Parent->Right = todel;
		}
		todel->Left = node->Left;
		if(todel->Left)
			todel->Left->Parent = todel;
		todel->Right = node->Right;
		if(todel->Right)
			todel->Right->Parent = todel;
	}

	node->OwnerTree = NULL;
	node->Left = node->Right = node->Parent = NULL;

	return true;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::GetMin()
{
	return Root->GetSubMin();
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VTreeBst<T>::GetMin() const
{
	return Root->GetSubMin();
}

//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::GetMax()
{
	return Root->GetSubMax();
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VTreeBst<T>::GetMax() const
{
	return Root->GetSubMax();
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::Find(const T data)
{
	VNodeBst<T>* node = Root;
	while(node)
	{
		if(node->Data == data)
			return node;
		else if (node->Data < data)
			node = node->Right;
		else
			node = node->Left;
	}
	return NULL;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VTreeBst<T>::Find(const T data) const
{
	return const_cast<VTreeBst*>(this)->Find(data);
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VNodeBst<T>* VTreeBst<T>::GetRoot()
{
	return Root;
}

//----------------------------------------------------------
template <class T>
const VNodeBst<T>* VTreeBst<T>::GetRoot() const
{
	return Root;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VTreeBst<T>::IsTree(const VNodeBst<T>* node) const
{
	if(node && node->OwnerTree == this)
		return true;
	return false;
}


//----------------------------------------------------------
//----------------------------------------------------------

#endif
