template <class T> class Stack
{
	private:
   	T *data;
      int size,actual;
   public:
   	Stack(int elements);
      ~Stack(void);
      int  Inside(void);
      bool IsFull(void);
      bool IsEmpty(void);
      bool Pop(T &element);
      bool Push(T element);
};
template <class T> Stack<T>::Stack(int elements)
{
	data = new T[elements];
   size = elements;
   actual = 0;
}
template <class T> Stack<T>::~Stack(void)
{	delete[] data;		}
template <class T> bool Stack<T>::IsFull(void)
{	return actual>=size ? true : false;		}
template <class T> bool Stack<T>::IsEmpty(void)
{	return actual<=0 ? true : false;		}
template <class T> int Stack<T>::Inside(void)
{	return actual;	}
template <class T> bool Stack<T>::Pop(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[--actual];
   return true;
}
template <class T> bool Stack<T>::Push(T element)
{
	if (IsFull())
   	return false;
   data[actual++] = element;
   return true;
}


#include <iostream.h>
#include <ctype.h>

void StrToLow(char *s)
{
	while (*s)
	{
   	*s = (char)tolower(*s);
   	s++;
   }
}

Stack<int> tq(10);
int tmp;
char com[256];

int main()
{
	cout << "Welcome to STACK TEST program" << endl << endl;
	for (;;)
   {
   	cout << "> ";
   	cin >> com;
      StrToLow(com);
      if (!strcmp(com,"quit"))
      	break;
      else if (!strcmp(com,"help"))
      {
      	cout << "----- HELP !!! HELP !!! HELP -----" << endl;
      	cout << "List of possible commands:" << endl;
         cout << "   Help      - this text" << endl;
         cout << "   Push      - puts element on stack" << endl;
         cout << "   Pop       - gets element from stack" << endl;
         cout << "   IsFull    - tests if stack is full" << endl;
         cout << "   IsEmpty   - tests if stack is empty" << endl;
         cout << "   HowInside - returns number of elements actually on stack" << endl;
      	cout << "   QUIT      - quits the program" << endl << endl;
      }
      else if (!strcmp(com,"push"))
      {
      	cout << "Enter element to be stored" << endl << "> ";
         cin >> tmp;
      	cout << "Push REPORT" << endl;
      	if (tq.Push(tmp))
         	cout << "The element added to stack is: " << tmp << endl;
         else
         	cout << "Can't put element on stack - stack is probably full" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"pop"))
      {
      	cout << "Pop REPORT" << endl;
      	if (tq.Pop(tmp))
         	cout << "The element got from stack is: " << tmp << endl;
         else
         	cout << "Can't get element from stack - stack is probably empty" << endl;
			cout << endl;
      }
      else if (!strcmp(com,"isfull"))
      {
			cout << "IsFull REPORT:" << endl;
         cout << "The stack " << (tq.IsFull() ? "is" : "isn't") << " full" << endl << endl;
      }
      else if (!strcmp(com,"isempty"))
      {
			cout << "IsEmpty REPORT:" << endl;
         cout << "The stack " << (tq.IsEmpty() ? "is" : "isn't") << " empty" << endl << endl;
      }
      else if (!strcmp(com,"howinside"))
      {
      	cout << "HowInside REPORT:" << endl;
         cout << "There are " << tq.Inside() << " elements stored in stack" << endl << endl;
      }
      else
      	cout << "ERROR: Unknown command: " << endl << "   " << com << endl << endl;
   }
   cout << "Thanks for using STACK TEST program" << endl;

	return 0;
}
