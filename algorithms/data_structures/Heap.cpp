template <class T> class Heap
{
	private:
   	T *data;
      int size, actual;

   private:
   	void HeapDown(int start);
      void HeapUp(int start);
   public:
   	Heap(int elements);
      ~Heap(void);
      bool IsEmpty(void);
      bool IsFull(void);
      int Inside(void);
      bool Insert(T element);
      bool GetMax(T &element);
      bool ExtractMax(T &element);
};
template <class T> Heap<T>::Heap(int elements)
{
	data = new T[elements];
   actual = 0;
   size = elements;
};
template <class T> Heap<T>::~Heap(void)
{	delete[] data;		}
template <class T> bool Heap<T>::Insert(T element)
{
	if (IsFull())
   	return false;
   data[actual++] = element;
   HeapUp(actual-1);
   return true;
}
template <class T> bool Heap<T>::GetMax(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[0];
   return true;
}
template <class T> bool Heap<T>::ExtractMax(T &element)
{
	if (IsEmpty())
   	return false;
   element = data[0];
   data[0] = data[--actual];
   HeapDown(0);
   return true;
}
template <class T> void Heap<T>::HeapDown(int start)
{
	int left,right,bigest;
   T temp;
   for (;;)
   {
	   left = start*2+1;
      if (left >= actual)
      	break;
      bigest = start;
      if (data[left] > data[start])
			bigest = left;
      right = start*2+2;
      if (right < actual && data[right] > data[bigest])
      	bigest = right;
      if (bigest == start)
      	break;
     	temp = data[bigest];
      data[bigest] = data[start];
      data[start] = temp;
      start = bigest;
   }
}
template <class T> void Heap<T>::HeapUp(int start)
{
	int root;
   T temp;
   while (start > 0)
   {
   	root = (start-1)/2;
      if (data[root] < data[start])
      {
      	temp = data[root];
         data[root] = data[start];
         data[start] = temp;
         start = root;
      }
      else
      	break;
   }
}
template <class T> bool Heap<T>::IsEmpty(void)
{	return actual<=0 ? true ; false;		}
template <class T> bool Heap<T>::IsFull(void)
{	return actual>=size ? true : false;	}
template <class T> int Heap<T>::Inside(void)
{	return actual;	}



int main()
{
	return 0;
}

