//----------------------------------------------------------
// LIST.H
//----------------------------------------------------------
// Implementation of two directional list
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------

#ifndef V_LIST_H
#define V_LIST_H

//----------------------------------------------------------
template <class T>
class VList;
//----------------------------------------------------------

//----------------------------------------------------------
#include <stdlib.h>
#include "listelem.h"
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VList
{
// ----- FIELDS -----
private:
	VListElem<T>	*m_Head,*m_Tail;

// ----- METHODS -----
public:
	// konstruktor - tworzy pust� list�
	VList();
	// destruktor - usuwa wszystkie
	// elementy listy
	~VList();

	// czysci liste w wyniku czego
	// jest ona pusta
	void Clear();

	// usuwa element z listy
	// zwalniajac jego pamiec
	bool Delete(VListElem<T>* elem);
	// wycina element z listy, ale nie
	// zwalnia jego pamieci
	// element moze zostac ponownie
	// wlaczony do listy funkcjami
	// InsertHead/Tail/Before/After,
	bool Remove(VListElem<T>* elem);

	// wyszukuje w liscie element
	// zawierajacy podane dane
	VListElem<T>* Find(const T data);
	// wyszukuje w liscie element
	// zawierajacy podane dane
	const VListElem<T>* Find(const T data) const;
	// wyszukuje w liscie element
	// zawierajacy podane dane
	// znajdujacy sie za elementem 'last'
	// (element last wchodzi takze do poszukiwan
	// jesli parametr 'include' jest rowny true)
	VListElem<T>* FindAfter(const T data,const VListElem<T>* last,bool include);
	// wyszukuje w liscie element
	// zawierajacy podane dane
	// znajdujacy sie za elementem 'last'
	// (element last wchodzi takze do poszukiwan
	// jesli parametr 'include' jest rowny true)
	const VListElem<T>* FindAfter(const T data,const VListElem<T>* last,bool include) const;
	// wyszukuje w liscie element
	// zawierajacy podane dane
	// znajdujacy sie przed elementem 'last'
	// (element last wchodzi takze do poszukiwan
	// jesli parametr 'include' jest rowny true)
	VListElem<T>* FindBefore(const T data,const VListElem<T>* last,bool include);
	// wyszukuje w liscie element
	// zawierajacy podane dane
	// znajdujacy sie przed elementem 'last'
	// (element last wchodzi takze do poszukiwan
	// jesli parametr 'include' jest rowny true)
	const VListElem<T>* FindBefore(const T data,const VListElem<T>* last,bool include) const;

	// zwraca pierwszy element listy
	VListElem<T>* GetHead();
	// zwraca pierwszy element listy
	const VListElem<T>* GetHead() const;
	// zwraca ostatni element listy
	VListElem<T>* GetTail();
	// zwraca ostatni element listy
	const VListElem<T>* GetTail() const;

	// sprawdza czy element znajduje
	// sie w liscie
	bool IsInList(const VListElem<T>* elem) const;

	// tworzy i wstawia element na poczatek listy
	VListElem<T>* InsertHead(const T data);
	// tworzy i wstawia element na koniec listy
	VListElem<T>* InsertTail(const T data);
	// tworzy i wstawia element za element 'elem'
	// element 'elem' musi znajdowac sie na liscie
	VListElem<T>* InsertAfter(const T data,VListElem<T>* elem);
	// tworzy i wstawia element przed element 'elem'
	// element 'elem' musi znajdowac sie na liscie
	VListElem<T>* InsertBefore(const T data,VListElem<T>* elem);

	// wstawia element na poczatek listy
	// element nie moze znajdowac
	// sie na zadnej licie
	bool InsertHead(VListElem<T>* elem);
	// wstawia element na koniec listy
	// element nie moze znajdowac
	// sie na zadnej licie
	bool InsertTail(VListElem<T>* elem);
	// wstawia element za element 'elem'
	// element 'elem' musi znajdowac sie na liscie
	// element 'newelem' nie moze znajdowac
	// sie na zadnej licie
	bool InsertAfter(VListElem<T>* newelem,VListElem<T>* elem);
	// wstawia element przed element 'elem'
	// element 'elem' musi znajdowac sie na liscie
	// element 'newelem' nie moze znajdowac
	// sie na zadnej licie
	bool InsertBefore(VListElem<T>* newelem,VListElem<T>* elem);

	// sortuje rosnaco liste
	// por�wnuj�c operatorem '<'
	void Sort();

	// sortuje rosn�co liste
	// porownuje funkcj� CompFunc
	// funkcja powinna zwraca�:
	// wartosc<0 = gdy element Data1 < Data2
	//         0 = gdy element Data1 == Data2
	// wartosc>0 = gdy element Data1 > Data2
	void Sort(int (*CompFunc)(const T& Data1, const T& Data2));

	// podaje ilosc elementow na liscie
	int CountElems() const;

private:
	// tworzy nowy element zawierajacy
	// podane jako parametr dane
	VListElem<T>* CreateNewElem(const T data);
};
//----------------------------------------------------------

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VList<T>::VList() :
	m_Head(NULL),m_Tail(NULL)
{
}

//----------------------------------------------------------
template <class T>
VList<T>::~VList()
{
	Clear();
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VList<T>::Clear()
{
	while(m_Head)
		Delete(m_Head);
}

//----------------------------------------------------------
template <class T>
bool VList<T>::Delete(VListElem<T>* elem)
{
	if(Remove(elem))
	{
		delete elem;
		return true;
	}
	else
		return false;
}

//----------------------------------------------------------
template <class T>
bool VList<T>::Remove(VListElem<T>* elem)
{
	// test if belong to list
	if(!elem || elem->m_ParentList != this)
		return false;

	// test if it is head/tail and relink if needed
	if(elem == m_Head)
		m_Head = elem->m_Next;
	if(elem == m_Tail)
		m_Tail = elem->m_Prev;

	// relinking
	if(elem->m_Next)
		elem->m_Next->m_Prev = elem->m_Prev;
	if(elem->m_Prev)
		elem->m_Prev->m_Next = elem->m_Next;

	// remove
	elem->m_Next = elem->m_Prev = NULL;
	elem->m_ParentList = NULL;

	// all OK
	return true;
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::Find(const T data)
{
	VListElem<T>* temp = m_Head;
	while(temp)
	{
		if(temp->m_Data == data)
			return temp;
		temp = temp->m_Next;
	}
	return NULL;
}

//----------------------------------------------------------
template <class T>
const VListElem<T>* VList<T>::Find(const T data) const
{
	VListElem<T>* temp = m_Head;
	while(temp)
	{
		if(temp->m_Data == data)
			return temp;
		temp = temp->m_Next;
	}
	return NULL;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::FindAfter(const T data,const VListElem<T>* last,bool include)
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->m_Next;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->m_Next;
	}
	return last;
}

//----------------------------------------------------------
template <class T>
const VListElem<T>* VList<T>::FindAfter(const T data,const VListElem<T>* last,bool include) const
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->m_Next;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->m_Next;
	}
	return last;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::FindBefore(const T data,const VListElem<T>* last,bool include)
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->m_Prev;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->m_Prev;
	}
	return last;
}

//----------------------------------------------------------
template <class T>
const VListElem<T>* VList<T>::FindBefore(const T data,const VListElem<T>* last,bool include) const
{
	if(!last || last->m_ParentList != this)
		return NULL;
	if(!include)
		last = last->m_Prev;
	while(last)
	{
		if(last->m_Data == data)
			return last;
		last = last->m_Prev;
	}
	return last;
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::GetHead()
{
	return m_Head;
}
//----------------------------------------------------------
template <class T>
const VListElem<T>* VList<T>::GetHead() const
{
	return m_Head;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::GetTail()
{
	return m_Tail;
}
//----------------------------------------------------------
template <class T>
const VListElem<T>* VList<T>::GetTail() const
{
	return m_Tail;
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
bool VList<T>::IsInList(const VListElem<T>* elem) const
{
	if(!elem)
		return false;
	return elem->m_ParentList == this;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::InsertHead(const T data)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		if (InsertHead(newelem))
			return newelem;
		else
			delete newelem;
	return NULL;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::InsertTail(const T data)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		if (InsertTail(newelem))
			return newelem;
		else
			delete newelem;
	return NULL;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::InsertAfter(const T data,VListElem<T>* elem)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		if (InsertAfter(newelem,elem))
			return newelem;
		else
			delete newelem;
	return NULL;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::InsertBefore(const T data,VListElem<T>* elem)
{
	VListElem<T> *newelem;

	newelem = CreateNewElem(data);
	if(newelem)
		if (InsertBefore(newelem,elem))
			return newelem;
		else
			delete newelem;
	return NULL;
}

	
//----------------------------------------------------------
template <class T>
bool VList<T>::InsertHead(VListElem<T>* elem)
{
	// test if there is an element
	// and if it is linked to any list
	if(!elem || elem->m_ParentList)
		return false;
	// linking
	elem->m_ParentList = this;
	if(m_Head)
	{
		m_Head->m_Prev = elem;
		elem->m_Next = m_Head;
	}
	else
		m_Tail = elem;
	m_Head = elem;
	return true;
}

//----------------------------------------------------------
template <class T>
bool VList<T>::InsertTail(VListElem<T>* elem)
{
	// test if there is an element
	// and if it is linked to any list
	if(!elem || elem->m_ParentList)
		return false;
	// linking
	elem->m_ParentList = this;
	if(m_Tail)
	{
		m_Tail->m_Next = elem;
		elem->m_Prev = m_Tail;
	}
	else
		m_Head = elem;
	m_Tail = elem;
	return true;
}

//----------------------------------------------------------
template <class T>
bool VList<T>::InsertAfter(VListElem<T>* newelem,VListElem<T>* elem)
{
	// check
	if(!elem || !newelem || elem->m_ParentList!=this
		|| newelem->ParentList)
		return false;

	// relink
	newelem->m_ParentList = this;
	newelem->m_Prev = elem;
	newelem->m_Next = elem->m_Next;

	if(elem->m_Next)
		elem->m_Next->m_Prev = newelem;
	elem->m_Next = newelem;

	if(elem == m_Tail)
		m_Tail = newelem;

	// all OK
	return true;
}

//----------------------------------------------------------
template <class T>
bool VList<T>::InsertBefore(VListElem<T>* newelem,VListElem<T>* elem)
{
	// check
	if(!elem || !newelem || elem->m_ParentList!=this
		|| newelem->ParentList)
		return false;

	// relink
	newelem->m_ParentList = this;
	newelem->m_Next = elem;
	newelem->m_Prev = elem->m_Prev;

	if(elem->m_Prev)
		elem->m_Prev->m_Next = newelem;
	elem->m_Prev = newelem;

	if(elem == m_Head)
		m_Head = newelem;

	// all OK
	return true;
}


//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VList<T>::CreateNewElem(const T data)
{
	// podwojny test na NULL oraz throw(..)
	// bo rozne kompilatory roznie robia
	VListElem<T> *newelem;
	try
	{
		newelem = new VListElem<T>(data);
	}
	catch(...)
	{
		newelem = NULL;
	}
	return newelem;
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VList<T>::Sort()
{
	VListElem<T>* a;
	VListElem<T>* b;
	VListElem<T>* rest;
	VListElem<T>* elem;

	int i;
	int total = CountElems();
	int chunk = 1;
	while(chunk < total)
	{
		// przechwytujemy liste
		// elementow
		rest = m_Head;

		// resetujemy rodzicow
		elem = rest;
		while(elem)
		{
			elem->m_ParentList = NULL;
			elem = elem->m_Next;
		}

		// a prawdziwa liste czyscimy
		m_Head = m_Tail = NULL;

		while(rest)
		{
			// wskaznik na pierwsza podliste
			a = rest;
			for(i=0; i<chunk && rest; i++)
				rest = rest->m_Next;
			// odcinamy pierwsza podliste
			if(rest)
			{
				rest->m_Prev->m_Next = NULL;
				rest->m_Prev = NULL;
			}

			// wskaznik na druga podliste
			b = rest;
			for(i=0; i<chunk && rest; i++)
				rest = rest->m_Next;
			// odcinamy pierwsza podliste
			if(rest)
			{
				rest->m_Prev->m_Next = NULL;
				rest->m_Prev = NULL;
			}

			// sortujemy
			while(a && b)
			{
				if(a->m_Data < b->m_Data)
				{
					elem = a;
					if((a = a->m_Next) != NULL)
					{
						a->m_Prev->m_Next = NULL;
						a->m_Prev = NULL;
					}
					InsertTail(elem);
				}
				else
				{
					elem = b;
					if((b = b->m_Next) != NULL)
					{
						b->m_Prev->m_Next = NULL;
						b->m_Prev = NULL;
					}
					InsertTail(elem);
				}
			}
			while(a)
			{
				elem = a;
				if((a = a->m_Next) != NULL)
				{
					a->m_Prev->m_Next = NULL;
					a->m_Prev = NULL;
				}
				InsertTail(elem);
			}
			while(b)
			{
				elem = b;
				if((b = b->m_Next) != NULL)
				{
					b->m_Prev->m_Next = NULL;
					b->m_Prev = NULL;
				}
				InsertTail(elem);
			}
		}

		chunk *= 2;
	}
}

//----------------------------------------------------------
template <class T>
void VList<T>::Sort(int (*CompFunc)(const T& Data1, const T& Data2))
{
	VListElem<T>* a;
	VListElem<T>* b;
	VListElem<T>* rest;
	VListElem<T>* elem;

	int i;
	int total = CountElems();
	int chunk = 1;
	while(chunk < total)
	{
		// przechwytujemy liste
		// elementow
		rest = m_Head;

		// resetujemy rodzicow
		elem = rest;
		while(elem)
		{
			elem->m_ParentList = NULL;
			elem = elem->m_Next;
		}

		// a prawdziwa liste czyscimy
		m_Head = m_Tail = NULL;

		while(rest)
		{
			// wskaznik na pierwsza podliste
			a = rest;
			for(i=0; i<chunk && rest; i++)
				rest = rest->m_Next;
			// odcinamy pierwsza podliste
			if(rest)
			{
				rest->m_Prev->m_Next = NULL;
				rest->m_Prev = NULL;
			}

			// wskaznik na druga podliste
			b = rest;
			for(i=0; i<chunk && rest; i++)
				rest = rest->m_Next;
			// odcinamy pierwsza podliste
			if(rest)
			{
				rest->m_Prev->m_Next = NULL;
				rest->m_Prev = NULL;
			}

			// sortujemy
			while(a && b)
			{
				if(CompFunc(a->m_Data,b->m_Data)<0)
				{
					elem = a;
					if((a = a->m_Next) != NULL)
					{
						a->m_Prev->m_Next = NULL;
						a->m_Prev = NULL;
					}
					InsertTail(elem);
				}
				else
				{
					elem = b;
					if((b = b->m_Next) != NULL)
					{
						b->m_Prev->m_Next = NULL;
						b->m_Prev = NULL;
					}
					InsertTail(elem);
				}
			}
			while(a)
			{
				elem = a;
				if((a = a->m_Next) != NULL)
				{
					a->m_Prev->m_Next = NULL;
					a->m_Prev = NULL;
				}
				InsertTail(elem);
			}
			while(b)
			{
				elem = b;
				if((b = b->m_Next) != NULL)
				{
					b->m_Prev->m_Next = NULL;
					b->m_Prev = NULL;
				}
				InsertTail(elem);
			}
		}

		chunk *= 2;
	}
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
int VList<T>::CountElems() const
{
	VListElem<T>* elem = m_Head;
	int total = 0;
	while(elem)
	{
		total++;
		elem = elem->m_Next;
	}
	return total;
}

//----------------------------------------------------------
//----------------------------------------------------------
#endif
