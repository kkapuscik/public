#include <iostream>
#include <algorithm>
#include <conio.h>
using namespace std;

#include "TreeAVL.h"


template <class T>
void PrintStructProc(VNodeAvl<T>* node,int indent,char *sign)
{
	// output indent
	for(int i=0; i<indent; i++)
		cout << ' ';
	cout << sign << ": ";

	if(!node)
	{
		cout << "<<none>>" << endl;
		return;
	}
	
	cout << node->Data << endl;

	if(node->GetLeft() || node->GetRight())
	{
		PrintStructProc(node->GetLeft(),indent + 2,"Left");
		PrintStructProc(node->GetRight(),indent + 2,"Rght");
	}
}

template <class T>
void PrintStruct(VTreeAvl<T>* tree)
{
	cout << "Tree structure:" << endl;
	PrintStructProc(tree->GetRoot(),0,"Root");
	cout << endl;
}


template <class T>
void PrintInOrderProc(VNodeAvl<T>* node)
{
	// output indent
	if(!node)
		return;
	
	PrintInOrderProc(node->GetLeft());
	cout << "  " << node->Data << endl;
	PrintInOrderProc(node->GetRight());
}

template <class T>
void PrintInOrder(VTreeAvl<T>* tree)
{
	cout << "In-order tree contents:" << endl;
	PrintInOrderProc(tree->GetRoot());
	cout << endl;
}



template <class T>
void PrintHeightProc(VNodeAvl<T>* node,int& minh,int& maxh)
{
	if(!node)
	{
		minh = maxh = 0;
		return;
	}
	int lmin,lmax,rmin,rmax;
	PrintHeightProc(node->GetLeft(),lmin,lmax);
	PrintHeightProc(node->GetRight(),rmin,rmax);
	minh = _cpp_min(lmin,rmin)+1;
	maxh = _cpp_max(lmax,rmax)+1;
}

template <class T>
void PrintHeight(VTreeAvl<T>* tree)
{
	int minh,maxh;
	PrintHeightProc(tree->GetRoot(),minh,maxh);
	cout << "Tree paths details:" << endl;
	cout << "  Shortest path = " << minh << endl;
	cout << "  Longest path  = " << maxh << endl;
	cout << "  Difference    = " << (maxh-minh) << endl;
	cout << endl;
}







VTreeAvl<int> mytree;

int main()
{
	int option,value;
	VNodeAvl<int>* node;
	bool end = false;

	while(!end)
	{
		cout << endl;
		cout << "Options:" << endl;
		cout << "  1. Insert" << endl;
		cout << "  2. Find"   << endl;
		cout << "  3. Delete" << endl;
		cout << "  4. Remove & relink" << endl;
		cout << "  5. Get maximum" << endl;
		cout << "  6. Get mimimum" << endl;
		cout << "  7. Print structure" << endl;
		cout << "  8. Print in-order"  << endl;
		cout << "  9. Print height report" << endl;
		cout << "  0. Quit" << endl;
		cout << endl;
		cout << "> ";

		cin >> option;
		cout << endl;
		
		switch(option)
		{
		case 1:
			cout << "Enter value to insert:" << endl << "> ";
			cin >> value;

			if (node = mytree.Insert(value))
			{
				if(node->Data == value)
					cout << "Insert complete" << endl;
				else
					cout << "Insert value error" << endl;
			}
			else
				cout << "Insert failed" << endl;
			break;

		case 2:
			cout << "Enter value to find:" << endl << "> ";
			cin >> value;

			if (node = mytree.Find(value))
			{
				if(node->Data == value)
					cout << "Value " << value << " found" << endl;
				else
					cout << "Find value error" << endl;
			}
			else
				cout << "Search failed" << endl;
			break;

		case 3:
			cout << "Enter value to delete:" << endl << "> ";
			cin >> value;
			
			node = mytree.Find(value);

			if (node)
			{
				if(mytree.Delete(node))
					cout << "Element deleted" << endl;
				else
					cout << "Delete failed" << endl;
			}
			else
				cout << "Cannot delete - Element not found" << endl;
			break;

		case 4:
			cout << "Enter value to relink:" << endl << "> ";
			cin >> value;
			
			node = mytree.Find(value);
			if(node->Remove())
			{
				if(node->Link(&mytree))
					cout << "Element linked" << endl;
				else
					cout << "Link failed" << endl;
			}
			else
				cout << "Cannot remove" << endl;
			break;

		case 5:
			node = mytree.GetMin();
			if(node)
				cout << "Minumum value is: " << node->Data << endl;
			else
				cout << "No minumum found" << endl;
			break;
		case 6:
			node = mytree.GetMax();
			if(node)
				cout << "Maximum value is: " << node->Data << endl;
			else
				cout << "No maximum found" << endl;
			break;
		case 7:
			PrintStruct(&mytree);
			cout << "Press any key" << endl;
			getch();
			break;
		case 8:
			PrintInOrder(&mytree);
			cout << "Press any key" << endl;
			getch();
			break;
		case 9:
			PrintHeight(&mytree);
			cout << "Press any key" << endl;
			getch();
			break;
		case 0:
			end = true;
			break;
		}
	}
	
	return 0;
}
