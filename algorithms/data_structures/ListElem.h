//----------------------------------------------------------
// LISTELEM.H
//----------------------------------------------------------
// Implementation of two directional list node
//----------------------------------------------------------
// (C) VSoftware 2002
//----------------------------------------------------------
// Contact: saveman@wp.pl
//----------------------------------------------------------

#ifndef V_LIST_ELEM_H
#define V_LIST_ELEM_H

//----------------------------------------------------------
template <class T>
class VListElem;
//----------------------------------------------------------

//----------------------------------------------------------
#include <stdlib.h>
#include "list.h"
//----------------------------------------------------------

//----------------------------------------------------------
template <class T>
class VListElem
{
	// klasa listy
	friend class VList<T>;

// ----- FIELDS -----
public:
	// dane przechowywane przez element
	T			m_Data;
private:
	// lista zawierajaca dany objekt
	VList<T>*		m_ParentList;
	// element nastepny
	VListElem<T>*	m_Next;
	// element poprzedni
	VListElem<T>*	m_Prev;

// ----- METHODS -----
public:
	// konstruktor - tworzy nowy obiekt 
	VListElem(const T data);
	// destruktor - nie powinien byc bezposrednio
	// wywolywany - nalezy uzywac funkcji Delete
	~VListElem();

	// tworzy nowy element zawierajacy dane
	// przekazane jako argument i umieszcza
	// za elementem dla ktorego funkcja zostala
	// wywolana
	VListElem<T>* InsertAfter(const T data);
	// tworzy nowy element zawierajacy dane
	// przekazane jako argument i umieszcza
	// przed elementem dla ktorego funkcja
	// zostala wywolana
	VListElem<T>* InsertBefore(const T data);

	// umieszcza przekazany jako parametr
	// element za elementem dla ktorego
	// funkcja zostala wywolana
	bool InsertAfter(VListElem<T>* elem);
	// umieszcza przekazany jako parametr
	// element przed elementem dla ktorego
	// funkcja zostala wywolana
	bool InsertBefore(VListElem<T>* elem);

	// usuwa dany element z listy
	// nie zwalniajac jego pamieci
	// element moze byc pozniej dolaczony
	// z powrotem do listy
	void Remove();

	// usuwa dane element z listy
	// i zwalnia jego pamiec
	void Delete();

	// zwraca kolejny element na liscie
	VListElem<T>* GetNext();
	// zwraca kolejny element na liscie
	const VListElem<T>* GetNext() const;

	// zwraca poprzedni element na liscie
	VListElem<T>* GetPrev();
	// zwraca poprzedni element na liscie
	const VListElem<T>* GetPrev() const;

	// zwraca liste do ktorej nalezy element
	VList<T>* GetList();
	// zwraca liste do ktorej nalezy element
	const VList<T>* GetList() const;

};
//----------------------------------------------------------

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>::VListElem(const T data) :
	m_Data(data),
	m_ParentList(NULL),
	m_Next(NULL),m_Prev(NULL)
{
}

//----------------------------------------------------------
template <class T>
VListElem<T>::~VListElem()
{
	if(m_ParentList)
		Remove();
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VListElem<T>::InsertAfter(const T data)
{
	if(!m_ParentList)
		return NULL;
	return m_ParentList->InsertAfter(data,this);
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VListElem<T>::InsertBefore(const T data)
{
	if(!m_ParentList)
		return NULL;
	return m_ParentList->InsertBefore(data,this);
}

//----------------------------------------------------------
template <class T>
bool VListElem<T>::InsertAfter(VListElem<T>* elem)
{
	if(!m_ParentList)
		return false;
	return m_ParentList->InsertAfter(elem,this);
}

//----------------------------------------------------------
template <class T>
bool VListElem<T>::InsertBefore(VListElem<T>* elem)
{
	if(!m_ParentList)
		return false;
	return m_ParentList->InsertBefore(elem,this);
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
void VListElem<T>::Remove()
{
	if(m_ParentList)
		m_ParentList->Remove(this);
}

//----------------------------------------------------------
template <class T>
void VListElem<T>::Delete()
{
	Remove();
	delete this;
}

//----------------------------------------------------------
//----------------------------------------------------------
template <class T>
VListElem<T>* VListElem<T>::GetNext()
{
	return m_Next;
}

//----------------------------------------------------------
template <class T>
const VListElem<T>* VListElem<T>::GetNext() const
{
	return m_Next;
}

//----------------------------------------------------------
template <class T>
VListElem<T>* VListElem<T>::GetPrev()
{
	return m_Prev;
}

//----------------------------------------------------------
template <class T>
const VListElem<T>* VListElem<T>::GetPrev() const
{
	return m_Prev;
}

//----------------------------------------------------------
template <class T>
VList<T>* VListElem<T>::GetList()
{
	return m_ParentList;
}

//----------------------------------------------------------
template <class T>
const VList<T>* VListElem<T>::GetList() const
{
	return m_ParentList;
}

//----------------------------------------------------------
//----------------------------------------------------------

#endif
