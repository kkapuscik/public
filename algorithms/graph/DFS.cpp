/* That is a DepthFirstSearch graph algorithm implementation */
/* done by: SAVEMAN 02-apr-2001 */

#include "DFS.h"

/* DFS implementation */
void DFSProc (char **Matrix, DFSInfo *Info, DFSItem Start, DFSItem Nodes);
/* where:
Nodes  - number of nodes in graph
Matrix - a graph neighbours matrix filled with values:
         0-if such connections don't exists, else <>0
         (SIZE Nodes*Nodes)
Info   - info structure table to store dfs results in
			(and also temporary data)
			(SIZE Nodes)
Start  - starting node number

NOTE:
I put the info table to parameters because I was trying
to give all error situations handling for programmer using
this proc and to put in DFSProc only algorithm code
NOTE2:
If you use less/more than 256/32767 nodes you should change
the size of DFSItem type in "DFS.h" file
*/

/* subroutine declaration */
void DFSVisit (DFSItem NodeIndex);

/* subroutine calling parametres */
DFSItem Time;
char **DFSMatrix;
DFSInfo *DFSNodeInfo;
DFSItem DFSNodes;

/* algorithm */
void DFSProc (char **Matrix, DFSInfo *Info, DFSItem Start, DFSItem Nodes)
{
	DFSItem NodeCnt;

	/* copying the parametres to skip copying when calling */
   /* the subroutine */
	DFSMatrix = Matrix;	DFSNodeInfo = Info;	DFSNodes = Nodes;
	/* setting 'time' counter to 0 */
	Time = 0;

   for (NodeCnt=0; NodeCnt<Nodes; NodeCnt++)
      Info[NodeCnt].Status = DFS_STATUS_CLEAR;

   DFSVisit (Start);
   for (NodeCnt=0; NodeCnt<Nodes; NodeCnt++)
   	if (Info[NodeCnt].Status == DFS_STATUS_CLEAR)
      	DFSVisit (NodeCnt);
}

/* algorithm subroutine */
void DFSVisit (DFSItem NodeIndex)
{
	DFSItem EdgeCnt;
	DFSNodeInfo[NodeIndex].Status = DFS_STATUS_QUEUED;
   DFSNodeInfo[NodeIndex].TimeIn = Time++;

   for (EdgeCnt=0; EdgeCnt<DFSNodes; EdgeCnt++)
   	if (DFSMatrix[NodeIndex][EdgeCnt]!=0)
	      if (DFSNodeInfo[EdgeCnt].Status == DFS_STATUS_CLEAR)
         	DFSVisit (EdgeCnt);
	DFSNodeInfo[NodeIndex].Status = DFS_STATUS_DONE;
   DFSNodeInfo[NodeIndex].TimeOut = Time++;
}

