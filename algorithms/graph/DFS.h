#ifndef SavDFS_H
#define SavDFS_H 1

#define DFS_STATUS_CLEAR	0
#define DFS_STATUS_QUEUED	1
#define DFS_STATUS_DONE	   2

typedef short DFSItem;

typedef struct StructDFSInfo
{
	DFSItem TimeIn, TimeOut;
	DFSItem Status;
} DFSInfo;

/* functions to call */
extern void DFSProc (char **Matrix, DFSInfo *Info,
   DFSItem Start, DFSItem Nodes);

#endif
