#ifndef SavDIJKSTRA_H
#define SavDIJKSTRA_H 1

#include <limits.h>

#define DIJ_NO_WAY LONG_MAX

typedef long DIJDistance;
typedef short DIJItem;

/* functions to call */
extern void Dijkstra (DIJDistance **Matrix, DIJDistance *Result,
	DIJItem *Queue, DIJItem Source, DIJItem Items);

#endif

