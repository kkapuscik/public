/* That is a BreadthFirstSearch graph algorithm implementation */
/* done by: SAVEMAN 02-apr-2001 */

#include "BFS.h"

/* BFS implementation */
void BFSProc (char **Matrix, BFSInfo *Info,
	BFSItem *Queue, BFSItem Start, BFSItem Nodes);
/* where:
Nodes  - number of nodes in graph
Matrix - a graph neighbours matrix filled with values:
         0-if such connections don't exists, else <>0
         (SIZE Nodes*Nodes)
Info   - info structure table to store dfs results in
			(and also temporary data)
			(SIZE Nodes)
Start  - starting node number
			(SIZE Nodes)
Queue  - temporary node queue (used in algorithm)
NOTE:
I put the info table and queue to parameters because I was trying
to give all error situations handling for programmer using
this proc and to put in DFSProc only algorithm code
NOTE2:
If you use less/more than 256/32767 nodes you should change
the size of DFSItem type in "DFS.h" file
NOTE3:
This algorithm doesn't work on graphs that are not connected
(e.g. have two or more parts that are not connected)
All of the nodes that couldn't be reached from start node will
have distance value == -1
*/

/* algorithm */
void BFSProc (char **Matrix, BFSInfo *Info,
	BFSItem *Queue, BFSItem Start, BFSItem Nodes)
{
	BFSItem NodeCnt;
   BFSItem ActNode,ActDist;
	BFSItem StartQueue;
   BFSItem EndQueue;

   /* empty queue at start */
   StartQueue = EndQueue = 0;
   /* clear all nodes status */
   for (NodeCnt=0; NodeCnt<Nodes; NodeCnt++)
   {
      Info[NodeCnt].Status = BFS_STATUS_CLEAR;
		Info[NodeCnt].Distance = -1;
   }
   /* put start node into queue */
	Info[Start].Status = BFS_STATUS_QUEUED;
   Info[Start].Distance = 0;
   Queue[EndQueue++] = Start;
   /* and process graph */
   while (EndQueue > StartQueue)
   {
   	ActNode = Queue[StartQueue++]; /* we mark this node as processed here '++' */
      ActDist = Info[ActNode].Distance;
      for (NodeCnt=0; NodeCnt<Nodes; NodeCnt++)
      	if (Matrix[ActNode][NodeCnt] != 0 && Info[NodeCnt].Status == BFS_STATUS_CLEAR)
         {
         	Info[NodeCnt].Status = BFS_STATUS_QUEUED;
            Info[NodeCnt].Distance = (BFSItem) (ActDist+1);
            Queue[EndQueue++] = NodeCnt;
         }
      Info[ActNode].Status = BFS_STATUS_DONE;
   }
}

