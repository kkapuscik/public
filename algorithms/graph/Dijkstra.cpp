/* Dijkstra shortest path with one source algorithm */
/* CODED BY: Saveman 03-apr-2001 */

#include "Dijkstra.h"

/* specification */
void Dijkstra (DIJDistance **Matrix, DIJDistance *Result,
	DIJItem *Queue, DIJItem Source, DIJItem Items);
/* info
This is neighbours matrix based version with simple queue
implementation so it is rather slow O(V^3) but that is only
example of Dijkstra algorithm. You can speed-up this if you
represent neighbours in lists (so you'll get O(V^2+E) ) and
with implementing Queue with binary heap ( O((V+E)logV) ) or
with Fibonacci heap (so you'll get O(VlgV + E) )	*/
/* parametres:
Matrix - neighbours matrix
			SIZE (Items * Items)
Result - result distance matrix
			SIZE (Items)
Queue  - temporary queue table
			SIZE (Items)
Source - source node index
Items	 - total number of items
NOTE:
This algorithm don't work with graphs with negative
edge values (in most cases).
*/

void Dijkstra (DIJDistance **Matrix, DIJDistance *Result,
	DIJItem *Queue, DIJItem Source, DIJItem Items)
{
	DIJItem Cnt,NodeCnt,Find,Index;
   DIJDistance Minimum;
   DIJItem InQueue;

   /* initialization */
	for (Cnt=0; Cnt<Items; Cnt++)
	{
   	Result[Cnt]=DIJ_NO_WAY;
      Queue[Cnt]=Cnt;
   }
   InQueue = Items;
   Result[Source]=0;

   while (InQueue > 0)
   {
   	/* searching for actual minimum */
   	Minimum = Result[Queue[0]];
      Index = 0;
   	for (Cnt=1; Cnt<InQueue; Cnt++)
      	if (Result[Queue[Cnt]] < Minimum)
         {
   			Minimum = Result[Queue[Cnt]];
	   	   Index = Cnt;
         }
      Find = Queue[Index];
		/* flushing found item from queue (putting it outside range) */
      Queue[Index] = Queue[--InQueue];

      /* processing found node */
      for (NodeCnt=0; NodeCnt<Items; NodeCnt++)
      	if (Matrix[Find][NodeCnt] != 0)
         	if (Minimum+Matrix[Find][NodeCnt] < Result[NodeCnt])
            	Result[NodeCnt] = Minimum+Matrix[Find][NodeCnt];
   }
}
