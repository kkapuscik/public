#ifndef SavBFS_H
#define SavBFS_H 1

#define BFS_STATUS_CLEAR	0
#define BFS_STATUS_QUEUED	1
#define BFS_STATUS_DONE	   2

typedef short BFSItem;

typedef struct StructBFSInfo
{
	BFSItem Distance;
	BFSItem Status;
} BFSInfo;

/* functions to call */
extern void BFSProc (char **Matrix, BFSInfo *Info,
	BFSItem *Queue, BFSItem Start, BFSItem Nodes);

#endif
