/* GAUSS ELIMINATION */
/* CODED BY: Saveman 03-apr-2001 */

#include "Gauss.h"

/* specification */
int Gauss (GAUItem **Table, GAUSize N);
/* the parameters are:
Table - matrix of coefficients and results
		  SIZE (N rows * (N+1) columns) where last
        column have the results
        after function call if function result
        was GAU_OK this table have results in
        values (last) column
N     - number of variables
NOTE:
This function doesn't test if the matrix have
multiple or no solution (it will a bit complicate
the code and in most cases it means the same - that
this equations aren't have one solution)
*/

int Gauss (GAUItem **Table, GAUSize n)
{
	GAUSize i,j,k;
   GAUItem swp,tmp;

   /* create down-triangle matrix */
   for (i = 0; i < n; i++)
   {
   	/* search for row with non-zero variable */
   	for (j = i; j < n; j++)
      	if (Table[j][i] != 0)
         	break;
      /* when no row found it means that this equations have no */
      /* normal solution (is no or multiple solutions) */
      if (j == n)
      {	return GAU_CANNOT_COUNT;	}
      /* if row is found then if needed we place it at current position */
      if (j != i)
      {
      	for (k = 0; k <= n; k++)
         {	swp = Table[i][k];	Table[i][k] = Table[j][k];	Table[j][k] = swp;	}
      }
		/* we divide whole row by aii (from i because earlier are already 0) */
      tmp = Table[i][i];
      for (j = i; j <= n; j++)
      	Table[i][j] /= tmp;

		/* and now we eliminate variable from lower rows */
      for (j = (GAUSize)(i+1); j < n; j++)
      	if (Table[j][i] != 0)
         {
         	tmp = Table[j][i];
            for (k = 0; k <= n; k++)
            	Table[j][k] -= Table[i][k]*tmp;
         }
   }
	/* we create unit matrix (only 1's at main diagonal) */
   for (i = (GAUSize)(n-1); i >= 0; i--)
   {
   	tmp = Table[i][n];
   	for (j=(GAUSize)(i-1); j >= 0; j--)
      {
      	Table[j][n] -= Table[j][i]*tmp;
         Table[j][i] = 0;
      }
   }

   return GAU_OK;
}

