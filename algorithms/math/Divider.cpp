/* SMALLEST DIVIDER TABLE ALGORITHM */
/* CODED by Saveman 03-apr-2001 */

#include "Divider.h"

/* specification */
void Divider (DIVItem *Table, DIVItem Items);
/* the parametres are:
Table - a table to store smallest dividers
        (SIZE Items+1) (index 0..Items)
        after calling function all table items
        are set to smallest divider:
        Table[i] = smallest_divider (i)
Total - a highest number to count divider for
NOTE:
0 (zero) have 0 as it's smallest divider
1 have 1 as it's smallest divider
NOTE2:
Pay attention for situation of overload
(if you want to count dividers for more than 30000
change the DIVItem type to (unsigned) long)
*/

void Divider (DIVItem *Table, DIVItem Items)
{
   DIVItem Cnt, Mul;
   for (Cnt=0; Cnt<=Items; Cnt++)
      Table[Cnt] = 0;

   Table[1]=1;
   for (Cnt=2; Cnt<=Items; Cnt++)
   {
      if (Table[Cnt] != 0)
         continue;
		Table[Cnt] = Cnt;
      for (Mul=(DIVItem)(Cnt+Cnt); Mul<=Items; Mul+=Cnt)
         if (Table[Mul] == 0)
            Table[Mul] = Cnt;
   }
}