#ifndef SavGAUSS_H
#define SavGAUSS_H 1

/* switch only between real types */
typedef double GAUItem;

typedef short GAUSize;

#define GAU_CANNOT_COUNT	1
#define GAU_OK					0

extern int Gauss (GAUItem **Table, GAUSize N);

#endif
