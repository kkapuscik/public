/* Eratosthenes sieve algorithm */
/* CODED BY Saveman 03-apr-2001 */

#include "EratoSieve.h"

/* specification */
void ErastPrime (ESVResult *Table, ESVItem Items);
/* the parametres are:
Table - a table to store primes data
        (SIZE Items+1) (index 0..Items)
        after calling function the table will
        have ESV_PRIME or ESV_NORMAL value
Total - a highest number to count divider for
NOTE:
1 (one) is not a prime because it have only 1
divider so it is marked as ESV_NORMAL
NOTE2:
Pay attention for situation of overload
(if you want to count prime for more than 30000
change the ESVItem type to (unsigned) long)
*/

void ErastPrime (ESVResult *Table, ESVItem Items)
{
	ESVItem Cnt, Mul;

   Table[0] = Table[1] = ESV_NORMAL;
   for (Cnt=2; Cnt<=Items; Cnt++)
   	Table[Cnt] = ESV_PRIME;

	for (Cnt=2; Cnt<=Items; Cnt++)
   {
   	if (Table[Cnt] == ESV_NORMAL)
      	continue;
		for (Mul=(ESVItem)(Cnt+Cnt); Mul<=Items; Mul+=Cnt)
      	Table[Mul] = ESV_NORMAL;
   }
}

