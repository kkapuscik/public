#ifndef SavERATOSIEVE_H
#define SavERATOSIEVE_H 1

#define ESV_NORMAL 0
#define ESV_PRIME 1
typedef char ESVResult;

typedef unsigned short ESVItem;

/* functions to call */
extern void ErastPrime (ESVResult *Table, ESVItem Items);

#endif
