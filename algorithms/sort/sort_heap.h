#ifndef V_SORT_HEAP_H
#define V_SORT_HEAP_H

template <class T>
void HeapSort(T *tab,int n)
{
	int i,r,c;
	T t;
	// build heap O(n)
	for(i=(n-1)/2; i>=0; i--)
	{
		for(r=i,t=tab[r]; ; r=c)
		{
			if((c = 2*r+1)>=n)
				break;
			if(c+1<n && tab[c+1]>tab[c])
				c++;
			if(tab[c] > t)
				tab[r] = tab[c];
			else
				break;
		}
		tab[r] = t;
	}
	// extract_max O(logn) * n = O(nlogn)
	for(i=n-1; i>=1; i--)
	{
		t = tab[i];
		tab[i] = tab[0];
		tab[0] = t;
		for(r=0,t=tab[r]; ; r=c)
		{
			if((c = 2*r+1)>=i)
				break;
			if(c+1<i && tab[c+1]>tab[c])
				c++;
			if(tab[c] > t)
				tab[r] = tab[c];
			else
				break;
		}
		tab[r] = t;
	}
}

#endif

