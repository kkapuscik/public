#ifndef V_SORT_SHELL_H
#define V_SORT_SHELL_H

template <class T>
void ShellSort(T *tab,int n)
{
	int h,i,j;
	T t;
	for(h = 1; h <= n/9; h = h*3 + 1)
		;
	while(h > 0)
	{
		for(i=h; i<n; i++)
		{
			t = tab[i];
			j = i-h;
			while((j>=0) && (tab[j]>t))
			{
				tab[j+h] = tab[j];
				j -= h;
			}
			tab[j+h] = t;
		}
		h /= 3;
	}
}

#endif
