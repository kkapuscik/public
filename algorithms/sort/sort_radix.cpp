#include "sort_radix.h"

// sorts table with 8 steps (4 bits processed at each step)
// values in table 'tab' must be higher or equal to zero
void RadixSort(int *tab,int size)
{
	int i,j,s,max,pos;
	int *temp[16];
	int inside[16];
	char shift;
	int subtab;
	
	// alloc temporary tables memory
	for(i=0; i<16; i++)
		temp[i] = new int[size];

	// sort
	shift = 0;
	for(s=0; s<8; s++)
	{
		for(i=0; i<16; i++)
			inside[i] = 0;

		// divide
		for(i=0; i<size; i++)
		{
			subtab = (tab[i]>>shift) & 0x0F;
			temp[subtab][inside[subtab]++] = tab[i];
		}

		// merge
		pos = 0;
		for(j=0; j<16; j++)
		{
			max = inside[j];
			for(i=0; i<max; i++)
				tab[pos++] = temp[j][i];
		}

		shift+=4;
	}

	// free temporary tables memory
	for(i=0; i<16; i++)
		delete[] temp[i];
}
