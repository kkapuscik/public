#ifndef V_SORT_SELECTPLUS_H
#define V_SORT_SELECTPLUS_H

template <class T>
void SelectPlusSort(T *tab,int n)
{
	int i,j,min,max;
	int last = n/2;
	T mine,maxe;
	for(i=0; i<last; i++)
	{
		min = max = i;
		mine = maxe = tab[i];
		for(j=i+1; j<n-i; j++)
			if (tab[j] < mine)
			{
				min = j;
				mine = tab[j];
			}
			else if (tab[j] > maxe)
			{
				max = j;
				maxe = tab[j];
			}
		if (min==max)
			break;
		tab[min] = tab[i];
		tab[i] = mine;
		if (max==i)
			max=min;
		tab[max] = tab[n-i-1];
		tab[n-i-1] = maxe;
	}
}

#endif
