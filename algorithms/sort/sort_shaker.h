#ifndef V_SORT_SHAKER_H
#define V_SORT_SHAKER_H

template <class T>
void ShakerSort(T *tab,int n)
{
	int i,l,r,p;
	T t;
	l = 0;
	r = n-1;
	p = l;
	while(l < r)
	{
		for(i=l; i<r; i++)
			if (tab[i] > tab[i+1])
			{
				t = tab[i+1];
				tab[i+1] = tab[i];
				tab[i] = t;
				p = i;
			}
		r = p;

		for(i=r-1; i>=l; i--)
			if (tab[i] > tab[i+1])
			{
				t = tab[i+1];
				tab[i+1] = tab[i];
				tab[i] = t;
				p = i;
			}
		l = p+1;
	}
}

#endif
