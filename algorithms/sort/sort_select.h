#ifndef V_SORT_SELECT_H
#define V_SORT_SELECT_H

template <class T>
void SelectSort(T *tab,int n)
{
	int i,j,min;
	T t;
	for(i=0; i<n-1; i++)
	{
		min = i;
		t = tab[min];
		for(j=i+1; j<n; j++)
			if (tab[j] < t)
			{
				min = j;
				t = tab[min];
			}
		tab[min] = tab[i];
		tab[i] = t;
	}
}

#endif
