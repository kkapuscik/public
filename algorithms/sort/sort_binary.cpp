#include "sort_binary.h"

// values in table 'tab' must be higher or equal to zero
void BinarySortProc(int *tab,int size,int bit)
{
	int i,m,temp;

	if(size<=1)
		return;

	m = 0;
	for(i=0; i<size; i++)
		if(!((tab[i]>>bit)&0x01))
		{
			temp = tab[m];
			tab[m] = tab[i];
			tab[i] = temp;
			m++;
		}
	if(bit>0)
	{
		BinarySortProc(tab,m,bit-1);
		BinarySortProc(&tab[m],size-m,bit-1);
	}
}
void BinarySort(int *tab,int size)
{
	BinarySortProc(tab,size,30);
}