#ifndef V_SORT_LINEAR_H
#define V_SORT_LINEAR_H

template <class T>
void SelectSort(T *tab,int n)
{
	int i;
	T t;
	for(i=0; i<n; i++)
	{
		while(tab[i]!=i)
		{
			t = tab[i];
			tab[i] = tab[tab[i]];
			tab[tab[i]] = t;
		}
	}
}

#endif
