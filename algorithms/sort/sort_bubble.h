#ifndef V_SORT_BUBBLE_H
#define V_SORT_BUBBLE_H

template <class T>
void BubbleSort(T *tab,int n)
{
	int i,j;
	T t;
	for(i=0; i<n-1; i++)
		for(j=n-2; j>=i; j--)
			if (tab[j] > tab[j+1])
			{
				t = tab[j];
				tab[j] = tab[j+1];
				tab[j+1] = t;
			}
}

#endif
