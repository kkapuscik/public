#ifndef V_SORT_INSERTHALF_H
#define V_SORT_INSERTHALF_H

template <class T>
void InsertHalfSort(T *tab,int n)
{
	int i,l,r,m;
	T t;
	for(i=1; i<n; i++)
	{
		t = tab[i];

		l = 0;
		r = i-1;
		while(r-l>1)
		{
			m = (l+r) >> 1; // (l+p) div 2
			if (tab[m] < t)
				l = m;
			else
				r = m;
		}
		if(tab[l]>t)
			m = l;
		else if (tab[r]>t)
			m = r;
		else
			m = r+1;

		for(r=i-1; r>=m; r--)
			tab[r+1] = tab[r];
		tab[m] = t;
	}
}

#endif
