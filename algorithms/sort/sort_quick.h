#ifndef V_SORT_QUICK_H
#define V_SORT_QUICK_H

template <class T>
void QuickSort(T *tab,int n)
{
	int i,m;
	T t;
	if (n>1)
	{
		m = 0;
		for(i=1; i<n; i++)
			if (tab[i] < tab[0])
			{
				m++;
				t = tab[i];
				tab[i] = tab[m];
				tab[m] = t;
			}
		t = tab[0] ;
		tab[0] = tab[m];
		tab[m] = t;

		if (m>1)
			QuickSort(tab,m);
		if (m+2<n)
			QuickSort(&tab[m+1],n-m-1);
	}
}

#endif

