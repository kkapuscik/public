#include <stdlib.h>
#include <time.h>
#include <iostream.h>

//----------
//----- SORT METHODS THAT USE COMPARISIONS
//----------
#include "sort_bubble.h"
#include "sort_heap.h"
#include "sort_insert.h"
#include "sort_inserthalf.h"
#include "sort_quick.h"
#include "sort_select.h"
#include "sort_selectplus.h"
#include "sort_shaker.h"
#include "sort_shell.h"

//----------
//----- SORT METHODS THAT USE KEY ATTRIBUTES
//----------
#include "sort_radix.h"
#include "sort_binary.h"
#include "sort_bucket.h"
#include "sort_merge.h"


//----------
//----- data generation function
//----------
enum GenMode {gmRandom,gmSorted,gmReverse};

void GenerateData(int *table,int size,GenMode mode)
{
	int i;

	switch(mode)
	{
		case gmRandom:
			srand((unsigned int)time(NULL));
			for(i=0; i<size; i++)
				table[i] = (rand()<<16) + rand();
			break;
		case gmSorted:
			for(i=0; i<size; i++)
				table[i] = i;
			break;
		case gmReverse:
			for(i=0; i<size; i++)
				table[i] = size-i;
			break;
	}
}

//----------
//----- table copy function (TEMPLATE)
//----------
template <class T>
void CopyTable(T *dest,T *src,int elems)
{
	int i;
	for(i=0; i<elems; i++)
		dest[i] = src[i];
}

//----------
//----- table compare function (TEMPLATE)
//----------
// * returns true if tables are identical
//----------
template <class T>
bool CompareTable(T *tab1,T *tab2,int elems)
{
	int i;
	for(i=0; i<elems; i++)
		if (tab1[i] != tab2[i])
			return false;
	return true;
}

//----------
//----- Sorting test function
//----------
void Test(void (*sortfunc)(int *,int),
		  char *name,int *data,int *temp,int *done,int size)
{
	clock_t start,end;

	cout << "Sort method: " << name << endl;

	CopyTable(temp,data,size);

	start = clock();
	sortfunc(temp,size);
	end = clock();

	if(CompareTable(temp,done,size))
		cout << "Work time: " << (end-start) << " ticks" << endl;
	else
		cout << "--- error occured" << endl;
}

//----------
//----- int compare function
//----------
int CompareInt(const void *i1,const void *i2)
{
	if(*(int*)i1 > *(int*)i2)		return 1;
	else if(*(int*)i1 < *(int*)i2)	return -1;
	else							return 0;
}

//----------
//----- sorting using built-in qsort function
//----------
void BuiltInQSort(int *data,int size)
{
	clock_t start,end;

	cout << "Sort method: Build-in Quicksort (qsort)" << endl;

	start = clock();
	qsort(data,size,sizeof(data[0]),CompareInt);
	end = clock();

	cout << "Work time: " << (end-start) << " ticks" << endl;
}


//----------
//----- test all methods using given data
//----------
void TestPack(int *data,int *temp,int *done,int size,char *packname)
{
	cout << "Test pack name = " << packname << endl << endl;

	CopyTable(done,data,size);
	BuiltInQSort(done,size);

	Test(BubbleSort<int>,		"Bubble sort",		data,temp,done,size);
	Test(ShakerSort<int>,		"Shaker sort",		data,temp,done,size);
	Test(SelectSort<int>,		"Select sort",		data,temp,done,size);
	Test(SelectPlusSort<int>,	"Select+ sort",		data,temp,done,size);
	Test(InsertSort<int>,		"Insert sort",		data,temp,done,size);
	Test(InsertHalfSort<int>,	"InsertHalf sort",	data,temp,done,size);
	Test(ShellSort<int>,		"Shell sort",		data,temp,done,size);
	Test(HeapSort<int>,			"Heap sort",		data,temp,done,size);
	Test(QuickSort<int>,		"Quick sort",		data,temp,done,size);
	Test(RadixSort,				"Radix sort",		data,temp,done,size);
	Test(BinarySort,			"Binary sort",		data,temp,done,size);
	Test(BucketSort,			"Bucket sort",		data,temp,done,size);
	Test(MergeSort,				"Merge sort",		data,temp,done,size);
	cout << endl << endl;
}

//----------
//----- program DATA
//----------
const int DATAPACKSIZE = 100000;
int data[DATAPACKSIZE];
int done[DATAPACKSIZE];
int temp[DATAPACKSIZE];

//----------
//----- program MAIN function
//----------
int main()
{
	GenerateData(data,DATAPACKSIZE,gmRandom);
	TestPack(data,temp,done,DATAPACKSIZE,"Random data");

	GenerateData(data,DATAPACKSIZE,gmSorted);
	TestPack(data,temp,done,DATAPACKSIZE,"Sorted data");

	GenerateData(data,DATAPACKSIZE,gmReverse);
	TestPack(data,temp,done,DATAPACKSIZE,"Reverse sorted data");

	return 0;
}
