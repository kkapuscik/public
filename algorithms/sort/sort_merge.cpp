#include "sort_merge.h"

#include <stdlib.h>

struct Node
{
	int		Value;
	Node	*Next;
};

Node *MergeSortProc(Node *list,int size)
{
	Node *sub1,*sub2,*first,*temp;
	int i,half;

	if(size<=1)
		return list;

	// split into two lists
	half = size/2;
	sub1 = temp = list;
	for(i=1; i<half; i++)
		temp = temp->Next;
	sub2 = temp->Next;
	temp->Next = NULL;

	// sort sub lists
	sub1 = MergeSortProc(sub1,half);
	sub2 = MergeSortProc(sub2,size-half);

	// merge
	if(sub1->Value < sub2->Value)
	{
		first = temp = sub1;
		sub1 = sub1->Next;
	}
	else
	{
		first = temp = sub2;
		sub2 = sub2->Next;
	}
	while(sub1!=NULL && sub2!=NULL)
	{
		if(sub1->Value < sub2->Value)
		{
			temp->Next = sub1;
			temp = temp->Next;
			sub1 = sub1->Next;
		}
		else
		{
			temp->Next = sub2;
			temp = temp->Next;
			sub2 = sub2->Next;
		}
	}
	if(sub1!=NULL)
		temp->Next = sub1;
	else
		temp->Next = sub2;

	return first;
}

void MergeSort(int *tab,int size)
{
	int i;
	Node *first,*act;
	if(size>1)
	{
		// create list from data table
		first = new Node;
		first->Value = tab[0];
		act = first;
		for(i=1; i<size; i++)
		{
			act->Next = new Node;
			act = act->Next;
			act->Value = tab[i];
		}
		act->Next = NULL;

		// sort list
		first = act = MergeSortProc(first,size);
		// rebuild table (from sorted list)
		i = 0;
		while(act)
		{
			tab[i++] = act->Value;
			act = act->Next;
		}

		// delete list
		while(first)
		{
			act = first->Next;
			delete first;
			first = act;
		}
	}
}