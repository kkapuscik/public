#ifndef V_SORT_COUNT_H
#define V_SORT_COUNT_H

// elements must be in range 0..255
template <class T>
void CountSort(T *tab,int size)
{
	int i,p,count[256];
	for(i=0; i<256; i++)
		count[i] = 0;
	for(i=0; i<size; i++)
		count[tab[i]]++;
	for(p=i=0; i<256; i++)
		while(count[i]-- > 0)
			tab[p++] = i;
}

#endif
