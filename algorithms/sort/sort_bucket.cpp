#include "sort_bucket.h"

#include <stdlib.h>

struct Node
{
	int		Value;
	Node	*Next;
};

Node *BucketSortProc(Node *list,int shift)
{
	Node *buckact[16];
	int i;
	Node *act;
	int buckno;

	if(list==NULL)
		return NULL;

	// buckets are empty now
	for(i=0; i<16; i++)
		buckact[i] = NULL;
	// divide elements into buckets
	while(list)
	{
		act = list;
		list = list->Next;

		buckno = (act->Value >> shift) & 0x0F;
		act->Next = buckact[buckno];
		buckact[buckno] = act;
	}

	// sort buckets
	if(shift > 0)
	{
		for(i=0; i<16; i++)
			if(buckact[i]!=NULL)
				buckact[i] = BucketSortProc(buckact[i],shift-4);
	}

	// merge buckets
	for(i=0; i<16; i++)
		if(buckact[i]!=NULL)
		{
			list = act = buckact[i];
			break;
		}
	for(i++; i<16; i++)
	{
		if(buckact[i]!=NULL)
		{
			while(act->Next != NULL)
				act = act->Next;
			act->Next = buckact[i];
		}
	}
	return list;
}

void BucketSort(int *tab,int size)
{
	int i;
	Node *first,*act;
	if(size>1)
	{
		// create list from data table
		first = new Node;
		first->Value = tab[0];
		act = first;
		for(i=1; i<size; i++)
		{
			act->Next = new Node;
			act = act->Next;
			act->Value = tab[i];
		}
		act->Next = NULL;

		// sort list
		first = act = BucketSortProc(first,28);
		// rebuild table (from sorted list)
		i = 0;
		while(act)
		{
			tab[i++] = act->Value;
			act = act->Next;
		}

		// delete list
		while(first)
		{
			act = first->Next;
			delete first;
			first = act;
		}
	}
}