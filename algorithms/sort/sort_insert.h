#ifndef V_SORT_INSERT_H
#define V_SORT_INSERT_H

template <class T>
void InsertSort(T *tab,int n)
{
	int i,j;
	T t;
	for(i=1; i<n; i++)
	{
		t = tab[i];
		j = i-1;
		while((j>=0) && (tab[j]>t))
		{
			tab[j+1] = tab[j];
			j--;
		}
		tab[j+1] = t;
	}
}

#endif
