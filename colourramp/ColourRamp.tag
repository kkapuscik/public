<?xml version='1.0' encoding='ISO-8859-1' standalone='yes' ?>
<tagfile>
  <compound kind="page">
    <name>description</name>
    <title>Description</title>
    <filename>description</filename>
  </compound>
  <compound kind="page">
    <name>usage</name>
    <title>Usage</title>
    <filename>usage</filename>
  </compound>
  <compound kind="class">
    <name>Arguments</name>
    <filename>classArguments.html</filename>
    <member kind="function">
      <type></type>
      <name>Arguments</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>3d9dd7b675eac3c375c6b2e9bb13e896</anchor>
      <arglist>(int argc, char *argv[])</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_valid</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>a4e080f16205409e75a06db10272bbb5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>has_string</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>02385d51cbb094f4c1b49501ee62d318</anchor>
      <arglist>(argument_id_t id) const </arglist>
    </member>
    <member kind="function">
      <type>const string &amp;</type>
      <name>get_string</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>4189981b5d750f33db52fb5acc92f57c</anchor>
      <arglist>(argument_id_t id) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>has_int</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>f4ce884e4d6aec65532fb9e9ba81fa15</anchor>
      <arglist>(argument_id_t id) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_int</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>693730231e4dd19ad7bf52972fd53f1d</anchor>
      <arglist>(argument_id_t id) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_int</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>aac1f8f11285469c4a446d89bb7ecfcb</anchor>
      <arglist>(argument_id_t id, int default_value) const </arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>m_is_valid</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>91f7c6b6af3ab4df3ec0106bf9030eed</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>map&lt; argument_id_t, string &gt;</type>
      <name>m_string_values</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>b406ffbd8ed1b7ce9cbf42845e77c561</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>map&lt; argument_id_t, int &gt;</type>
      <name>m_int_values</name>
      <anchorfile>classArguments.html</anchorfile>
      <anchor>018173b262b461ac102983448d546458</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Color</name>
    <filename>classColor.html</filename>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>9a742cbe9f9f4037f5d9f4e81a9b2428</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>0d63f0364390800287974e0bdd7ad1dd</anchor>
      <arglist>(int color16)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Color</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>866f5b3f4192cdd953900b3bdae4b2cd</anchor>
      <arglist>(const Color &amp;other)</arglist>
    </member>
    <member kind="function">
      <type>unsigned short</type>
      <name>getUShort</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>fd86f7aab3e4261b512ec97047410efa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>m_red</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>b089d0dee4bae1d152683ad607f81156</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>m_green</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>c4f962fdced1b73ef3efe2b0d68cecc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>m_blue</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>3be3a2a274093a6b9063b36791e31c0b</anchor>
      <arglist></arglist>
    </member>
    <member kind="friend">
      <type>friend bool</type>
      <name>operator==</name>
      <anchorfile>classColor.html</anchorfile>
      <anchor>d8d4157e8894538e14651eee162df58e</anchor>
      <arglist>(const Color &amp;color1, const Color &amp;color2)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ColorDiff</name>
    <filename>classColorDiff.html</filename>
    <member kind="function">
      <type></type>
      <name>ColorDiff</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>9f87dd1870cdec4afe088a1facc9c530</anchor>
      <arglist>(const Color &amp;color1, const Color &amp;color2, int line_pixels)</arglist>
    </member>
    <member kind="function">
      <type>Color</type>
      <name>doStep</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>49057016f2671162379f5ca20694d334</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Color</type>
      <name>m_color</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>1e23a663d7f6ffa4de1b6248c58cd16a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_distance</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>701d44a39bfd2d788e07c499b74e0817</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_step</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>a811222c573ac0cec185f0982e46f68d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_red_diff</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>f7acd8dbcf8c8a33fc01d4fe865529bb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_green_diff</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>116ca7d6d4673c18e2ba627411784ce8</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_blue_diff</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>c7af193e4533dd8b2462a2285fef997c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>m_red_sign</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>3d8075b2b52ca3adc43c519b11e75277</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>m_green_sign</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>072cfe84ee06acccf834863e93abe5a9</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>bool</type>
      <name>m_blue_sign</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>ba99180709e19230475fd6e27df02159</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_red_error</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>85b3003324cdf8a8670b329a41295c59</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_green_error</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>537b10cbfd0e54ad1ab3e614e88e07be</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_blue_error</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>c77da4c15e86cd14057a7476f96ccd70</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_red_value</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>92fa9dc8979418363b86ff967805e270</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_green_value</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>22230f3ca0c83744125a0543f896c3b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_blue_value</name>
      <anchorfile>classColorDiff.html</anchorfile>
      <anchor>c0c197b47e479f97ae3bf92e6a41e648</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ColourRamp</name>
    <filename>classColourRamp.html</filename>
    <member kind="function">
      <type></type>
      <name>ColourRamp</name>
      <anchorfile>classColourRamp.html</anchorfile>
      <anchor>c351ee47711becd314b4ad970c146988</anchor>
      <arglist>(int argc, char *argv[])</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>exec</name>
      <anchorfile>classColourRamp.html</anchorfile>
      <anchor>fed0538b0770c99ee4e9bd2b302d4cb6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Arguments</type>
      <name>m_arguments</name>
      <anchorfile>classColourRamp.html</anchorfile>
      <anchor>5e0362e9a56c11a8e823c943a27ac110</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Display</name>
    <filename>classDisplay.html</filename>
    <member kind="function">
      <type></type>
      <name>Display</name>
      <anchorfile>classDisplay.html</anchorfile>
      <anchor>e972fffea6f7ca1d627ef48c3d841bb3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Display</name>
      <anchorfile>classDisplay.html</anchorfile>
      <anchor>c2607a6bb236c55547a4223d40d85d1f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>connect</name>
      <anchorfile>classDisplay.html</anchorfile>
      <anchor>ba15a39c4780b3e0a799e897c4d82bac</anchor>
      <arglist>(const char *display_name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>get_size</name>
      <anchorfile>classDisplay.html</anchorfile>
      <anchor>faac7675cd13861b520eb7e5378b43e5</anchor>
      <arglist>(int &amp;width, int &amp;height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>draw_raster</name>
      <anchorfile>classDisplay.html</anchorfile>
      <anchor>e0648796cbd0b14887213754b85155fb</anchor>
      <arglist>(int x, int y, const unsigned short *pixels, int width)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>DrawRampAction</name>
    <filename>classDrawRampAction.html</filename>
    <member kind="function">
      <type></type>
      <name>DrawRampAction</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>34e13ee745ac45e458184a80467c3889</anchor>
      <arglist>(const Arguments &amp;arguments)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~DrawRampAction</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>013630040b91e445052cdf6866036b19</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>execute</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>202c2cc9951551de6f072db850d2b991</anchor>
      <arglist>(Display *display)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="pure">
      <type>virtual bool</type>
      <name>draw</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>14009098644b675b0eb9ec8bb61df363</anchor>
      <arglist>(Display *display, int width, int height)=0</arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>m_colorTR</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>51d94365ea982445687b2f4d6ea63068</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>m_colorTL</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>da5843b9bc2662dfa22299c00cab8ac4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>m_colorBR</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>c524c68ba43a80b2dc24aa691b89dcc2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="protected">
      <type>Color</type>
      <name>m_colorBL</name>
      <anchorfile>classDrawRampAction.html</anchorfile>
      <anchor>f51b78cd01f6948ebb43ebff08bf2f77</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>LinearRampAction</name>
    <filename>classLinearRampAction.html</filename>
    <base>DrawRampAction</base>
    <member kind="function">
      <type></type>
      <name>LinearRampAction</name>
      <anchorfile>classLinearRampAction.html</anchorfile>
      <anchor>38667ee58b9e4620e2a0841305dc983b</anchor>
      <arglist>(const Arguments &amp;arguments)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual bool</type>
      <name>draw</name>
      <anchorfile>classLinearRampAction.html</anchorfile>
      <anchor>9fcac43a629ee5488418ab70687a1ca2</anchor>
      <arglist>(Display *display, int width, int height)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>bool</type>
      <name>drawLine</name>
      <anchorfile>classLinearRampAction.html</anchorfile>
      <anchor>80f99138307584dfeba671160e896bfc</anchor>
      <arglist>(Display *display, int width, int y, const Color &amp;left, const Color &amp;right)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>RectRampAction</name>
    <filename>classRectRampAction.html</filename>
    <base>DrawRampAction</base>
    <member kind="function">
      <type></type>
      <name>RectRampAction</name>
      <anchorfile>classRectRampAction.html</anchorfile>
      <anchor>4808d69c79387a252b979664e8559120</anchor>
      <arglist>(const Arguments &amp;arguments)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual bool</type>
      <name>draw</name>
      <anchorfile>classRectRampAction.html</anchorfile>
      <anchor>5da47c127be554ea7492a874294011d5</anchor>
      <arglist>(Display *display, int width, int height)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>drawPixel</name>
      <anchorfile>classRectRampAction.html</anchorfile>
      <anchor>3e26db4f3ce94967051fefa9e762ec79</anchor>
      <arglist>(Display *display, int x, int y, double coefBR, double coefBL, double coefTR, double coefTL)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Results</name>
    <filename>classResults.html</filename>
    <member kind="function">
      <type></type>
      <name>Results</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>e15a9e843b67c54e98a6f59a56104e1f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Results</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>215a927dba2d92d8370c9974638dc985</anchor>
      <arglist>(const Results &amp;results)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~Results</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>0547c32c2061192a72cad9db694ffb16</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>load</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>b219ec4227abc28956b34f6219161dd1</anchor>
      <arglist>(const QFile &amp;file)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>width</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>d8cdade22c99de0dbd2064053b88e5e7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>height</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>14b5cda5dd85404d3043417073b98df5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>unsigned short</type>
      <name>get</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>cd0c98fb5237cecb1c4bce2e10094537</anchor>
      <arglist>(int x, int y)</arglist>
    </member>
    <member kind="typedef" protection="private">
      <type>QVector&lt; QVector&lt; unsigned short &gt; &gt;</type>
      <name>DataTable</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>4eb039a4c0714c29baaa39fe35b43a28</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>DataTable</type>
      <name>m_data</name>
      <anchorfile>classResults.html</anchorfile>
      <anchor>501811952bca99b4936329e591dc36ba</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ResultsWidget</name>
    <filename>classResultsWidget.html</filename>
    <member kind="function">
      <type></type>
      <name>ResultsWidget</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>ebd04f034c1394360e844a60b0a2738a</anchor>
      <arglist>(QWidget *parent=0)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setResults</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>77ddfe4146dbf5f2836b99d0f3f4a494</anchor>
      <arglist>(const Results &amp;results)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>paintEvent</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>7bd13ac25c505083494dcd2fc1079157</anchor>
      <arglist>(QPaintEvent *event)</arglist>
    </member>
    <member kind="function" protection="private">
      <type>QColor</type>
      <name>convertColor</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>e9bc95eb3cb6e6aa7382df5937437b33</anchor>
      <arglist>(unsigned short color16)</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>Results</type>
      <name>m_results</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>438e1f5b015c9db9f166b8da3c37069d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>int</type>
      <name>m_pixelSize</name>
      <anchorfile>classResultsWidget.html</anchorfile>
      <anchor>7b240093c7fb3d51faddc77e74457da5</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>TesterFrame</name>
    <filename>classTesterFrame.html</filename>
    <member kind="function">
      <type></type>
      <name>TesterFrame</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>ee0d411390b6644b8fdb7801e0af67ef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~TesterFrame</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>d059c334244a36ee5a110884a7d4461d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot" protection="private">
      <type>void</type>
      <name>fileOpen</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>fed618294bff02cbb247af24b78a442f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot" protection="private">
      <type>void</type>
      <name>runTest</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>1c6f471148523ad4d97452b1b678ee5f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot" protection="private">
      <type>void</type>
      <name>about</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>ad2a41e6113b36cf22bb618ae63176b4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="slot" protection="private">
      <type>void</type>
      <name>aboutQt</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>d7bfb5dbf7c3a2a1588eafabc00f1517</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>createMenu</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>4b005fe8133304dcac7e36a61361f589</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="private">
      <type>void</type>
      <name>createWidgets</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>dec778fd5c4bdb1c052589e328f9fa7d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable" protection="private">
      <type>QLineEdit *</type>
      <name>m_testInfo</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>15d3a174d65f0cc3c1e20ec7891bac2f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable" protection="private">
      <type>ResultsWidget *</type>
      <name>m_resultsWidget</name>
      <anchorfile>classTesterFrame.html</anchorfile>
      <anchor>d601b84e0b8e29a194b4d42dcb5c5ee8</anchor>
      <arglist></arglist>
    </member>
  </compound>
</tagfile>
