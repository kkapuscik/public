/*
 * $Id: results.cpp,v 1.1 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "results.h"

#include <QTextStream>
#include <QStringList>

/*------------------------------------------------------------------------*/

Results::Results()
{
}

/*------------------------------------------------------------------------*/

Results::Results(
        const Results& results)
    : m_data(results.m_data)
{
}

/*------------------------------------------------------------------------*/

Results::~Results()
{
}

/*------------------------------------------------------------------------*/

bool Results::load(
        const QFile& file)
{
    bool result = false;

    m_data.clear();

    QFile tmpFile(file.fileName());

    if(tmpFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&tmpFile);
        for(;;) {
            QString line = in.readLine(20 * 1024);
            if(line.isNull()) {
                result = true;
                break;
            }

            QStringList parts = line.split(' ');
            if(m_data.size() > 0) {
                if(m_data.last().size() != parts.size()) {
                    break;
                }
            }

            /* prepare space for new values */
            m_data.resize(m_data.size() + 1);
            m_data.last().resize(parts.size());

            bool isOK = true;
            for(int i = 0; isOK && i < parts.size(); ++i) {
                m_data.last()[i] = parts[i].toUShort(&isOK, 16);
            }
            if(!isOK) {
                break;
            }
        }

        tmpFile.close();
    }

    return result;
}

/*------------------------------------------------------------------------*/

int Results::width()
{
    if(height() > 0) {
        return m_data.last().size();
    }
    return 0;
}

/*------------------------------------------------------------------------*/

int Results::height()
{
    return m_data.size();
}

/*------------------------------------------------------------------------*/

unsigned short Results::get(
        int x,
        int y)
{
    return m_data[y][x];
}

/*------------------------------------------------------------------------*/
