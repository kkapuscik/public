/*
 * $Id: testerframe.cpp,v 1.2 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "testerframe.h"
#include "results.h"

#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QFileDialog>

/*------------------------------------------------------------------------*/

TesterFrame::TesterFrame()
{
    setWindowTitle(tr("ColorRamp Tester"));
    resize(600, 400);

    createMenu();
    createWidgets();
}

/*------------------------------------------------------------------------*/

TesterFrame::~TesterFrame()
{
}

/*------------------------------------------------------------------------*/

void TesterFrame::createMenu()
{
    QMenu* menuFile = new QMenu(tr("&File"));
    QMenu* menuHelp = new QMenu(tr("&Help"));

    menuFile->addAction(tr("&Open..."), this, SLOT( fileOpen() ));
//    menuFile->addAction(tr("&Run..."), this, SLOT( runTest() ));
    menuFile->addSeparator();
    menuFile->addAction(tr("E&xit"), this, SLOT( close() ));

    menuHelp->addAction(tr("&About..."), this, SLOT( about() ));
    menuHelp->addAction(tr("&About Qt..."), this, SLOT( aboutQt() ));

    menuBar()->addMenu(menuFile);
    menuBar()->addMenu(menuHelp);
}

/*------------------------------------------------------------------------*/

void TesterFrame::createWidgets()
{
    QWidget* centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);

    m_testInfo = new QLineEdit(tr("No results available"), centralWidget);
    m_testInfo->setReadOnly(false);

    m_resultsWidget = new ResultsWidget(centralWidget);

    QVBoxLayout* mainLayout = new QVBoxLayout(centralWidget);
    mainLayout->setSpacing(5);
    mainLayout->addWidget(m_testInfo, 0);
    mainLayout->addWidget(m_resultsWidget, 1);
}

/*------------------------------------------------------------------------*/

void TesterFrame::fileOpen()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open results file"));
    if(fileName.length() > 0) {
        Results results;
        QFile resultsFile(fileName);

        if(results.load(resultsFile)) {
            m_testInfo->setText(tr("Results file: ") + fileName);
            m_resultsWidget->setResults(results);
        } else {
            QMessageBox::warning(this, tr("Open file"),
                    tr("Could not load results data from selected file:\n") + fileName);
        }
    }
}

/*------------------------------------------------------------------------*/

void TesterFrame::runTest()
{
}

/*------------------------------------------------------------------------*/

void TesterFrame::about()
{
    QMessageBox::about(this, tr("About"),
            tr("ColourRamp Tester 0.1\n\n"
            "Copyright (C) 2008 Krzysztof Kapuscik\n"
            "All Rights Reserved.\n\n"
            "Contact: k.kapuscik@gmail.com"));
}

/*------------------------------------------------------------------------*/

void TesterFrame::aboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}

/*------------------------------------------------------------------------*/
