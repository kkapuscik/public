/*
 * $Id: resultswidget.cpp,v 1.1 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "resultswidget.h"

#include <QPainter>
#include <Qt>

/*------------------------------------------------------------------------*/

ResultsWidget::ResultsWidget(
        QWidget* parent)
    : QWidget(parent)
    , m_pixelSize(20)
{
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setAttribute(Qt::WA_StaticContents);
}

/*------------------------------------------------------------------------*/

QColor ResultsWidget::convertColor(
        unsigned short color16)
{
    int r = (color16 >> 11) & 0x1F;
    int g = (color16 >>  5) & 0x3F;
    int b = (color16 >>  0) & 0x1F;

    r = r * 255 / 31;
    g = g * 255 / 63;
    b = b * 255 / 31;

    return QColor(r, g, b);
}

/*------------------------------------------------------------------------*/

void ResultsWidget::paintEvent(
            QPaintEvent* /*event*/)
{
    QPainter painter(this);

    int resultsWidth = m_results.width();
    int resultsHeight = m_results.height();

    painter.setBackground(Qt::white);
    painter.eraseRect(painter.window());

    if(resultsWidth > 0 || resultsHeight > 0) {
        /* draw grid */
        int gridStep = m_pixelSize + 2 + 1;
        painter.setPen(Qt::black);

        int gridHeight = resultsHeight * gridStep;
        for(int x = 0; x <= resultsWidth; ++x) {
            painter.drawLine(x * gridStep, 0, x * gridStep, gridHeight);
        }

        int gridWidth = resultsWidth * gridStep;
        for(int y = 0; y <= resultsHeight; ++y) {
            painter.drawLine(0, y * gridStep, gridWidth, y * gridStep);
        }

        /* draw results */
        for(int x = 0; x < resultsWidth; ++x) {
            int px = x * gridStep + 2;

            for(int y = 0; y < resultsHeight; ++y) {
                int py = y * gridStep + 2;

                unsigned short color16 = m_results.get(x, y);

                QColor color = convertColor(color16);

                painter.fillRect( px, py, m_pixelSize, m_pixelSize, color);
            }
        }

    }
}

/*------------------------------------------------------------------------*/

void ResultsWidget::setResults(
        const Results& results)
{
    m_results = results;
    update(); // schedule repaint
}

/*------------------------------------------------------------------------*/
