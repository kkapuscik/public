/*
 * $Id: testerframe.h,v 1.3 2008-08-20 18:54:44 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _TESTER__TESTER_FRAME_H_
#define _TESTER__TESTER_FRAME_H_

/*------------------------------------------------------------------------*/

#include "resultswidget.h"

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QLineEdit>

/*------------------------------------------------------------------------*/

/**
 * Main frame of tester application.
 */
class TesterFrame : public QMainWindow {
    Q_OBJECT

private:
    /** Test info text line. */
    QLineEdit* m_testInfo;
    /** Results presentation widget. */
    ResultsWidget* m_resultsWidget;

public:
    /**
     * Constructs new frame.
     */
    TesterFrame();

    /**
     * Destructor.
     */
    virtual ~TesterFrame();

private slots:
    /**
     * Opens results file.
     */
    void fileOpen();

    /**
     * Runs test.
     */
    void runTest();

    /**
     * Shows about dialog.
     */
    void about();

    /**
     * Shows Qt about dialog.
     */
    void aboutQt();

private:
    /**
     * Creates menu.
     */
    void createMenu();

    /**
     * Creates frame widgets.
     */
    void createWidgets();

};

/*------------------------------------------------------------------------*/

#endif /*_TESTER__TESTER_FRAME_H_*/
