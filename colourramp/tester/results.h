/*
 * $Id: results.h,v 1.2 2008-08-20 18:54:44 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _TESTER__RESULTS_H_
#define _TESTER__RESULTS_H_

/*------------------------------------------------------------------------*/

#include <QVector>
#include <QFile>

/*------------------------------------------------------------------------*/

/**
 * Results object.
 */
class Results {
private:
    /** Data table type. */
    typedef QVector< QVector <unsigned short> > DataTable;

private:
    /** Data collection. */
    DataTable m_data;

public:
    /**
     * Constructs empty results.
     */
    Results();

    /**
     * Copy constructor.
     *
     * \param[in] results
     *      Results source object.
     */
    Results(
            const Results& results);

    /**
     * Destructor.
     */
    ~Results();

    /**
     * Loads results from file.
     *
     * \param[in] file
     *      Results file.
     *
     * \return
     * True on success, false otherwise.
     */
    bool load(
            const QFile& file);

    /**
     * Returns width of the results array.
     *
     * \return
     * Width in pixels.
     */
    int width();

    /**
     * Returns height of the results array.
     *
     * \return
     * Height in pixels.
     */
    int height();

    /**
     * Returns results element.
     *
     * \param[in] x
     *      X coordinate of the element.
     * \param[in] y
     *      Y coordinate of the element.
     *
     * \return
     * Color of the element (16 bit, 5-6-5 format).
     */
    unsigned short get(
            int x,
            int y);
};

/*------------------------------------------------------------------------*/

#endif /*_TESTER__RESULTS_H_*/
