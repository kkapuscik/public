/*
 * $Id: main.cpp,v 1.1 2008-08-19 12:31:13 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "testerframe.h"

#include <QtWidgets/QApplication>

/*------------------------------------------------------------------------*/

int main(
        int argc,
        char* argv[])
{
    QApplication testerApp(argc, argv);
    TesterFrame testerFrame;
    testerFrame.show();
    return testerApp.exec();
}

/*------------------------------------------------------------------------*/
