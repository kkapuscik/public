/*
 * $Id: resultswidget.h,v 1.2 2008-08-20 18:54:44 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _TESTER__RESULTS_WIDGET_H_
#define _TESTER__RESULTS_WIDGET_H_

/*------------------------------------------------------------------------*/

#include "results.h"

#include <QtWidgets/QWidget>

/*------------------------------------------------------------------------*/

/**
 * Widget for results presentation.
 *
 * <p>Current implementation draws grid with elements of NxN.
 *    Each grid element represents one pixels from results.</p>
 */
class ResultsWidget : public QWidget {
    Q_OBJECT

private:
    /** Results currently shown. */
    Results m_results;
    /** Size of the single element. */
    int m_pixelSize;

public:
    /**
     * Constructs new widget.
     *
     * \param[in] parent
     *      Parent widtget.
     */
    ResultsWidget(
            QWidget* parent = 0);

    /**
     * Sets results to be presented.
     *
     * \param[in] results
     *      Results object.
     */
    void setResults(
            const Results& results);

private:
    /**
     * Converts 16-bit color to QColor.
     *
     * \param[in] color16
     *      Color to be converted. 16-bit in 5-6-5 format.
     *
     * \return
     * Converted color.
     */
    QColor convertColor(
            unsigned short color16);

protected:
    /**
     * Processes paint event.
     *
     * \see Qt / QWidget / paintEvent() for more details.
     *
     * \param[in] event
     * Paint event details.
     */
    virtual void paintEvent(
            QPaintEvent * event);
};

/*------------------------------------------------------------------------*/

#endif /*_TESTER__RESULTS_WIDGET_H_*/
