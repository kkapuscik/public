/*
 * $Id: display.h,v 1.3 2008-08-18 20:07:24 saveman Exp $
 *
 * Copyright (C) 2008 by DisplayLink
 * All Rights Reserved.
 *
 * Modified: 2008 Krzysztof Kapuscik
 */

#ifndef _COLOUR_RAMP__DISPLAY_H_
#define _COLOUR_RAMP__DISPLAY_H_

/*------------------------------------------------------------------------*/

/**
 * Display (output).
 */
class Display {
public:
    /**
     * Constructs new display.
     */
    Display();

    /**
     * Destroys the display.
     */
    ~Display();

    /**
     * Connects display
     *
     * \param[in] display_name
     *      Name of the display to connect.
     *
     * \return
     * True on success, false on failure.
     */
    bool connect(
            const char *display_name);

    /**
     * Returns display size.
     *
     * \param[out] width
     *      Width of the display.
     * \param[out] height
     *      Height of the display.
     */
    void get_size(
            int &width,
            int &height);

    /**
     * Draws raster of pixels.
     *
     * \param[in] x
     *      X coordinate where raster starts.
     * \param[in] y
     *      Y coordinate where raster starts.
     * \param[in] pixels
     *      Array of pixels to be drawn.
     * \param[in] width
     *      Width of the area (number of pixels).
     */
    void draw_raster(
            int x,
            int y,
            const unsigned short *pixels,
            int width);

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__DISPLAY_H_*/
