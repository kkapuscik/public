/*
 * $Id: colourramp.cpp,v 1.3 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "colourramp.h"
#include "display.h"
#include "drawrampaction.h"
#include "rectrampaction.h"
#include "linearrampaction.h"

#include <iostream>
using namespace std;

/*------------------------------------------------------------------------*/

ColourRamp::ColourRamp(
        int argc,
        char* argv[])
    : m_arguments(argc, argv)
{
    // nothing to do
}

/*------------------------------------------------------------------------*/

int ColourRamp::exec()
{
    int result = -1;

    if(m_arguments.is_valid()) {
        /* get display name */
        string displayName = m_arguments.get_string(ARGUMENT_ID__DISPLAY_NAME);

        /* create display */
        Display* display = new Display();
        if(display != NULL) {
            /* try to connect to display */
            if(display->connect(displayName.c_str())) {
//                DrawRampAction* action = new RectRampAction(m_arguments);
                LinearRampAction* action = new LinearRampAction(m_arguments);
                if(action != NULL) {
                    if(action->execute(display)) {
                        result = 0; // success
                    } else {
                        cerr << "ERROR: Action execution failed" << endl;
                        result = 4;
                    }

                    delete action;
                } else {
                    cerr << "ERROR: Could not create action" << endl;
                    result = 4;
                }

            } else {
                cerr << "ERROR: Could not connect to display" << endl;
                result = 3;
            }

            delete display; // this will also print the results
        } else {
            cerr << "ERROR: Could not create display" << endl;
            result = 2;
        }
    } else {
        cerr << "ERROR: Invalid arguments passed" << endl;
        result = 1;
    }

    return result;
}

/*------------------------------------------------------------------------*/
