SOURCES += main.cpp \
 display.cpp \
 arguments.cpp \
 colourramp.cpp \
 drawrampaction.cpp \
 rectrampaction.cpp \
 color.cpp \
 linearrampaction.cpp \
 colordiff.cpp
TEMPLATE = app
CONFIG += warn_on \
	  thread
TARGET = ../bin/colourramp

QT -= gui \
 core
HEADERS += display.h \
arguments.h \
 colourramp.h \
 drawrampaction.h \
 rectrampaction.h \
 color.h \
 linearrampaction.h \
 colordiff.h
CONFIG -= qt

DISTFILES += usage.dox

