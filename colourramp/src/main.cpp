/*
 * $Id: main.cpp,v 1.3 2008-08-18 20:07:24 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "colourramp.h"

/*------------------------------------------------------------------------*/

int main(
        int argc,
        char *argv[])
{
    /* application object */
    ColourRamp colourRamp(argc, argv);

    /* execute application. */
    return colourRamp.exec();
}

/*------------------------------------------------------------------------*/
