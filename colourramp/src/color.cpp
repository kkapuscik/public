/*
 * $Id: color.cpp,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "color.h"

/*------------------------------------------------------------------------*/

Color::Color()
    : m_red(0)
    , m_green(0)
    , m_blue(0)
{
}

/*------------------------------------------------------------------------*/

Color::Color(
        int color16)
{
    m_red   = (color16 >> 11) & 0x1F;
    m_green = (color16 >>  5) & 0x3F;
    m_blue  = (color16 >>  0) & 0x1F;
}

/*------------------------------------------------------------------------*/

Color::Color(
        const Color& other)
    : m_red(other.m_red)
    , m_green(other.m_green)
    , m_blue(other.m_blue)
{
}

/*------------------------------------------------------------------------*/

unsigned short Color::getUShort()
{
    return (m_red << 11) | (m_green << 5) | m_blue;
}

/*------------------------------------------------------------------------*/
