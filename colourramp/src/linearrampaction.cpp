/*
 * $Id: linearrampaction.cpp,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "linearrampaction.h"
#include "colordiff.h"

#include <inttypes.h>
#include <cassert>
#include <iostream>
using namespace std;

/*------------------------------------------------------------------------*/

LinearRampAction::LinearRampAction(
        const Arguments& arguments)
    : DrawRampAction(arguments)
{
}

/*------------------------------------------------------------------------*/

bool LinearRampAction::draw(
        Display* display,
        int width,
        int height)
{
    assert(width > 0 && height > 0);

    if(height == 1) {
        Color left;
        Color right;

        if(m_colorTL == m_colorBL) {
            left = m_colorTL;
        } else {
            left.m_red   = (m_colorTL.m_red   + m_colorBL.m_red   + 1) / 2;
            left.m_green = (m_colorTL.m_green + m_colorBL.m_green + 1) / 2;
            left.m_blue  = (m_colorTL.m_blue  + m_colorBL.m_blue  + 1) / 2;
        }

        if(m_colorTR == m_colorBR) {
            right = m_colorTR;
        } else {
            right.m_red   = (m_colorTR.m_red   + m_colorBR.m_red   + 1) / 2;
            right.m_green = (m_colorTR.m_green + m_colorBR.m_green + 1) / 2;
            right.m_blue  = (m_colorTR.m_blue  + m_colorBR.m_blue  + 1) / 2;
        }

        return drawLine(display, width, 0, left, right);
    } else if(height <= 0xFFFFFF) {
        ColorDiff left_diff(m_colorTL, m_colorBL, height);
        ColorDiff right_diff(m_colorTR, m_colorBR, height);
        Color left_color = m_colorTL;
        Color right_color = m_colorTR;

        for(int y = 0; y < height; ++y) {
            if(! drawLine(display, width, y, left_color, right_color)) {
                return false;
            }

            left_color = left_diff.doStep();
            right_color = right_diff.doStep();
        }

        return true;
    } else {
        cerr << "ERROR: Image height is larger than " << 0xFFFFFF << " pixels. Operation not supported." << endl;
        return false;
    }
}
/*------------------------------------------------------------------------*/

bool LinearRampAction::drawLine(
        Display* display,
        int width,
        int y,
        const Color& left,
        const Color& right)
{
    if(width == 1) {
        Color color;

        if(left == right) {
            color = left;
        } else {
            color.m_red   = (left.m_red   + right.m_red   + 1) / 2;
            color.m_green = (left.m_green + right.m_green + 1) / 2;
            color.m_blue  = (left.m_blue  + right.m_blue  + 1) / 2;
        }

        unsigned short pixel = color.getUShort();

        display->draw_raster(0, y, &pixel, 1);

        return true;
    } else if(width <= 0xFFFFFF) {
        ColorDiff diff(left, right, width);
        Color color = left;

        for(int x = 0; x < width; ++x) {
            unsigned short pixel = color.getUShort();

            display->draw_raster(x, y, &pixel, 1);

            color = diff.doStep();
        }

        return true;
    } else {
        cerr << "ERROR: Image width is larger than " << 0xFFFFFF << " pixels. Operation not supported." << endl;
        return false;
    }
}

/*------------------------------------------------------------------------*/
