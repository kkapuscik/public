/*
 * $Id: colourramp.h,v 1.2 2008-08-18 20:07:24 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__COLOUR_RAMP_H_
#define _COLOUR_RAMP__COLOUR_RAMP_H_

/*------------------------------------------------------------------------*/

#include "arguments.h"

/*------------------------------------------------------------------------*/

/**
 * Colour Ramp application class.
 */
class ColourRamp {
private:
    /** Input arguments. */
    Arguments m_arguments;

public:
    /**
     * Constructs new application
     *
     * \param[in] argc
     *      Program arguments count.
     * \param[in] argv
     *      Program arguments values.
     */
    ColourRamp(
            int argc,
            char* argv[]);

    /**
     * Executes the application.
     *
     * \return
     * Application return code.
     * This value could be returned from main() function.
     */
    int exec();

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__COLOUR_RAMP_H_*/
