/*
 * $Id: rectrampaction.h,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__RECT_RAMP_ACTION_H_
#define _COLOUR_RAMP__RECT_RAMP_ACTION_H_

/*------------------------------------------------------------------------*/

#include "drawrampaction.h"

/*------------------------------------------------------------------------*/

/**
 * Rectangles area based ramp drawing action (algorithm).
 */
class RectRampAction : public DrawRampAction {
public:
    /**
     * Constructs new action.
     *
     * \param[in] arguments
     *      Program arguments.
     */
    RectRampAction(
            const Arguments& arguments);

protected:
    /*
     * DrawRampAction#draw().
     */
    virtual bool draw(
            Display* display,
            int width,
            int height);

private:
    /**
     * Draws single pixel.
     *
     * \param[in] display
     *      Display on which pixel should be drawn.
     * \param[in] x
     *      X coordinate of the pixel.
     * \param[in] y
     *      Y coordinate of the pixel.
     * \param[in] coefBR
     *      Coefficient for bottom-right color.
     * \param[in] coefBL
     *      Coefficient for bottom-left color.
     * \param[in] coefTR
     *      Coefficient for top-right color.
     * \param[in] coefTL
     *      Coefficient for top-left color.
     */
    void drawPixel(
            Display* display,
            int x,
            int y,
            double coefBR,
            double coefBL,
            double coefTR,
            double coefTL);

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__x_H_*/
