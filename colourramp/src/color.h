/*
 * $Id: color.h,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__COLOR_H_
#define _COLOUR_RAMP__COLOR_H_

/*------------------------------------------------------------------------*/

/**
 * Color (16-bit) with separated components.
 */
class Color {
public:
    /** Red component value. */
    int m_red;
    /** Green component value. */
    int m_green;
    /** Blue component value. */
    int m_blue;

public:
    /**
     * Constructs color initialized with zeroes (black).
     */
    Color();

    /**
     * Constructs color initialized with given value.
     *
     * \param[in] color16
     *      Pixel color (uses lower 16 bits in 5-6-5 format).
     */
    Color(
            int color16);

    /**
     * Copy constructor.
     *
     * \param[in] other
     *      Color source.
     */
    Color(
            const Color& other);

    /**
     * Returns 'compressed' representation of color.
     *
     * \return
     * Color encoded in unsigned short (5-6-5 format).
     */
    unsigned short getUShort();

    /**
     * Equality operator.
     *
     * \param[in] color1
     *      First color to be compared.
     * \param[in] color2
     *      Second color to be compared.
     *
     * \return
     * True if objects are equal, false otherwise.
     */
    friend bool operator == (
            const Color& color1,
            const Color& color2)
    {
        return color1.m_red == color2.m_red &&
                color1.m_green == color2.m_green &&
                color1.m_blue == color2.m_blue;
    }
};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__x_H_*/
