/*
 * $Id: linearrampaction.h,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__LINEAR_RAMP_ACTION_H_
#define _COLOUR_RAMP__LINEAR_RAMP_ACTION_H_

/*------------------------------------------------------------------------*/

#include "drawrampaction.h"

/*------------------------------------------------------------------------*/

/**
 * Linear (DDA) ramp drawing action (algorithm).
 *
 * <p>The algorithm:
 * <ol>
 *      <li>For each y calculate color on left & right edge using DDA.</li>
 *      <li>For each pair of colours calculated in step 1 (coordinate y) draw
 *          a line using DDA algorithm.</li>
 * </ol>
 */
class LinearRampAction : public DrawRampAction {
public:
    /**
     * Constructs new action.
     *
     * \param[in] arguments
     *      Algorithm arguments.
     */
    LinearRampAction(
            const Arguments& arguments);

protected:
    /*
     * DrawRampAction#draw()
     */
    virtual bool draw(
            Display* display,
            int width,
            int height);

private:
    /**
     * Draws single line.
     *
     * \param[in] display
     *      Display on which line should be drawn.
     * \param[in] width
     *      Width of the display.
     * \param[in] y
     *      Y coordinate of the line.
     * \param[in] left
     *      Color at the left end of the line.
     * \param[in] right
     *      Color at the right end of the line.
     *
     * \return
     * True on success, false otherwise.
     */
    bool drawLine(
            Display* display,
            int width,
            int y,
            const Color& left,
            const Color& right);

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__LINEAR_RAMP_ACTION_H_*/
