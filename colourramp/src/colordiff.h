/*
 * $Id: colordiff.h,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__COLOR_DIFF_H_
#define _COLOUR_RAMP__COLOR_DIFF_H_

/*------------------------------------------------------------------------*/

#include "color.h"

/*------------------------------------------------------------------------*/

/**
 * Color Digital Differential Analyzer.
 *
 * <p>This class implements DDA algorithm for calculating ramp colors.</p>
 */
class ColorDiff {
private:
    /** Current color value (major color). */
    Color m_color;
    /** Number of calculation steps to do. */
    int m_distance;
    /** Step number. */
    int m_step;

    /** Red component difference. */
    int m_red_diff;
    /** Green component difference. */
    int m_green_diff;
    /** Blue component difference. */
    int m_blue_diff;

    /** Red component difference sign. */
    bool m_red_sign;
    /** Green component difference sign. */
    bool m_green_sign;
    /** Blue component difference sign. */
    bool m_blue_sign;

    /** Red calculation DDA error. */
    int m_red_error;
    /** Green calculation DDA error. */
    int m_green_error;
    /** Blue calculation DDA error. */
    int m_blue_error;

    /** Red DDA level. */
    int m_red_value;
    /** Green DDA level. */
    int m_green_value;
    /** Blue DDA level. */
    int m_blue_value;

public:
    /**
     * Constructs new anylyzer instance.
     *
     * \param[in] color1
     *      Ramp start color.
     * \param[in] color2
     *      Ramp end color.
     * \param[in] line_pixels
     *      Number of pixels in line.
     */
    ColorDiff(
            const Color& color1,
            const Color& color2,
            int line_pixels);

    /**
     * Perform algorithm step.
     *
     * \return
     * Calculated value of the color for this step.
     */
    Color doStep();

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__COLOR_DIFF_H_*/
