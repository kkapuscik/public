/*
 * $Id: drawrampaction.cpp,v 1.3 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "drawrampaction.h"

#include <cassert>
#include <iostream>
using namespace std;

/*------------------------------------------------------------------------*/

DrawRampAction::DrawRampAction(
        const Arguments& arguments)
{
    int valueTR = arguments.get_int(ARGUMENT_ID__COLOR_TOP_RIGHT);
    int valueTL = arguments.get_int(ARGUMENT_ID__COLOR_TOP_LEFT);
    int valueBR = arguments.get_int(ARGUMENT_ID__COLOR_BOTTOM_RIGHT, valueTR);
    int valueBL = arguments.get_int(ARGUMENT_ID__COLOR_BOTTOM_LEFT, valueTL);

    /* setup the paramters */
    m_colorTR = Color(valueTR);
    m_colorTL = Color(valueTL);
    m_colorBR = Color(valueBR);
    m_colorBL = Color(valueBL);
}

/*------------------------------------------------------------------------*/

DrawRampAction::~DrawRampAction()
{
    // nothing to do
}

/*------------------------------------------------------------------------*/

bool DrawRampAction::execute(
        Display* display)
{
    assert(display != NULL);

    /* initialize width & height */
    int width = -1;
    int height = -1;

    /* get & check display size */
    display->get_size(width, height);
    if(width < 0 || height < 0) {
        cerr << "ERROR: Invalid width/height of the display: w=" << width << " h=" << height << endl;
        return false;
    }

    /* check if there are any pixels */
    if(width == 0 || height == 0) {
        // there is nothing to do
        return true;
    }

    return draw(display, width, height);
}

/*------------------------------------------------------------------------*/
