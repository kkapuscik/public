/*
 * $Id: drawrampaction.h,v 1.4 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__DRAW_RAMP_ACTION_H_
#define _COLOUR_RAMP__DRAW_RAMP_ACTION_H_

/*------------------------------------------------------------------------*/

#include "arguments.h"
#include "display.h"
#include "color.h"

/*------------------------------------------------------------------------*/

/**
 * Abstract draw colour ramp action (algorithm).
 */
class DrawRampAction {
protected:
    /** Top-right color. */
    Color m_colorTR;
    /** Top-left color. */
    Color m_colorTL;
    /** Bottom-right color. */
    Color m_colorBR;
    /** Bottom-left color. */
    Color m_colorBL;

public:
    /**
     * Constructs new action.
     *
     * \param[in] arguments
     *      Program arguments.
     */
    DrawRampAction(
            const Arguments& arguments);

    /**
     * Destructor.
     */
    virtual ~DrawRampAction();

    /**
     * Executes the action.
     *
     * \param[in] display
     *      Display to draw ramp on.
     *
     * \return
     * True if action executed successfully, false otherwise.
     */
    bool execute(
            Display* display);

protected:
    /**
     * Draws the ramp.
     *
     * \param[in] display
     *      Display to draw ramp on.
     * \param[in] width
     *      Width of the display.
     * \param[in] height
     *      Height of the display.
     *
     * \return
     * True if ramp was drawn, false otherwise.
     */
    virtual bool draw(
            Display* display,
            int width,
            int height) = 0;

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__x_H_*/
