/*
 * $Id: arguments.cpp,v 1.4 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "arguments.h"

#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>
using namespace std;

/*------------------------------------------------------------------------*/

enum argument_type_t {
    ARGUMENT_TYPE__STRING,
    ARGUMENT_TYPE__INT
};

struct argument_info_t {
    argument_id_t id;
    const char* name;
    argument_type_t type;
    bool required;
    bool hasRange;
    int rangeLow;
    int rangeHigh;
};

/*------------------------------------------------------------------------*/

static const argument_info_t ARGS_INFO[]  = {
    {   ARGUMENT_ID__DISPLAY_NAME,          "--display",        ARGUMENT_TYPE__STRING,  true,   false,  0,  0           },
    {   ARGUMENT_ID__COLOR_TOP_LEFT,        "--topleft",        ARGUMENT_TYPE__INT,     true,   true,   0,  (2 << 16) - 1 },
    {   ARGUMENT_ID__COLOR_TOP_RIGHT,       "--topright",       ARGUMENT_TYPE__INT,     true,   true,   0,  (2 << 16) - 1 },
    {   ARGUMENT_ID__COLOR_BOTTOM_LEFT,     "--bottomleft",     ARGUMENT_TYPE__INT,     false,  true,   0,  (2 << 16) - 1 },
    {   ARGUMENT_ID__COLOR_BOTTOM_RIGHT,    "--bottomright",    ARGUMENT_TYPE__INT,     false,  true,   0,  (2 << 16) - 1 }
};

static const int ARGS_COUNT = sizeof(ARGS_INFO) / sizeof(ARGS_INFO[0]);

/*------------------------------------------------------------------------*/

Arguments::Arguments(
        int argc,
        char* argv[])
{
    /* iterate over arguments */
    for(int argIndex = 1; argIndex < argc; ++argIndex) {
        bool infoFound = false;

        /* find info for current argument */
        for(int infoIndex = 0; infoIndex < ARGS_COUNT; ++infoIndex) {
            /* compare names */
            if(strcmp(argv[argIndex], ARGS_INFO[infoIndex].name) == 0) {
                int argValueIndex = argIndex + 1;

                infoFound = true;

                /* check if there is value available */
                if(argValueIndex < argc) {
                    /* check if argument was not already defined */
                    if(m_string_values.find(ARGS_INFO[infoIndex].id) != m_string_values.end()) {
                        cerr << "WARNING: Argument redefined: " << argv[argIndex] << endl;
                    }

                    /* process the value */
                    switch(ARGS_INFO[infoIndex].type) {
                        case ARGUMENT_TYPE__STRING:
                            /* put to string arguments */
                            m_string_values.insert(make_pair(ARGS_INFO[infoIndex].id, string(argv[argValueIndex])));
                            break;

                        case ARGUMENT_TYPE__INT: {
                            char* start_ptr = argv[argValueIndex];
                            char* end_ptr = argv[argValueIndex];

                            int intValue = strtol(start_ptr, &end_ptr, 0);
                            if(*start_ptr != 0 && *end_ptr == 0) {
                                if(ARGS_INFO[infoIndex].hasRange) {
                                    if((intValue < ARGS_INFO[infoIndex].rangeLow) || (intValue > ARGS_INFO[infoIndex].rangeHigh)) {
                                        cerr << "WARNING: Value for argument outside range: " << argv[argIndex] << " = " << intValue << endl;
                                        cerr << "WARNING: - Range allowed: " << ARGS_INFO[infoIndex].rangeLow << ".." << ARGS_INFO[infoIndex].rangeHigh << endl;
                                        break;
                                    }
                                }

                                /* put to integer arguments */
                                m_int_values.insert(make_pair(ARGS_INFO[infoIndex].id, intValue));

                                /* put to string arguments */
                                m_string_values.insert(make_pair(ARGS_INFO[infoIndex].id, string(argv[argIndex] + 1)));

                            } else {
                                cerr << "WARNING: Invalid value for argument: " << argv[argIndex] << " = " << start_ptr << endl;
                            }
                            break;
                        }
                    }
                } else {
                    cerr << "WARNING: Value missing for argument: " << argv[argIndex] << endl;
                }

                ++argIndex; // skip value
                break;
            }
        }

        if(!infoFound) {
            cerr << "WARNING: Unknown argument: " << argv[argIndex] << endl;
        }
    }

    /* verify if arguments are valid */
    m_is_valid = true;
    for(int j = 0; j < ARGS_COUNT; ++j) {
        if(ARGS_INFO[j].required && !has_string(ARGS_INFO[j].id)) {
            m_is_valid = false;
            break;
        }
    }
}

/*------------------------------------------------------------------------*/

bool Arguments::is_valid() const
{
    return m_is_valid;
}

/*------------------------------------------------------------------------*/

bool Arguments::has_string(
        argument_id_t id) const
{
    return m_string_values.find(id) != m_string_values.end();
}

/*------------------------------------------------------------------------*/

const string& Arguments::get_string(
        argument_id_t id) const
{
    assert(has_string(id));

    return m_string_values.find(id)->second;
}

/*------------------------------------------------------------------------*/

bool Arguments::has_int(
        argument_id_t id) const
{
    return m_int_values.find(id) != m_int_values.end();
}

/*------------------------------------------------------------------------*/

int Arguments::get_int(
        argument_id_t id) const
{
    assert(has_int(id));

    return m_int_values.find(id)->second;
}

/*------------------------------------------------------------------------*/

int Arguments::get_int(
        argument_id_t id,
        int default_value) const
{
    if(has_int(id)) {
        return m_int_values.find(id)->second;
    } else {
        return default_value;
    }
}

/*------------------------------------------------------------------------*/
