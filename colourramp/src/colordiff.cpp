/*
 * $Id: colordiff.cpp,v 1.2 2008-08-20 12:38:28 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "colordiff.h"

#include <cstdlib>
using namespace std;

/*------------------------------------------------------------------------*/

ColorDiff::ColorDiff(
        const Color& color1,
        const Color& color2,
        int line_pixels)
    : m_color(color1)
    , m_distance(line_pixels - 1)
    , m_step(0)
    , m_red_error(0)
    , m_green_error(0)
    , m_blue_error(0)
    , m_red_value(0)
    , m_green_value(0)
    , m_blue_value(0)
{
    m_red_diff   = abs(color1.m_red   - color2.m_red);
    m_green_diff = abs(color1.m_green - color2.m_green);
    m_blue_diff  = abs(color1.m_blue  - color2.m_blue);

    m_red_sign   = (color1.m_red   < color2.m_red);
    m_green_sign = (color1.m_green < color2.m_green);
    m_blue_sign  = (color1.m_blue  < color2.m_blue);
}

/*------------------------------------------------------------------------*/

Color ColorDiff::doStep()
{
    ++m_step;

    m_red_value += m_red_diff;
    m_green_value += m_green_diff;
    m_blue_value += m_blue_diff;

    while(m_red_value >= m_distance) {
        m_red_value -= m_distance;
        m_color.m_red += m_red_sign ? 1 : -1;
    }

    while(m_green_value >= m_distance) {
        m_green_value -= m_distance;
        m_color.m_green += m_green_sign ? 1 : -1;
    }

    while(m_blue_value >= m_distance) {
        m_blue_value -= m_distance;
        m_color.m_blue += m_blue_sign ? 1 : -1;
    }

    Color result(m_color);

    if(m_step <= (m_distance + 1) / 2) {
        if(m_red_value * 2 > m_distance) {
            result.m_red += m_red_sign ? 1 : -1;
        }
        if(m_green_value * 2 > m_distance) {
            result.m_green += m_green_sign ? 1 : -1;
        }
        if(m_blue_value * 2 > m_distance) {
            result.m_blue += m_blue_sign ? 1 : -1;
        }
    } else {
        if(m_red_value * 2 >= m_distance) {
            result.m_red += m_red_sign ? 1 : -1;
        }
        if(m_green_value * 2 >= m_distance) {
            result.m_green += m_green_sign ? 1 : -1;
        }
        if(m_blue_value * 2 >= m_distance) {
            result.m_blue += m_blue_sign ? 1 : -1;
        }
    }

    return result;
}

/*------------------------------------------------------------------------*/
