/*
 * $Id: rectrampaction.cpp,v 1.1 2008-08-20 04:49:30 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#include "rectrampaction.h"

#include <iostream>
using namespace std;

/*------------------------------------------------------------------------*/

RectRampAction::RectRampAction(
        const Arguments& arguments)
    : DrawRampAction(arguments)
{
}

/*------------------------------------------------------------------------*/

bool RectRampAction::draw(
        Display* display,
        int width,
        int height)
{
    for(int y = 0; y < height; ++y) {
        double dy;
        if(height > 1) {
            dy = (double)y / (height - 1);
        } else {
            dy = 0.5f;
        }

        for(int x = 0; x < width; ++x) {
            double dx;
            if(width > 1) {
                dx = (double)x / (width - 1);
            } else {
                dx = 0.5f;
            }

            double coefBR = dx * dy;
            double coefBL = (1.0f - dx) * dy;
            double coefTR = dx * (1.0f - dy);
            double coefTL = (1.0f - dx) * (1.0f - dy);

// double coefDiv = coefBR + coefBL + coefTR + coefTL;
// cerr << coefBR << ' ' << coefBL << ' ' << coefTR << ' ' << coefTL << ' ' << coefDiv << endl;

            drawPixel(display, x, y, coefBR, coefBL, coefTR, coefTL);
        }
    }

    return true;
}

/*------------------------------------------------------------------------*/

void RectRampAction::drawPixel(
        Display* display,
        int x,
        int y,
        double coefBR,
        double coefBL,
        double coefTR,
        double coefTL)
{
    double red, green, blue;

    red  = m_colorBR.m_red * coefBR;
    red += m_colorBL.m_red * coefBL;
    red += m_colorTR.m_red * coefTR;
    red += m_colorTL.m_red * coefTL;

    green  = m_colorBR.m_green * coefBR;
    green += m_colorBL.m_green * coefBL;
    green += m_colorTR.m_green * coefTR;
    green += m_colorTL.m_green * coefTL;

    blue  = m_colorBR.m_blue * coefBR;
    blue += m_colorBL.m_blue * coefBL;
    blue += m_colorTR.m_blue * coefTR;
    blue += m_colorTL.m_blue * coefTL;

    int red_int, green_int, blue_int;

    red_int   = (int)red;
    green_int = (int)green;
    blue_int  = (int)blue;

//    cerr << red << ' ' << green << ' ' << blue << endl;

    if(red_int < 0) {
        red_int = 0;
    } else if(red_int > 0x1F) {
        red_int = 0x1F;
    }

    if(green_int < 0) {
        green_int = 0;
    } else if(green_int > 0x3F) {
        green_int = 0x3F;
    }

    if(blue_int < 0) {
        blue_int = 0;
    } else if(blue_int > 0x1F) {
        blue_int = 0x1F;
    }

    unsigned short color = (red_int << 11) | (green_int << 5) | blue_int;

    display->draw_raster(x, y, &color, 1);
}

/*------------------------------------------------------------------------*/
