/*
 * $Id: arguments.h,v 1.3 2008-08-18 20:07:24 saveman Exp $
 *
 * Copyright (C) 2008 by Krzysztof Kapuscik
 * All Rights Reserved.
 */

#ifndef _COLOUR_RAMP__ARGUMENTS_H_
#define _COLOUR_RAMP__ARGUMENTS_H_

/*------------------------------------------------------------------------*/

#include <string>
#include <map>
using namespace std;

/*------------------------------------------------------------------------*/

/** Argument identifier. */
enum argument_id_t {
    /** Argument id - Display name. */
    ARGUMENT_ID__DISPLAY_NAME,
    /** Argument id - Top left color. */
    ARGUMENT_ID__COLOR_TOP_LEFT,
    /** Argument id - Top right color. */
    ARGUMENT_ID__COLOR_TOP_RIGHT,
    /** Argument id - Bottom left color. */
    ARGUMENT_ID__COLOR_BOTTOM_LEFT,
    /** Argument id - Bottom right color. */
    ARGUMENT_ID__COLOR_BOTTOM_RIGHT
};

/*------------------------------------------------------------------------*/

/**
 * Arguments parses and verifier.
 *
 * <p>This class parses the input arguments, verifying names & values.</p>
 */
class Arguments {
private:
    /** Arguments are valid flag. */
    bool m_is_valid;
    /** Parsed arguments string values. */
    map< argument_id_t, string > m_string_values;
    /** Parsed arguments integer values. */
    map< argument_id_t, int > m_int_values;

public:
    /**
     * Constructs new arguments parser.
     *
     * \param[in] argc
     *      Program arguments count.
     * \param[in] argv
     *      Program arguments values.
     */
    Arguments(
            int argc,
            char* argv[]);

    /**
     * Checks if arguments are valid.
     *
     * \return
     * True if arguments are valid, false otherwise.
     */
    bool is_valid() const;

    /**
     * Checks if there is a string value available for argument.
     *
     * \param[in] id
     *      Identifier of the argument.
     *
     * \return
     * True if value is available, false otherwise.
     */
    bool has_string(
            argument_id_t id) const;

    /**
     * Returns argument string value.
     *
     * \note This function asserts if argument is not available.
     *
     * \param[in] id
     *      Identifier of the argument.
     *
     * \return
     * Value of the argument.
     */
    const string& get_string(
            argument_id_t id) const;

    /**
     * Checks if there is a integer value available for argument.
     *
     * \param[in] id
     *      Identifier of the argument.
     *
     * \return
     * True if value is available, false otherwise.
     */
    bool has_int(
            argument_id_t id) const;

    /**
     * Returns argument integer value.
     *
     * \note This function asserts if argument is not available.
     *
     * \param[in] id
     *      Identifier of the argument.
     *
     * \return
     * Value of the argument.
     */
    int get_int(
            argument_id_t id) const;

    /**
     * Returns argument integer value.
     *
     * \param[in] id
     *      Identifier of the argument.
     * \param[in] default_value
     *      Default value returned if there was no value
     *      defined in input parameters.
     *
     * \return
     * Value of the argument.
     */
    int get_int(
            argument_id_t id,
            int default_value) const;

};

/*------------------------------------------------------------------------*/

#endif /*_COLOUR_RAMP__ARGUMENTS_H_*/
